//
//  ViewController.h
//  TTSEngineTest
//
//  Created by Asatur Galstyan on 10/30/12.
//  Copyright (c) 2012 Zanazan Systems. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "TTSEngine.h"

@interface ViewController : UIViewController {
    
    IBOutlet UITextField *txtToSpeach;
    
    TTSEngine *ttsEngine;
    IBOutlet UISlider *rateSlider;
    IBOutlet UILabel *lblSlider;
}

- (IBAction)btnSpeakPressed:(id)sender;
- (IBAction)rateChanged:(id)sender;

@end