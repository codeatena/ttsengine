//
//  AppDelegate.h
//  TTSEngineTest
//
//  Created by Asatur Galstyan on 10/30/12.
//  Copyright (c) 2012 Zanazan Systems. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
