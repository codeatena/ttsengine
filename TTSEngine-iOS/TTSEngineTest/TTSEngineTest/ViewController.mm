//
//  ViewController.m
//  TTSEngineTest
//
//  Created by Asatur Galstyan on 10/30/12.
//  Copyright (c) 2012 Zanazan Systems. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

static NSString* kTTSEngineLicenseKey = @"Pws5/2h1hX9NwVD/4dj8/A==:d1lT1It+b60ROpfVjUbqmCY5Gm+fhDxS7Ttn1IYx3oc=";

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    ttsEngine = [[TTSEngine alloc] initWithLicense:kTTSEngineLicenseKey withVoice:@"Sivan"];
    
    txtToSpeach.text = @"זה אני.";
    txtToSpeach.text = @"עם זאת, אומרים במשרד האוצר שלא כל הפרטים נסגרו.";
    txtToSpeach.text = @"לקראת ההצבעה בממשלה עומדים על הפרק גם תקציבי משרד הביטחון, משרד החינוך והמשרד לביטחון פנים, שהעומדים בראשם לא מסכימים להצעת האוצר";
    txtToSpeach.text = @"מדוברות חברת דואר ישראל, נמסר כי בחברה פועלים על מנת לתקן את התקלה בהקדם האפשרי.";

    lblSlider.text = [NSString stringWithFormat:@"%.01f", rateSlider.value];
}

- (void)viewDidUnload
{
    [txtToSpeach release];
    txtToSpeach = nil;
    [rateSlider release];
    rateSlider = nil;
    [lblSlider release];
    lblSlider = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (void)dealloc {
    [txtToSpeach release];
    [rateSlider release];
    [lblSlider release];
    [super dealloc];
}

- (IBAction)btnSpeakPressed:(id)sender {
    [txtToSpeach resignFirstResponder];
    [ttsEngine prepareText:txtToSpeach.text];
    [ttsEngine rate:rateSlider.value];
    [ttsEngine play];
}

- (IBAction)rateChanged:(id)sender {
    
    [ttsEngine rate:rateSlider.value];
    lblSlider.text = [NSString stringWithFormat:@"%.01f", rateSlider.value];
}

@end
