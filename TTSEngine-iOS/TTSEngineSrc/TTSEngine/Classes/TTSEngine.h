//
//  TTSEngine.h
//  TTSEngine
//
//  Created by Asatur Galstyan on 23/10/2012.
//  Copyright (c) 2012 ZanazanSystems. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TTSEngine : NSObject

- (id)initWithLicense:(NSString*)licenseKey;
- (id)initWithLicense:(NSString*)licenseKey withVoice:(NSString*)voice;
- (void)prepareText:(NSString*)text;
- (void)play;
- (void)pause;
- (void)stop;
- (void)volume:(float)volume;
- (void)rate:(float)rate;

@end
