#include "common.h"
#include "TreeDict.h"
#include "Sentences.h"
#include "g2p.h"
#include "pos.h"
#include "TTS_Engine.h"
#include "Decimal.h"
#include "Processor.h"
#include "HTS_engine.h"
#include "MemoryBuffer.h"
#include "Encoding.h"

#include <sstream>

std::list<std::string> CTTSEngine::Convert2HTS(const std::string& strPhonetics) const
{
	std::list<std::string> lstResult;

	std::string strPhonetics0 = strPhonetics;
	if (strPhonetics0.length() > 1)
	{
		if (strPhonetics0[strPhonetics0.length() - 1] == '.'
			|| strPhonetics0[strPhonetics0.length() - 1] == '!'
			|| strPhonetics0[strPhonetics0.length() - 1] == '?')
		{
			bool bTruncate = true;
			int nTruncateIndex;
			for (nTruncateIndex = (int)strPhonetics0.length() - 2; nTruncateIndex > 0; nTruncateIndex--)
			{
				if (strPhonetics0[nTruncateIndex] == '.' 
					|| strPhonetics0[nTruncateIndex] == '!' 
					|| strPhonetics0[nTruncateIndex] == '?')
				{
					continue;
				}
				if (strPhonetics0[nTruncateIndex] != ' ')
					bTruncate = false;
				break;
			}
			if (bTruncate && nTruncateIndex >= 0)
				strPhonetics0.erase(nTruncateIndex);
		}
	}
	CPhones phones((std::string)"_ " + strPhonetics0 + " _");
	//CPhones phones(strPhonetics);
	for (size_t pos = 0; pos < phones.length(); pos++)
	{
		if (!phones.IsPhoneme(pos))
			continue;

		std::stringstream output;

		output 
			<< phones.PrevPrevPhoneme(pos)
			<< '^'
			<< phones.PrevPhoneme(pos)
			<< '-'
			<< phones.GetPhonemeAt(pos)
			<< '+'
			<< phones.NextPhoneme(pos)
			<< '='
			<< phones.NextNextPhoneme(pos)
			<< '@'
			<< phones.GetForwardPhonemeInSyllablePos(pos)
			<< '_'
			<< phones.GetBackwardPhonemeInSyllablePos(pos)
			<< "/A:"
			<< phones.IsPrevSyllableStressed(pos)
			<< '_'
			<< phones.IsPrevSyllableAccented(pos)
			<< '_'
			<< phones.GetPrevSyllablePhonemeCount(pos)
			<< "/B:"
			<< phones.IsCurrSyllableStressed(pos)
			<< '-'
			<< phones.IsCurrSyllableAccented(pos)
			<< '-'
			<< phones.GetCurrSyllablePhonemeCount(pos)
			<< '@'
			<< phones.GetForwardSyllableInWordPos(pos)
			<< '-'
			<< phones.GetBackwardSyllableInWordPos(pos)
			<< '&'
			<< phones.GetForwardSyllableInPhrasePos(pos)
			<< '-'
			<< phones.GetBackwardSyllableInPhrasePos(pos)
			<< '#'
			<< phones.GetForwardStressedSyllablesInPhrase(pos)
			<< '-'
			<< phones.GetBackwardStressedSyllablesInPhrase(pos)
			<< '$'
			<< phones.GetForwardAccentedSyllablesInPhrase(pos)
			<< '-'
			<< phones.GetBackwardAccentedSyllablesInPhrase(pos)
			<< '!'
			<< phones.GetForwardCountFromStressedSyllable(pos)
			<< '-'
			<< phones.GetBackwardCountToStressedSyllable(pos)
			<< ';'
			<< phones.GetForwardCountFromAccentedSyllable(pos)
			<< '-'
			<< phones.GetBackwardCountToAccentedSyllable(pos)
			<< '|'
			<< phones.GetCurrSyllableVowels(pos)
			<< "/C:"
			<< phones.IsNextSyllableStressed(pos)
			<< '+'
			<< phones.IsNextSyllableAccented(pos)
			<< '+'
			<< phones.GetNextSyllablePhonemeCount(pos)
			<< "/D:"
			<< phones.GetPrevWordGPOS(pos)
			<< '_'
			<< phones.GetSyllableCountInPrevWord(pos)
			<< "/E:"
			<< phones.GetCurrWordGPOS(pos)
			<< '+'
			<< phones.GetSyllableCountInCurrWord(pos)
			<< '@'
			<< phones.GetForwardWordInPhrasePos(pos)
			<< '+'
			<< phones.GetBackwardWordInPhrasePos(pos)
			<< '&'
			<< phones.GetContentWordCountBeforeCurrInPhrase(pos)
			<< '+'
			<< phones.GetContentWordCountAfterCurrInPhrase(pos)
			<< '#'
			<< phones.GetWordCountFromPrevContentWord(pos)
			<< '+'
			<< phones.GetWordCountToNextContentWord(pos)
			<< "/F:"
			<< phones.GetNextWordGPOS(pos)
			<< '_'
			<< phones.GetSyllableCountInNextWord(pos)
			<< "/G:"
			<< phones.GetSyllableCountInPrevPhrase(pos)
			<< '_'
			<< phones.GetWordCountInPrevPhrase(pos)
			<< "/H:"
			<< phones.GetSyllableCountInCurrPhrase(pos)
			<< '='
			<< phones.GetWordCountInCurrPhrase(pos)
			<< '^'
			<< phones.GetForwardPhraseInUtterancePos(pos)
			<< '='
			<< phones.GetBackwardPhraseInUtterancePos(pos)
			<< '|'
			<< phones.GetCurrPhraseTOBIEndtone(pos)
			<< "/I:"
			<< phones.GetSyllableCountInNextPhrase(pos)
			<< '='
			<< phones.GetWordCountInNextPhrase(pos)
			<< "/J:"
			<< phones.GetSyllableCountInUtterance()
			<< '+'
			<< phones.GetWordCountInUtterance()
			<< '-'
			<< phones.GetPhraseCountInUtterance();

        /*
         sed -i s/A/AA/g *
         sed -i s/S/SS/g *
         sed -i s/Z/ZZ/g *
         sed -i s/M/MM/g *
         sed -i s/J/JJ/g *
         sed -i s/AA:/A:/ *
         sed -i s/JJ:/J:/ *
         */
        std::string strOutput = output.str();
        
        FindAndReplace(strOutput, "A", "AA");
        FindAndReplace(strOutput, "S", "SS");
        FindAndReplace(strOutput, "Z", "ZZ");
        FindAndReplace(strOutput, "M", "MM");
        FindAndReplace(strOutput, "J", "JJ");
        
        FindAndReplace(strOutput, "AA:", "A:");
        FindAndReplace(strOutput, "JJ:", "J:");
        
		lstResult.push_back(strOutput);
	}
	return lstResult;
}

static void HTS_Engine_save_generated_speech(HTS_Engine * engine, CMemoryBuffer& memoryBuffer)
{
   int i;
   short temp;
   HTS_GStreamSet *gss = &engine->gss;

   for (i = 0; i < HTS_GStreamSet_get_total_nsample(gss); i++) {
      temp = HTS_GStreamSet_get_speech(gss, i);
      memoryBuffer.Write(&temp, sizeof(short), 1);
   }
}

static void HTS_Engine_save_riff(HTS_Engine * engine, CMemoryBuffer& memoryBuffer)
{
   int i;
   short temp;

   HTS_GStreamSet *gss = &engine->gss;
   char data_01_04[] = { 'R', 'I', 'F', 'F' };
   int data_05_08 = HTS_GStreamSet_get_total_nsample(gss) * sizeof(short) + 36;
   char data_09_12[] = { 'W', 'A', 'V', 'E' };
   char data_13_16[] = { 'f', 'm', 't', ' ' };
   int data_17_20 = 16;
   short data_21_22 = 1;        /* PCM */
   short data_23_24 = 1;        /* monoral */
   int data_25_28 = engine->global.sampling_rate;
   int data_29_32 = engine->global.sampling_rate * sizeof(short);
   short data_33_34 = sizeof(short);
   short data_35_36 = (short) (sizeof(short) * 8);
   char data_37_40[] = { 'd', 'a', 't', 'a' };
   int data_41_44 = HTS_GStreamSet_get_total_nsample(gss) * sizeof(short);

   /* write header */
   memoryBuffer.WriteLE(data_01_04, sizeof(char), 4);
   memoryBuffer.WriteLE(&data_05_08, sizeof(int), 1);
   memoryBuffer.WriteLE(data_09_12, sizeof(char), 4);
   memoryBuffer.WriteLE(data_13_16, sizeof(char), 4);
   memoryBuffer.WriteLE(&data_17_20, sizeof(int), 1);
   memoryBuffer.WriteLE(&data_21_22, sizeof(short), 1);
   memoryBuffer.WriteLE(&data_23_24, sizeof(short), 1);
   memoryBuffer.WriteLE(&data_25_28, sizeof(int), 1);
   memoryBuffer.WriteLE(&data_29_32, sizeof(int), 1);
   memoryBuffer.WriteLE(&data_33_34, sizeof(short), 1);
   memoryBuffer.WriteLE(&data_35_36, sizeof(short), 1);
   memoryBuffer.WriteLE(data_37_40, sizeof(char), 4);
   memoryBuffer.WriteLE(&data_41_44, sizeof(int), 1);
   /* write data */
   for (i = 0; i < HTS_GStreamSet_get_total_nsample(gss); i++) {
      temp = HTS_GStreamSet_get_speech(gss, i);
      memoryBuffer.WriteLE(&temp, sizeof(short), 1);
   }
}

static void PrepareSynthesis(HTS_Engine *pEngine, double fSpeechRate, const std::list<std::string>& lstLab)
{
	std::string strLabel;
	for (std::list<std::string>::const_iterator it = lstLab.begin(); it != lstLab.end(); it++)
	{
		if (strLabel.length() > 0)
			strLabel += "\n";
		strLabel += *it;
	}
	strLabel += '\0';
	HTS_Engine_load_label_from_string(pEngine, const_cast<char *>(strLabel.c_str()));       /* load label file */

	if (fSpeechRate != 1.0)
		HTS_Label_set_speech_speed(&pEngine->label, fSpeechRate);

	HTS_Engine_create_sstream(pEngine);
	HTS_Engine_create_pstream(pEngine);  /* generate speech parameter vector sequence */
	HTS_Engine_create_gstream(pEngine);  /* synthesize speech */
}

void CTTSEngine::SynthesizeRAW(const std::list<std::string>& lstLab, CMemoryBuffer& memoryBuffer)
{
	CStopWatch stopwatch(_T("SynthesizeRAW"));

	PrepareSynthesis(m_pHTSEngine, m_fSpeechRate, lstLab);

	HTS_Engine_save_generated_speech(m_pHTSEngine, memoryBuffer);

	HTS_Engine_refresh(m_pHTSEngine);

	LOG(_T("Synthesized %lu bytes of RAW audio"), memoryBuffer.GetSize());
}

void CTTSEngine::SynthesizeWAV(const std::list<std::string>& lstLab, CMemoryBuffer& memoryBuffer)
{
	CStopWatch stopwatch(_T("SynthesizeWAV"));

	PrepareSynthesis(m_pHTSEngine, m_fSpeechRate, lstLab);

	HTS_Engine_save_riff(m_pHTSEngine, memoryBuffer);

	HTS_Engine_refresh(m_pHTSEngine);

	LOG(_T("Synthesized %lu bytes of WAV audio"), memoryBuffer.GetSize());
}
