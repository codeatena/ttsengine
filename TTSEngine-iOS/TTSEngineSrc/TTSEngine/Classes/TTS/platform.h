#pragma once

///#include <windows.h>
///#include <tchar.h>

#define PATH_DELIMITER _T('/')

#include <string>
#include <iconv.h>
#include <locale>
#include <iostream>
#include <errno.h>
#include <wchar.h>

#define FROMCODE "UTF-8"

#if (BYTE_ORDER == LITTLE_ENDIAN)
#define TOCODE "UTF-32LE"
#elif (BYTE_ORDER == BIG_ENDIAN)
#define TOCODE "UTF-32BE"
#else
#error Unsupported byte order
#endif


#ifdef _UNICODE
typedef wchar_t TCHAR;
#else
typedef char TCHAR;
#endif

typedef TCHAR *LPTSTR;
typedef const TCHAR *LPCTSTR;
typedef const wchar_t* LPCWSTR;

#ifndef LPCSTR
#define LPCSTR const char *
#endif

#ifndef CP_UTF8
#define CP_UTF8 65001
#endif

inline std::wstring utf8_to_utf16(LPCSTR pchBytes, int nLength)
{
    void *tmp;
    char *outbuf;
    char *inbuf;
    long converted = 0;
    wchar_t *output = NULL;
    int n;
    size_t inbytesleft, outbytesleft, size;
    
    char *input = (char*)pchBytes;
    
    iconv_t cd = iconv_open(TOCODE, FROMCODE);
    if ((iconv_t)-1 == cd) {
        if (EINVAL == errno) {
            printf("iconv: cannot convert from %s to %s\n",
                   FROMCODE, TOCODE);
        } else {
            printf("iconv: %s\n", strerror(errno));
        }
        
    }
    
    size = sizeof(input) * sizeof(wchar_t);
    inbuf = input;
    inbytesleft = nLength;
    while (1) {
        tmp = realloc(output, size + sizeof(wchar_t));
        if (!tmp) {
            printf("realloc: %s\n", strerror(errno));
        }
        output = (wchar_t*)tmp;
        outbuf = (char *)output + converted;
        outbytesleft = size - converted;
        n = iconv(cd, &inbuf, &inbytesleft, &outbuf, &outbytesleft);
        if (-1 == n) {
            if (EINVAL == errno) {
                /* junk at the end of the buffer, ignore it */
                break;
            } else if (E2BIG != errno) {
                /* unrecoverable error */
                printf("iconv: %s\n", strerror(errno));
            }
            /* increase the size of the output buffer */
            converted = size - outbytesleft;
            size <<= 1;
        } else {
            /* done */
            break;
        }
    }
    
    converted = (size - outbytesleft) / sizeof(wchar_t);
    output[converted] = L'\0';
    
    /* flush the iconv buffer */
    iconv(cd, NULL, NULL, &outbuf, &outbytesleft);
    
    std::wstring result(output);
    
    if (output) {
        free(output);
    }
    if (cd) {
        iconv_close(cd);
    }
  
	return result;
}

inline std::wstring utf8_to_utf16(const std::string& str)
{
	return utf8_to_utf16(str.c_str(), str.length());
}

inline std::string utf16_to_utf8( const wchar_t* buffer, int len )
{
   
//    std::wstring wstr = std::wstring(buffer);
//    std::string newbuffer = std::string(wstr.begin(), wstr.end());
    
    char output[1024] = {0};
    
    wcstombs(output, buffer, len);
    std::string newbuffer(output);

	return newbuffer;
}

// Convert wide-character string into UTF8-encoded single-byte-character string.
inline std::string utf16_to_utf8( const std::wstring& str )
{
	return utf16_to_utf8( str.c_str(), (int)str.length() );
}

#ifdef DEBUG
inline void LOG(const TCHAR *pszMessage, ...)
{
	if (pszMessage != NULL)
	{
		va_list vl;
		va_start(vl, pszMessage);
		vprintf(utf16_to_utf8(pszMessage).c_str(), vl);
        va_end(vl);
        
    }

	if (pszMessage == NULL || pszMessage[wcslen(pszMessage) - 1] != '\n')
		printf("\n");
    
}
#else
inline void LOG(const TCHAR *pszMessage, ...) {}
#endif
