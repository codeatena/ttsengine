#include "common.h"
#include "TreeDict.h"
#include "Sentences.h"
#include "g2p.h"
#include "pos.h"
#include "TTS_Engine.h"
#include "HTS_engine.h"

#define MAJOR_DICT_NAME _T("major_dict.bin")
#define NUMBERS_M_NAME _T("numbers_M.bin")
#define NUMBERS_F_NAME _T("numbers_F.bin")

#define TRAINED_LTS_ALLOPHONES _T("allophones.xml")
#define TRAINED_LTS_MODEL _T("lts-model.lts")

#define POS_MANIFEST _T("pos-manifest.properties")
#define POS_MODEL _T("pos.model")

CTTSEngine::CTTSEngine(size_t nSamplingRate, double fSpeechRate)
	: m_pDict(NULL)
	, m_pNumbersM(NULL)
	, m_pNumbersF(NULL)
	, m_pG2P(NULL)
	, m_pModel(NULL)
	, m_pTagger(NULL)
	, m_pHTSEngine(NULL)
	, m_nSamplingRate(nSamplingRate)
	, m_fSpeechRate(fSpeechRate)
{
}

CTTSEngine::~CTTSEngine()
{
	delete m_pDict;
	delete m_pNumbersM;
	delete m_pNumbersF;
	delete m_pG2P;
	delete m_pModel;
	delete m_pTagger;

	if (m_pHTSEngine != NULL)
	{
		HTS_Engine_clear(m_pHTSEngine);
		free(m_pHTSEngine);
	}
}

static void *LoadFile(const string& strFileName)
{
   
    
	FILE *pFile = fopen(Utf16_to_Utf8(strFileName).c_str(), "rb");
	if (pFile == NULL)
	{
		LOG(_T("Unable to open %s"), strFileName.c_str());
        return NULL;
	}

	fseek(pFile, 0, SEEK_END);
	long nFileLengthL = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);

	void *pData = NULL;
	size_t nFileLength = (size_t)nFileLengthL;
	if ((long)nFileLength == nFileLengthL)
	{
		pData = malloc(nFileLength);
		if (pData != NULL)
		{
			size_t nOffset = 0;
			size_t nBytesRead;
			while (nOffset < nFileLength 
				&& (nBytesRead = fread((char *)pData + nOffset, 1, nFileLength - nOffset, pFile)) > 0)
			{
				nOffset += nBytesRead;
			}

			LOG(_T("File %s: %ld bytes loaded"), Utf16_to_Utf8(strFileName).c_str(), nFileLength);
		}
		else
		{
			LOG(_T("File %s doesn't fit into heap: %ld"), strFileName.c_str(), nFileLength);
		}
	}
	else
	{
		LOG(_T("File %s is too large: %ld"), strFileName.c_str(), nFileLengthL);
	}

	fclose(pFile);
	return pData;
}

string CombinePath(const string& strPath, const string& strFileName)
{
	string strCombinedFileName = strPath;
	if (strCombinedFileName[strCombinedFileName.length() - 1] != PATH_DELIMITER)
		strCombinedFileName += PATH_DELIMITER;
	return strCombinedFileName + strFileName;
}

class CStrPtr
{
public:
	CStrPtr(size_t nSize=1)
		: m_nLength(nSize)
		, m_ppszPtr(NULL)
	{
		m_ppszPtr = (char **)calloc(m_nLength, sizeof(char *));
	}

	~CStrPtr()
	{
		if (m_ppszPtr != NULL)
		{
			for (size_t nIndex = 0; nIndex < m_nLength; nIndex++)
			{
				if (m_ppszPtr[nIndex] != NULL)
					free(m_ppszPtr[nIndex]);
			}
			free(m_ppszPtr);
		}
	}

	inline operator char **() { return m_ppszPtr; }
	inline void CopyTo(size_t nIndex, const std::string& strText) { m_ppszPtr[nIndex] = strdup(strText.c_str()); }
	inline void CopyTo(size_t nIndex, const char *pszText) { m_ppszPtr[nIndex] = strdup(pszText); }

private:
	size_t	m_nLength;
	char	**m_ppszPtr;
};

#define INIT_HTS_PATH(var, name) \
	CStrPtr var; var.CopyTo(0, ToUTF8(CombinePath(CombinePath(strVoicePath, _T("HTS")), _T(name))))
#define INIT_HTS_PATH3(var, name1, name2, name3) \
	CStrPtr var(3); \
	var.CopyTo(0, ToUTF8(CombinePath(CombinePath(strVoicePath, _T("HTS")), _T(name1)))); \
	var.CopyTo(1, ToUTF8(CombinePath(CombinePath(strVoicePath, _T("HTS")), _T(name2)))); \
	var.CopyTo(2, ToUTF8(CombinePath(CombinePath(strVoicePath, _T("HTS")), _T(name3))));

bool CTTSEngine::Configure(const string& strDataPath, const string& strVoicePath )
{
	{
		//CStopWatch stopwatch(_T("Configuration: Load dictionaries"));

		void *pMajorDictData = LoadFile(CombinePath(strDataPath, MAJOR_DICT_NAME));
		if (pMajorDictData == NULL)
			return false;
		m_pDict = new MainDict((PCMainTree)pMajorDictData);

		void *pNumbersMData = LoadFile(CombinePath(strDataPath, NUMBERS_M_NAME));
		if (pNumbersMData == NULL)
			return false;
        
		m_pNumbersM = new NumbersDict((PCNumbersTree)pNumbersMData);

		void *pNumbersFData = LoadFile(CombinePath(strDataPath, NUMBERS_F_NAME));
		if (pNumbersFData == NULL)
			return false;
		m_pNumbersF = new NumbersDict((PCNumbersTree)pNumbersFData);
	}

	try
	{
		{
			//CStopWatch stopwatch(_T("Configuration: Initialize TrainedLTS (G2P)"));
			m_pG2P = new CTrainedLTS(CombinePath(strDataPath, TRAINED_LTS_ALLOPHONES), CombinePath(strDataPath, TRAINED_LTS_MODEL));
		}

		{
			//CStopWatch stopwatch(_T("Configuration: Initialize POS Tagger"));
			m_pModel = new CPOSModel(CombinePath(strDataPath, POS_MANIFEST), CombinePath(strDataPath, POS_MODEL));
			m_pTagger = new CPOSTaggerME(*m_pModel);
		}
	}
	catch (std::exception& ex)
	{
		LOG(_T("Error initializing TTSEngine: %s"), ToT(ex.what()).c_str());
		return false;
	}

	{
		//CStopWatch stopwatch(_T("Configuration: HTS Engine"));

		m_pHTSEngine = (HTS_Engine *)calloc(1, sizeof(HTS_Engine));

		HTS_Engine_initialize(m_pHTSEngine, 3);

		{
			INIT_HTS_PATH(fn_ms_dur, "dur.pdf");
			INIT_HTS_PATH(fn_ts_dur, "tree-dur.inf");
			if (HTS_Engine_load_duration_from_fn(m_pHTSEngine, fn_ms_dur, fn_ts_dur, 1) != TRUE)
			{
				LOG(_T("HTS_Engine_load_duration_from_fn failed"));
				return false;
			}
		}
		{
			INIT_HTS_PATH(fn_ms_mgc, "mgc.pdf");
			INIT_HTS_PATH(fn_ts_mgc, "tree-mgc.inf");
			INIT_HTS_PATH3(fn_ws_mgc, "mgc.win1", "mgc.win2", "mgc.win3");
			if (HTS_Engine_load_parameter_from_fn(m_pHTSEngine, fn_ms_mgc, fn_ts_mgc, fn_ws_mgc, 0, FALSE, 3, 1) != TRUE)
			{
				LOG(_T("HTS_Engine_load_parameter_from_fn failed"));
				return false;
			}
		}
		{
			INIT_HTS_PATH(fn_ms_lf0, "lf0.pdf");
			INIT_HTS_PATH(fn_ts_lf0, "tree-lf0.inf");
			INIT_HTS_PATH3(fn_ws_lf0, "lf0.win1", "lf0.win2", "lf0.win3");
			if (HTS_Engine_load_parameter_from_fn(m_pHTSEngine, fn_ms_lf0, fn_ts_lf0, fn_ws_lf0, 1, TRUE, 3, 1) != TRUE)
			{
				LOG(_T("HTS_Engine_load_parameter_from_fn failed"));
				return false;
			}
		}
		{
			INIT_HTS_PATH(fn_ms_lpf, "lpf.pdf");
			INIT_HTS_PATH(fn_ts_lpf, "tree-lpf.inf");
			INIT_HTS_PATH(fn_ws_lpf, "lpf.win1");
			if (HTS_Engine_load_parameter_from_fn(m_pHTSEngine, fn_ms_lpf, fn_ts_lpf, fn_ws_lpf, 2, FALSE, 1, 1) != TRUE)
			{
				LOG(_T("HTS_Engine_load_parameter_from_fn failed"));
				return false;
			}
		}
		{
			INIT_HTS_PATH(fn_ms_gvm, "gv-mgc.pdf");
			INIT_HTS_PATH(fn_ts_gvm, "tree-gv-mgc.inf");
			if (HTS_Engine_load_gv_from_fn(m_pHTSEngine, fn_ms_gvm, fn_ts_gvm, 0, 1) != TRUE)
			{
				LOG(_T("HTS_Engine_load_gv_from_fn failed"));
				return false;
			}
		}
		{
			INIT_HTS_PATH(fn_ms_gvl, "gv-lf0.pdf");
			INIT_HTS_PATH(fn_ts_gvl, "tree-gv-lf0.inf");
			if (HTS_Engine_load_gv_from_fn(m_pHTSEngine, fn_ms_gvl, fn_ts_gvl, 1, 1) != TRUE)
			{
				LOG(_T("HTS_Engine_load_gv_from_fn failed"));
				return false;
			}
		}
		{
			INIT_HTS_PATH(fn_gv_switch, "gv-switch.inf");
			if (HTS_Engine_load_gv_switch_from_fn(m_pHTSEngine, *fn_gv_switch) != TRUE)
			{
				LOG(_T("HTS_Engine_load_gv_switch_from_fn failed"));
				return false;
			}
		}
		HTS_Engine_set_sampling_rate(m_pHTSEngine, m_nSamplingRate);
		HTS_Engine_set_fperiod(m_pHTSEngine, 240);		// -p
		HTS_Engine_set_alpha(m_pHTSEngine, 0.55);		// -a
		HTS_Engine_set_gamma(m_pHTSEngine, 0);			// -g
		HTS_Engine_set_log_gain(m_pHTSEngine, TRUE);	// -l
		HTS_Engine_set_beta(m_pHTSEngine, 0.0);			// -b
		HTS_Engine_set_audio_buff_size(m_pHTSEngine, 1600);
		HTS_Engine_set_msd_threshold(m_pHTSEngine, 1, 0.5);      /* set voiced/unvoiced threshold for stream[1] */
		HTS_Engine_set_gv_weight(m_pHTSEngine, 0, 1.0);
		HTS_Engine_set_gv_weight(m_pHTSEngine, 1, 1.0);
		HTS_Engine_set_gv_weight(m_pHTSEngine, 2, 1.0);

		HTS_Engine_set_duration_interpolation_weight(m_pHTSEngine, 0, 1.0);
		HTS_Engine_set_parameter_interpolation_weight(m_pHTSEngine, 0, 0, 1.0);
		HTS_Engine_set_parameter_interpolation_weight(m_pHTSEngine, 1, 0, 1.0);
		HTS_Engine_set_parameter_interpolation_weight(m_pHTSEngine, 2, 0, 1.0);

		HTS_Engine_set_gv_interpolation_weight(m_pHTSEngine, 0, 0, 1.0);

		HTS_Engine_set_gv_interpolation_weight(m_pHTSEngine, 1, 0, 1.0);
	}

	return true;
}

