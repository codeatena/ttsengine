#include "common.h"
#include "Sentences.h"

//////////////////////////////////////////////////////////////

CSentence::CSentence()
{
}

CSentence::CSentence(const CSentence &other)
	: m_strText(other.m_strText)
{
}

CSentence::CSentence(const std::wstring& strText)
	: m_strText(strText)
{
}

//////////////////////////////////////////////////////////////

CToken::CToken()
{
}

CToken::CToken(const CToken& other)
	: m_strText(other.m_strText)
{
}

CToken::CToken(const std::wstring& strText)
	: m_strText(strText)
{
}

CToken& CToken::operator= (const CToken& other)
{
	m_strText = other.m_strText;
	return *this;
}

//////////////////////////////////////////////////////////////

CSentenceFragment::CSentenceFragment()
	: m_chFragmentLang(0)
{
}

CSentenceFragment::CSentenceFragment(const std::wstring& strSingleToken, char chFragmentLang)
	: m_chFragmentLang(chFragmentLang)
{
	m_tokens.push_back(strSingleToken);
}

CSentenceFragment::CSentenceFragment(const CSentenceFragment& other)
	: m_tokens(other.m_tokens)
	, m_chFragmentLang(other.m_chFragmentLang)
{
}

CSentenceFragment& CSentenceFragment::operator= (const CSentenceFragment& other)
{
	m_tokens = other.m_tokens;
	m_chFragmentLang = other.m_chFragmentLang;
	return *this;
}

void CSentenceFragment::Clear()
{
	m_chFragmentLang = 0;
	m_tokens.clear();
}

std::wstring CSentenceFragment::GetFullText() const
{
	std::wstring strFullText;
	for (CTokens::const_iterator it = m_tokens.begin(); it != m_tokens.end(); it++)
	{
		const CToken& token = *it;
		
		if (token.m_strText.length() == 0)
			continue;

		if (strFullText.length() > 0)
			strFullText.append(L" ");

		strFullText.append(token.m_strText);
	}
	return strFullText;
}

void CSentenceFragment::Merge(const CSentenceFragment& other)
{
	m_tokens.insert(m_tokens.end(), other.m_tokens.begin(), other.m_tokens.end());
}
