#include "Tree.h"

PCMainTreeNodeValue MainTreeLookup(PCMainTree pTree, const wchar_t *pszWord)
{
	PCMainPosTable pPosTable;
	PCMainTreeNode pNode;

	if (pTree == NULL)
		return NULL;
	if (pszWord == NULL || *pszWord == L'\0')
		return NULL;

	pPosTable = (PCMainPosTable)(pTree + sizeof(MAIN_TREE_FLAGS));
	pNode = (PCMainTreeNode)(pTree + sizeof(MAIN_TREE_FLAGS) + pPosTable->m_nElementCount*(pPosTable->m_nElementSize + 1)*sizeof(tree_wchar_t) + sizeof(MainPosTable));

	for (; *pszWord != L'\0'; pszWord++)
	{
		wchar_t ch;
		MAIN_TREE_INDEX nIndex;

		/* See if this is a terminal node (leaf) which does not have any descendants */
		if (pNode->m_nLeafCount == 0)
		{
			pNode = NULL;
			break;
		}

		ch = *pszWord;
		for (nIndex = 0; nIndex < pNode->m_nLeafCount; nIndex++)
		{
			if (pNode->m_leaves[nIndex].m_chCharacter == ch)
				break;

			/* Because the leaves are sorted by the character, we can stop whenever a greater character is reached */
			if (ch < pNode->m_leaves[nIndex].m_chCharacter)
			{
				nIndex = pNode->m_nLeafCount;
				break;
			}
		}

		/* Check if the character was found */
		if (nIndex >= pNode->m_nLeafCount)
		{
			pNode = NULL;
			break;
		}

		/* Otherwise, we're in luck and can continue */
		pNode = (PCMainTreeNode)(pTree + pNode->m_leaves[nIndex].m_ofsTreeNode);
	}

	/* Whenever we have exhaused the input string, or ran out of tree nodes, we can return */
	return ((pNode == NULL || pNode->m_ofsTreeNodeValue == 0) ? NULL : (PCMainTreeNodeValue)(pTree + pNode->m_ofsTreeNodeValue));
}

MAIN_TREE_COUNTER MainTreeValueDictEntryCount(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue)
{
	return pTreeNodeValue->m_nElementCount;
}

MAIN_TREE_INDEX MainTreeValueDictEntryStatus(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue, MAIN_TREE_INDEX nDictEntryIndex)
{
	return pTreeNodeValue->m_dictEntries[nDictEntryIndex].m_nStatus;
}

const tree_wchar_t *MainTreeValueDictEntryPOS(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue, MAIN_TREE_INDEX nDictEntryIndex)
{
	PCMainPosTable pPosTable;

	pPosTable = (PCMainPosTable)(pTree + sizeof(MAIN_TREE_FLAGS));

	return (const tree_wchar_t *)(pPosTable->m_szTransEntries + (pPosTable->m_nElementSize + 1)*pTreeNodeValue->m_dictEntries[nDictEntryIndex].m_idxPOS);
}

const char *MainTreeValueDictEntryPhonemes(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue, MAIN_TREE_INDEX nDictEntryIndex)
{
	return (const char *)(pTree + pTreeNodeValue->m_dictEntries[nDictEntryIndex].m_ofsPhonemes);
}

const char *NumbersTreeLookup(PCNumbersTree pTree, NUMBERS_TREE_INDEX nNumber)
{
	int nIndex, nIncrement, bFound;
	PCNumbersTreeRoot pRoot;

	if (pTree == NULL)
		return NULL;

	pRoot = (PCNumbersTreeRoot)pTree;

	/* Estimate the initial node. If the dictionary is contiguous, we should land right on the correct node */
	nIndex = nNumber;
	if (nIndex >= pRoot->m_nEntriesCount)
		nIndex = pRoot->m_nEntriesCount - 1;

	if (pRoot->m_entries[nIndex].m_nNumber == nNumber)
	{
		bFound = 1;
	}
	else
	{
		/* Choose the direction of moving */
		if (pRoot->m_entries[nIndex].m_nNumber > nNumber)
			nIncrement = -1;
		else
			nIncrement = 1;

		for ( bFound = 0; nIndex >= 0 && nIndex < pRoot->m_nEntriesCount; nIndex += nIncrement)
		{
			if (pRoot->m_entries[nIndex].m_nNumber == nNumber)
			{
				bFound = 1;
				break;
			}
		}
	}

	if (bFound)
		return (const char *)(pTree + pRoot->m_entries[nIndex].m_ofsPhonemes);

	return NULL;
}
