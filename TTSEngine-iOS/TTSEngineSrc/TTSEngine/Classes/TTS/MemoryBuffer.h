#pragma once

#include <stdio.h>

class CMemoryBuffer
{
public:
	CMemoryBuffer();
	~CMemoryBuffer();
    
	void Write(const void *pMemPtr, size_t nSize, size_t nCount);
	void WriteLE(const void *pMemPtr, size_t nSize, size_t nCount);
	
	void *GetBuffer() { return m_pBuffer; }
	operator const void *() const { return m_pBuffer; }
	int GetSize() const { return m_nOffset; }
    
private:
	CMemoryBuffer(const CMemoryBuffer& other);
	CMemoryBuffer& operator = (const CMemoryBuffer& other);
    
	void	*m_pBuffer;
	size_t	m_nSize;
	size_t	m_nOffset;
};
