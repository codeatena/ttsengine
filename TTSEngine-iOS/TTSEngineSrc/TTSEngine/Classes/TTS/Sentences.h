#pragma once

class CSentence
{
public:
    CSentence();
    CSentence(const CSentence& other);
	CSentence(const std::wstring& strText);

	CSentence& operator = (const CSentence& other);

public:
	std::wstring m_strText;
};

typedef std::list<CSentence> CSentences;

class CToken
{
public:
	CToken();
	CToken(const CToken& other);
	CToken(const std::wstring& strText);

	CToken& operator = (const CToken& other);

public:
	std::wstring m_strText;
};

typedef std::vector<CToken> CTokens;

class CSentenceFragment
{
public:
	CSentenceFragment();
	CSentenceFragment(const std::wstring& strSingleToken, char chFragmentLang);
	CSentenceFragment(const CSentenceFragment& other);

	CSentenceFragment& operator = (const CSentenceFragment& other);

	void Clear();
	std::wstring GetFullText() const;
	void Merge(const CSentenceFragment& other);

public:
	CTokens	m_tokens;
	char	m_chFragmentLang;

};

typedef std::vector<CSentenceFragment> CSentenceFragments;
