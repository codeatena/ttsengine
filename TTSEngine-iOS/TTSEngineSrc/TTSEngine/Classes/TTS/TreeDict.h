#ifndef __TREE_DICT_H
#define __TREE_DICT_H

#include "Tree.h"

class MainDict
{
public:
	class const_value
	{
	public:
		const_value()
			: m_nStatus(0)
		{
		}

		const_value(MAIN_TREE_INDEX nStatus, const tree_wchar_t *pszPOS, const char *pszPhonemes)
			: m_nStatus(nStatus)
			, m_strPhonemes(ToT(pszPhonemes))
		{
#ifdef _WIN32
			// wchar_t is 16 bits wide, and byte-compatible with tree_wchar_t
			m_strPOS = (const wchar_t *)pszPOS;
#else
			// assume that wchar_t is 32 bits wide, and therefore requires conversion from tree_wchar_t.
			// tree_wchar_t is UTF16 and wchar_t (in this case) is UTF32, therefore we can simply cast
			// each individual tree_wchar_t to wchar_t and get to the desired result (at least for the
			// Hebrew character range we care about).
			for (; *pszPOS; pszPOS++)
				m_strPOS += (wchar_t)*pszPOS;
#endif
		}

		const_value(const const_value& other)
			: m_nStatus(other.m_nStatus)
			, m_strPOS(other.m_strPOS)
			, m_strPhonemes(other.m_strPhonemes)
		{
		}

		const wchar_t *GetPos() const { return m_strPOS.c_str(); }
		const TCHAR *GetPhonemes() const { return m_strPhonemes.c_str(); }

		const_value& operator = (const const_value& other)
		{
			m_nStatus = other.m_nStatus;
			m_strPOS = other.m_strPOS;
			m_strPhonemes = other.m_strPhonemes;
			return *this;
		}

		MAIN_TREE_INDEX	m_nStatus;
		std::wstring	m_strPOS;
		string			m_strPhonemes;
	};

	class const_iterator
	{
	public:
		const_iterator(int nPosition)
			: m_nPosition(nPosition)
			, m_pNodeValue(NULL)
			, m_pTree(NULL)
		{
		}

		const_iterator(PCMainTreeNodeValue pNodeValue, PCMainTree pTree)
			: m_nPosition(0)
			, m_pNodeValue(pNodeValue)
			, m_pTree(pTree)
		{
		}

		const_iterator(const const_iterator& other)
			: m_nPosition(other.m_nPosition)
			, m_pNodeValue(other.m_pNodeValue)
			, m_pTree(other.m_pTree)
		{
		}

		const_iterator& operator ++ ()
		{
			++m_nPosition;
			if (m_nPosition >= MainTreeValueDictEntryCount(m_pTree, m_pNodeValue))
				m_nPosition = -1;
			return *this;
		}

		const_iterator& operator ++ (int)
		{
			++m_nPosition;
			if (m_nPosition >= MainTreeValueDictEntryCount(m_pTree, m_pNodeValue))
				m_nPosition = -1;
			return *this;
		}

		const_value operator * () const
		{
			if (m_nPosition < 0)
				return const_value();
			return const_value(
				MainTreeValueDictEntryStatus(m_pTree, m_pNodeValue, m_nPosition),
				MainTreeValueDictEntryPOS(m_pTree, m_pNodeValue, m_nPosition),
				MainTreeValueDictEntryPhonemes(m_pTree, m_pNodeValue, m_nPosition)
				);
		}

		bool operator == (const const_iterator& other) const { return m_nPosition == other.m_nPosition; }
		bool operator != (const const_iterator& other) const { return m_nPosition != other.m_nPosition; }

		const_iterator& operator = (const const_iterator& other)
		{
			m_nPosition = other.m_nPosition;
			m_pNodeValue = other.m_pNodeValue;
			m_pTree = other.m_pTree;
			return *this;
		}

		size_t size() const { return MainTreeValueDictEntryCount(m_pTree, m_pNodeValue); }

	private:
		int					m_nPosition;
		PCMainTreeNodeValue	m_pNodeValue;
		PCMainTree			m_pTree;
	};

public:
	MainDict(PCMainTree pTree=NULL)
		: m_pTree(pTree)
	{
	}

	~MainDict()
	{
		if (m_pTree != NULL)
			free((void *)m_pTree);
	}

	const_iterator find(const std::wstring& strWord) const { return find(strWord.c_str()); }

	const_iterator find(const wchar_t *pszWord) const
	{
		PCMainTreeNodeValue result = MainTreeLookup(m_pTree, pszWord);
		if (result == NULL)
			return end();
		return const_iterator(result, m_pTree);
	}

	const_iterator end() const { return const_iterator(-1); }

private:
	PCMainTree	m_pTree;
};

class NumbersDict
{
public:
	class const_iterator
	{
	public:
		const_iterator(const char *pszValue, int nPosition=0)
			: m_nPosition(nPosition)
			, m_pszValue(pszValue)
		{
		}

		const_iterator& operator ++ () { if (m_nPosition < 1) m_nPosition++; return *this; }
		const_iterator& operator ++ (int) { if (m_nPosition < 1) m_nPosition++; return *this; }

		const char * operator * () const { return (m_nPosition == 0 ? m_pszValue : NULL); }

		bool operator == (const const_iterator& other) const { return m_nPosition == other.m_nPosition; }
		bool operator != (const const_iterator& other) const { return m_nPosition != other.m_nPosition; }

	private:
		int			m_nPosition;
		const char	*m_pszValue;
	};

public:
	NumbersDict(PCNumbersTree pTree=NULL)
		: m_pTree(pTree)
	{
	}

	~NumbersDict()
	{
		if (m_pTree != NULL)
			free((void *)m_pTree);
	}

	const_iterator find(const std::wstring& strValue) const
	{
		wchar_t *pszEndPtr = NULL;
		long value = wcstol(strValue.c_str(), &pszEndPtr, 10);
		return find(value);
	}

	const_iterator find(long value) const
	{
		if (value < 0 || value > 65536)
			return end();

		const char *pszResult = Lookup((NUMBERS_TREE_INDEX)value);
		if (pszResult == NULL)
			return end();
		return pszResult;
	}
	const_iterator end() const { return const_iterator(NULL, 1); }

	const char *Lookup(NUMBERS_TREE_INDEX nNumber) const { return NumbersTreeLookup(m_pTree, nNumber); }

private:
	PCNumbersTree	m_pTree;
};

#endif // !__TREE_DICT_H
