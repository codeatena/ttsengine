﻿#include "common.h"
#include "TreeDict.h"
#include "Sentences.h"
#include "g2p.h"
#include "TTS_Engine.h"

#define SENTENCE_DELIMITERS L".?!\r\n"

static void SplitOntoSentences(CSentences& lstSentences, const std::wstring& strText)
{
	lstSentences.clear();

	std::wstring::size_type nOffset = 0;

	while (true)
	{
		// Skip whitespace
		while ((strText[nOffset] == L' ' || strText[nOffset] == L'\t')
			&& nOffset < strText.length())
		{
			++nOffset;
		}

		if( nOffset >= strText.length() )
			break;

		// Find the next potential sentence delimiter.
		std::wstring::size_type nNextOffset = strText.find_first_of(SENTENCE_DELIMITERS, nOffset);
		while (nNextOffset != std::wstring::npos)
		{
			if (nNextOffset == std::wstring::npos)
				LOG(_T("No sentence delimiters found beyond position %u"), nOffset);
			else
			{
				std::wstring strDelimiter;
				switch (strText[nNextOffset])
				{
				case L'\r':
					strDelimiter = L"<LF>";
					break;
				case L'\n':
					strDelimiter = L"<CR>";
					break;
				default:
					strDelimiter = (std::wstring)L"'" + strText[nNextOffset] + L"'";
				}
				LOG(_T("Found sentence delimiter %s at position %u (from position %u)"), 
					ToT(strDelimiter).c_str(), nNextOffset, nOffset);
			}
			
			// If we found a delimiter, let's keep taking all adjacent
			// delimiters until we hit a non-delimiter character.
			if (nNextOffset != std::wstring::npos)
			{
				while (nNextOffset < strText.length() - 1
					&& wcschr(SENTENCE_DELIMITERS, strText[nNextOffset + 1]) != NULL)
				{
					++nNextOffset;

					std::wstring strDelimiter;
					switch (strText[nNextOffset])
					{
					case L'\r':
						strDelimiter = L"<LF>";
						break;
					case L'\n':
						strDelimiter = L"<CR>";
						break;
					default:
						strDelimiter = (std::wstring)L"'" + strText[nNextOffset] + L"'";
					}
					LOG(_T("Adjacent sentence delimiter %s found at position %u"), 
						ToT(strDelimiter).c_str(), nNextOffset);
				}
			}

			if (strText[nNextOffset] == L'.')
			{
				// If it is a part of a group of sentence delimiters (multiple
				// delimiters in a row), we don't have to demand a whitespace
				// character after the dot character.
				if (nNextOffset > 0
					&& wcschr(SENTENCE_DELIMITERS, strText[nNextOffset - 1]) != NULL)
				{
					LOG(_T("The potential dot sentence delimiter is qualified by its belonging to a group"));
					break;
				}
				
				// Otherwise, a dot character on its own may not be a sentence
				// delimiter, after all. The simple criterion is to demand some
				// whitespace following the dot.
				if (nNextOffset < strText.length() - 1 
					&& strText[nNextOffset + 1] != L' '
					&& strText[nNextOffset + 1] != L'\t')
				{
					LOG(_T("The potential dot sentence delimiter is disqualified due to lack of trailing whitespace"));
					// This is not a legitimate sentence separator, because
					// the dot is not followed by whitespace. Let's skip it
					// and proceed to the next one.

					nNextOffset = strText.find_first_of(SENTENCE_DELIMITERS, nNextOffset + 1);
					continue;
				}
			}

			// Otherwise, if the dot is the last character in the string,
			// or it is followed by proper whitespace, we can consider it
			// as a legitimate sentence separator.
			break;
		}

		if (nNextOffset != std::wstring::npos)
		{
			std::wstring strSentence = strText.substr( nOffset, nNextOffset - nOffset + 1 );

			lstSentences.push_back(CSentence(strSentence));

			nOffset = nNextOffset + 1;
			continue;
		}

		// The remainder of the text is not terminated. We'll assume it is a sentence, though.
		std::wstring strSentence = strText.substr( nOffset );
		lstSentences.push_back(CSentence(strSentence));
		break;
	}

	for (CSentences::iterator it = lstSentences.begin(); it != lstSentences.end(); it++)
	{
		CSentence& sentence = *it;

		FindAndErase(sentence.m_strText, L'\r');
		FindAndErase(sentence.m_strText, L'\n');
	}
}

static void new_lid_v4(const CSentence& sentence, CSentenceFragments& fragments)
{
	fragments.clear();

	std::wstring strPreparedText = sentence.m_strText;
	CTokens tokens;

	InsertAt(strPreparedText, 0, L" ");
	strPreparedText.append(L" ");

	int nDoubleSpacePos;
	while ((nDoubleSpacePos = strPreparedText.find(L"  ")) != std::wstring::npos)
		EraseAt(strPreparedText, nDoubleSpacePos, 1);

	std::vector<std::wstring::size_type> spaceIndexes;
	int nSpaceIndex = -1;
	while ((nSpaceIndex = strPreparedText.find(L" ", nSpaceIndex + 1)) != std::wstring::npos)
		spaceIndexes.push_back((std::wstring::size_type)nSpaceIndex);

	for (int index = 0; index < (int)spaceIndexes.size() - 1; index++)
	{
		CToken token(strPreparedText.substr(spaceIndexes.at(index) + 1, spaceIndexes.at(index + 1) - spaceIndexes.at(index) - 1));
		tokens.push_back(token);
	}

	std::string tl;
	for (size_t nIndex = 0; nIndex < tokens.size(); nIndex++)
	{
		std::wstring strTokenText = tokens[nIndex].m_strText;
		std::transform(strTokenText.begin(), strTokenText.end(), strTokenText.begin(), ::tolower);

		int nNN = CountChars(strTokenText, (wchar_t)48, (wchar_t)57);
		int nNE = CountChars(strTokenText, (wchar_t)97, (wchar_t)122);
		int nNH = CountChars(strTokenText, (wchar_t)1450, (wchar_t)1520);
		int nNR = CountChars(strTokenText, (wchar_t)1025, (wchar_t)1105);
		int nNA = CountChars(strTokenText, (wchar_t)1536, (wchar_t)1791);
		int nN = strTokenText.length();

        if (nNH > 0 && nNH >= nNN && nNH >= nNE && nNH >= nNR && nNH >= nNA)
            tl.push_back('H');
		else if (nNN > 0 && nNN >= nNE && nNN >= nNH && nNN >= nNR && nNN >= nNA)
            tl.push_back('N');
        else if (nNE > 0 && nNE >= nNN && nNE >= nNH && nNE >= nNR && nNE >= nNA)
            tl.push_back('E');
		else if (nNR > 0 && nNR >= nNN && nNR >= nNH && nNR >= nNE && nNR >= nNA)
			tl.push_back('R');
		else if (nNA > 0 && nNA >= nNN && nNA >= nNH && nNA >= nNE && nNA >= nNR)
			tl.push_back('A');
        else
            tl.push_back('U');
	}

	LOG(_T("new_lid_v4.1: %s"), ToT(tl).c_str());

	// See if all tokens are either numerics or "unknown" (e.g. punctuation).
	if (tl.find_first_not_of("NU") == std::string::npos)
	{
		// Let's deal with the borderline case: a standalone numeric. Which is assigned
		// the default language configurable by the end user.
		tl = std::string(tl.length(), 'H' /*GetConf().GetNumericsLang()*/);
		LOG(_T("Standalone numeric language override"));
	}

	std::vector<std::string::size_type> problematic;
	for (std::string::size_type index = 0; index < tl.length(); index++)
	{
		if (tl[index] == 'N')
			problematic.push_back(index);
	}
	for (std::string::size_type index = 0; index < tl.length(); index++)
	{
		if (tl[index] == 'U')
			problematic.push_back(index);
	}

	if (problematic.size() == 1)
	{
		if (problematic[0] < tl.size() - 1)
			tl[problematic[0]] = tl[problematic[0] + 1];
		else if (problematic[0] > 0)
			tl[problematic[0]] = tl[problematic[0] - 1];
		else
			tl[problematic[0]] = 'H';
	}
	else
	{
		for (std::vector<std::string::size_type>::size_type p = 0; p < problematic.size(); p++)
		{
			if (problematic[p] > 0 && problematic[p] < tl.length() - 1)
			{
				if (tl[problematic[p] - 1] == tl[problematic[p] + 1])
				{
					tl[problematic[p]] = tl[problematic[p] - 1];
				}
				else
				{
					tl[problematic[p]] = tl[problematic[p] + 1];
				}
			}
			else if (problematic[p] == 0)
			{
				// We don't have to check if there's only one token, because
				// that specific condition would have been cought in the
				// problematic.size() == 1 if-else branch.
				tl[0] = tl[1];
			}
			else if (problematic[p] == tl.length() - 1)
			{
				// We don't have to check if there's only one token, because
				// that specific condition would have been cought in the
				// problematic.size() == 1 if-else branch.
				tl[tl.length() - 1] = tl[tl.length() - 2];
			}
		}
	}

	LOG(_T("new_lid_v4.2: %s"), ToT(tl).c_str());

	CSentenceFragment currentFragment;

	for (std::string::size_type index = 0; index < tl.length(); index++)
	{
		if (index == 0 || currentFragment.m_chFragmentLang != tl[index])
		{
			if (index > 0)
			{
				// Language switch
				fragments.push_back(currentFragment);
				currentFragment.Clear();
			}

			currentFragment.m_chFragmentLang = tl[index];
		}

		// Add the token (with its bookmarks) to the fragment
		currentFragment.m_tokens.push_back(tokens[index]);
	}

	fragments.push_back(currentFragment);

	//if (GetConf().GetMelingoHandleEnglish())
	{
		// If the end user has asked the system to feed standalone English words (that is, surrounded by
		// a larger number of Hebrew words) to Melingo's text processor, we need to identify the described
		// conditions and merge corresponding fragments into a single Hebrew fragment.
		size_t nStartPos = 0, nEndPos = 0;
		size_t nEnglishWords = 0, nHebrewWords = 0;
		while (nEndPos < fragments.size())
		{
			const CSentenceFragment& frag = fragments[nEndPos];
			if (frag.m_chFragmentLang != 'E' && frag.m_chFragmentLang != 'H')
			{
				if (nEndPos > nStartPos + 1 && nEnglishWords > 0 && nHebrewWords > nEnglishWords)
				{
					// This seems like the data condition we're looking for.
					fragments[nStartPos].m_chFragmentLang = 'H';
					for (size_t nIndex = nStartPos + 1; nIndex <= nEndPos - 1; nIndex++)
					{
						fragments[nStartPos].Merge(fragments[nIndex]);
					}
					fragments.erase(fragments.begin() + nStartPos + 1, fragments.begin() + nEndPos - 1);
					nEndPos = nStartPos;
				}

				nStartPos = ++nEndPos;

				nEnglishWords = 0;
				nHebrewWords = 0;
			}
			else
			{
				if (frag.m_chFragmentLang == 'E')
					nEnglishWords += frag.m_tokens.size();
				if (frag.m_chFragmentLang == 'H')
					nHebrewWords += frag.m_tokens.size();

				++nEndPos;
			}
		}

		if (nEndPos > nStartPos + 1 && nEnglishWords > 0 && nHebrewWords > nEnglishWords)
		{
			// This seems like the data condition we're looking for.
			fragments[nStartPos].m_chFragmentLang = 'H';
			for (size_t nIndex = nStartPos + 1; nIndex < nEndPos; nIndex++)
			{
				fragments[nStartPos].Merge(fragments[nIndex]);
			}
			fragments.erase(fragments.begin() + nStartPos + 1, fragments.end());
		}
	}
}

static bool IsEnglish(const wchar_t *pszText)
{
	/*
    no_numbers_length=length(find(double(sentences{x})>=65));
    if length(find(double(sentences{x})>64 & double(sentences{x})<123))==no_numbers_length & no_numbers_length>0
	%all charachters are in English. this is the only case sentene will be sent in whole  
	%(and without processing to the English voice)
	*/

	if (pszText == NULL)
		return false;

	int nNoNumbersLength = 0;
	int nUnder123Length = 0;

	for (const wchar_t *pszTextPosition = pszText; *pszTextPosition; pszTextPosition++)
	{
		if (*pszTextPosition >= 65)
		{
			nNoNumbersLength++;
			if (*pszTextPosition < 123)
				nUnder123Length++;
		}
	}

	return nNoNumbersLength == nUnder123Length && nNoNumbersLength > 0;
}

static void parser4(CSentenceFragment& fragment)
{
	CTokens tokens;

	std::wstring strPreparedText = fragment.GetFullText();

	InsertAt(strPreparedText, 0, L" ");
	strPreparedText.append(L" ");

	/*
	* ticket #53:
	* replace ו/או with ואו (this way I’ll handle it in the dictionary and not break your code.
	* it’s not easy to replace hebrew with phonetics). specifically for this word – it is possible
	* (there is no ואו word, and it makes sense)
	*/
	FindAndReplace(strPreparedText, L" ו/או ", L" ואו ");

	/*
	* stage 1 - this should be ageneral purpose string replace, able to simply
	* replace any string (from an exception list) found in the text. notice that
	* whitespace IS meaningfull.
	* notice some more replacements at the end of this function, with the
	* symbols adjusent to numbers ($,% etc)
	*/
	FindAndReplace(strPreparedText, L" הת' ", L"התקשר ");
	FindAndReplace(strPreparedText, L" לקוח/ה ", L" לקוח / לקוחה");
	FindAndReplace(strPreparedText, L" יקר/ה ", L" יקר / יקרה");
	FindAndReplace(strPreparedText, L"ל2 ", L" לשנֵי ");

	FindAndReplace(strPreparedText, L"\\", L"/"); // slash normalization

	FindAndReplace(strPreparedText, L"(:", L", חיוך ,");
	FindAndReplace(strPreparedText, L":)", L", חיוך ,");
	FindAndReplace(strPreparedText, L"=)", L", חיוך ,");
	FindAndReplace(strPreparedText, L"(=", L", חיוך ,");
	FindAndReplace(strPreparedText, L"(-:", L", חיוך ,");
	FindAndReplace(strPreparedText, L":-)", L", חיוך ,");
	FindAndReplace(strPreparedText, L"(;", L", חיוך ,");
	FindAndReplace(strPreparedText, L";)", L", חיוך ,");
	FindAndReplace(strPreparedText, L";-)", L", חיוך ,");
	FindAndReplace(strPreparedText, L"(-;", L", חיוך ,");
	FindAndReplace(strPreparedText, L"P-)", L", חיוך ,");
	FindAndReplace(strPreparedText, L"):", L", סמיילי עצוב ,");
	FindAndReplace(strPreparedText, L":(", L", סמיילי עצוב ,");
	FindAndReplace(strPreparedText, L");", L", סמיילי עצוב ,");
	FindAndReplace(strPreparedText, L";(", L", סמיילי עצוב ,");
	FindAndReplace(strPreparedText, L")-:", L", סמיילי עצוב ,");
	FindAndReplace(strPreparedText, L":-(", L", סמיילי עצוב ,");
	FindAndReplace(strPreparedText, L")-;", L", סמיילי עצוב ,");
	FindAndReplace(strPreparedText, L";-(", L", סמיילי עצוב ,");
	// quotes/doublequotes normalization - but it is a part of the %general purpose replace
	FindAndReplace(strPreparedText, L"״", L"\"");
	FindAndReplace(strPreparedText, L"“", L"\"");
	FindAndReplace(strPreparedText, L"״", L"\""); //1524
	FindAndReplace(strPreparedText, L"`", L"'");
	FindAndReplace(strPreparedText, L"׳", L"'");

	FindAndReplace(strPreparedText, L"<", L" ");
	FindAndReplace(strPreparedText, L">", L" ");
	FindAndReplace(strPreparedText, L"=", L" ");

	FindAndReplace(strPreparedText, (wchar_t)1523, (wchar_t)39); // apostrophe
	FindAndReplace(strPreparedText, (wchar_t)1524, (wchar_t)39); // apostrophe
	FindAndReplace(strPreparedText, (wchar_t)96, (wchar_t)39); // apostrophe
	FindAndReplace(strPreparedText, (wchar_t)8217, (wchar_t)39); // another kind of apostroph
	FindAndReplace(strPreparedText, (wchar_t)8221, (wchar_t)34); // special double quotes
	FindAndReplace(strPreparedText, (wchar_t)8220, (wchar_t)34); // special double quotes
	FindAndReplace(strPreparedText, (wchar_t)1470, (wchar_t)45); // hyphen
	FindAndReplace(strPreparedText, (wchar_t)1472, (wchar_t)124); // pipe
	FindAndReplace(strPreparedText, (wchar_t)160, (wchar_t)32); // white space
	FindAndReplace(strPreparedText, (wchar_t)1475, (wchar_t)58); // semi colon

	FindAndErase(strPreparedText, (wchar_t)8206);
	FindAndErase(strPreparedText, (wchar_t)8207);
	FindAndReplace(strPreparedText, (wchar_t)1464, (wchar_t)1463); // a
	FindAndReplace(strPreparedText, (wchar_t)1462, (wchar_t)1461); // e

	FindAndReplace(strPreparedText, L" ב-", L" בֵ"); // the whitespace is to the right of the letter, i.e. before the letter
	FindAndReplace(strPreparedText, L" כ-", L" כֵ");
	FindAndReplace(strPreparedText, L" ל-", L" לֵ");
	FindAndReplace(strPreparedText, L"מ-", L" מֵ");
	FindAndReplace(strPreparedText, L"ה-", L" הַ");
	FindAndReplace(strPreparedText, L"ו-", L" וַ");

	FindAndReplace(strPreparedText, L"@", L" שטרודל ");
	FindAndReplace(strPreparedText, L"&", L" אמפרסנד ");
	FindAndReplace(strPreparedText, L"_", L" קו תחתון ");
	FindAndReplace(strPreparedText, L"^", L" ");
	FindAndReplace(strPreparedText, L"-", L" מקף ");

	FindAndReplace(strPreparedText, L"~", L" טילדה ");

	FindAndReplace(strPreparedText, L"!", L" , ");
	FindAndReplace(strPreparedText, L"?", L" ? ");

	//////////////////////////////////////////////////////
	// notice that this part should be AFTER taking care of smilyes... ;-)
	FindAndReplace(strPreparedText, L"[", L"(");
	FindAndReplace(strPreparedText, L"]", L")");
	FindAndReplace(strPreparedText, L"{", L"(");
	FindAndReplace(strPreparedText, L"}", L")");
	FindAndReplace(strPreparedText, L";", L" , ");
	FindAndReplace(strPreparedText, L")", L" , ");
	FindAndReplace(strPreparedText, L"(", L" , ");
	FindAndReplace(strPreparedText, L"|", L" , ");
	////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////
	// if comma  has digits from BOTH sides, remove it. other wise space it.
	FindAndReplace(strPreparedText, L" ,", L" , ");
	FindAndReplace(strPreparedText, L", ", L" , ");

	/*
	p=findstr(text,',');
	x=1;while  x<length(p)
	if isnum(text(p(x)-1)) & isnum(text(p(x)+1))
	text(x)=[];% remove the comma
	p=findstr(text,',');
	end
	x=x+1;
	end
	*/
	int nCommaPos = -1;
	while ((nCommaPos = strPreparedText.find(L",", nCommaPos + 1)) != std::wstring::npos)
	{
		if (nCommaPos > 0 && nCommaPos < (int)strPreparedText.length() - 1)
		{
			if (iswdigit(strPreparedText.at(nCommaPos - 1))
				&& iswdigit(strPreparedText.at(nCommaPos + 1)))
			{
				EraseAt(strPreparedText, nCommaPos, 1);
				--nCommaPos;
			}
		}
	}

	FindAndReplace(strPreparedText, L",", L" , ");

	//////////////////////////////////////////////
	FindAndReplace(strPreparedText, L" .", L" . ");
	FindAndReplace(strPreparedText, L". ", L" . ");
	// handling the dot is slightly different. i dont want to loose 11.08.11
	// (date)

	/*
	p=strfind(text,'.');
	x=1;
	while x<=length(p)
	if isnum(text(p(x)-1)) & isnum(text(p(x)+1))
	text=[text(1:p(x)-1) 'נקודה' text(p(x)+1:end)] ;% replace the dot with the word "dot" in Hebrew
	p=findstr(text,'.');
	x=x-1;
	end
	x=x+1;
	end
	*/
	int nDotPos = -1;
	while ((nDotPos = strPreparedText.find(L".", nDotPos + 1)) != std::wstring::npos)
	{
		if (nDotPos > 0 && nDotPos < (int)strPreparedText.length() - 1)
		{
			if (iswdigit(strPreparedText.at(nDotPos - 1))
				&& iswdigit(strPreparedText.at(nDotPos + 1)))
			{
				ReplaceWith(strPreparedText, nDotPos, 1, L"נקודה");
			}
		}
	}

	FindAndReplace(strPreparedText, L".", L" . ");

	/*
	%p=[findstr(text,':') , findstr(text,'\') , findstr(text,'/'), findstr(text,'-')];%these symbols have a meaning in numbers string, therefore we remove them only if not WITHIN a number string (both sides)
	p=[findstr(text,':') , findstr(text,'\') , findstr(text,'/')];%these symbols have a meaning in numbers string, therefore we remove them only if not WITHIN a number string (both sides)
	x=1;
	while x<=length(p)
	if isnum(text(p(x)-1)) & isnum(text(p(x)+1))
	else
	text=[text(1:p(x)-1) ' ' text(p(x)+1:end)] ;%spaced the charachter, since it's not within numbers
	%         p=[findstr(text,':') , findstr(text,'\') , findstr(text,'/')];
	end
	x=x+1;
	end
	*/
	int nCharPos = -1;
	while ((nCharPos = strPreparedText.find_first_of(L":\\/", nCharPos + 1)) != std::wstring::npos)
	{
		if (nCharPos > 0 && nCharPos < (int)strPreparedText.length() - 1
			&& iswdigit(strPreparedText.at(nCharPos - 1))
			&& iswdigit(strPreparedText.at(nCharPos + 1)))
		{
			continue;
		}
		ReplaceWith(strPreparedText, nCharPos, 1, (strPreparedText[nCharPos] == L':' ? L"," : L" "));
	}

	/*
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%code below hadles the specific case of ב before a number
	p=findstr(text,' ב');
	for x=1:length(p)
	if regexp(text(p(x)+2),'\d')
	%     text=strrep(text,[text(p(x)-1:p(x)+1)],[text(p(x)-1) , ' בֵ ' , text(p(x)+1)]);
	text=[text(1:p(x)+1) ' ' text(p(x)+2:end)];
	end
	end
	*/
	int nInPos = -2;
	while ((nInPos = strPreparedText.find(L" ב", nInPos + 2)) != std::wstring::npos)
	{
		if (nInPos < (int)strPreparedText.length() - 2
			&& iswdigit(strPreparedText.at(nInPos + 2)))
		{
			InsertAt(strPreparedText, nInPos + 2, L" ");
		}
	}

	/*
	%code below removes double quotes only if not INSIDE a word
	%BUT - doubleqoutes are allowed only as the charachter before the last
	%(within a word) - if this isnt the case - they should be remoed, this is
	%the third argument of the AND
	text=strrep(text,' "',' ');
	text=strrep(text,'" ',' ');
	p=findstr(text,'"');
	x=1;
	%Elik: the code below doesnt handle well case of "" double double quotes.
	%revise
	while x<=length(p)
	if text(p(x)-1)~=' ' & text(p(x)+1)~=' '  & text(p(x)+2)==' '
	else
	%        text=strrep(text,[text(p(x)-1:p(x)+1)],[text(p(x)-1) , ' " ' , text(p(x)+1)]);
	text(p(x))=[];%=strrep(text,[text(p(x)-1:p(x)+1)],[text(p(x)-1) , ' " ' , text(p(x)+1)]);
	p=findstr(text,'"');
	end
	x=x+1;
	end
	*/
	FindAndReplace(strPreparedText, L" \"", L" ");
	FindAndReplace(strPreparedText, L"\" ", L" ");

	int nQuotePos = 0;
	while ((nQuotePos = strPreparedText.find(L"\"", nQuotePos)) != std::wstring::npos)
	{
		if (nQuotePos > 0 
			&& strPreparedText.at(nQuotePos - 1) != L' '
			&& nQuotePos < (int)strPreparedText.length() - 2
			&& strPreparedText.at(nQuotePos + 1) != L' '
			&& strPreparedText.at(nQuotePos + 2) == L' ')
		{
			++nQuotePos;
			continue;
		}

		EraseAt(strPreparedText, nQuotePos, 1);
	}

	FindAndReplace(strPreparedText, L" א' ", L" אַלף ");
	FindAndReplace(strPreparedText, L" ב' ", L" בֵית ");
	FindAndReplace(strPreparedText, L" ג' ", L" גימל ");
	FindAndReplace(strPreparedText, L" ד' ", L" דלֵד  ");
	FindAndReplace(strPreparedText, L" ה' ", L" הֵי ");
	FindAndReplace(strPreparedText, L" ו' ", L" וו ");
	FindAndReplace(strPreparedText, L".com ", L" דוׂט קום ");

	/*
	%the below code removes quotes but not after spcific 4 charachters, and not
	%at the end of a word
	p=[findstr(text,'''')];
	x=1;
	while x<=length(p)
	elig=0;
	if  (text(p(x)-1)=='ג' | text(p(x)-1)=='ז' | text(p(x)-1)=='צ' | text(p(x)-1)=='ץ' | text(p(x)+1)==' ')
	elig=1;
	end
	if ~elig
	text(p(x))=[];
	p=[findstr(text,'׳')];
	p=[findstr(text,'''')];
	x=0;
	end
	x=x+1;
	end
	*/

	nQuotePos = 0;
	while ((nQuotePos = strPreparedText.find(L"'", nQuotePos)) != std::wstring::npos)
	{
		if (nQuotePos > 0)
		{
			if (strPreparedText.at(nQuotePos - 1) == L'ג'
				|| strPreparedText.at(nQuotePos - 1) == L'ז'
				|| strPreparedText.at(nQuotePos - 1) == L'צ'
				|| strPreparedText.at(nQuotePos - 1) == L'ץ'
				|| (nQuotePos < (int)strPreparedText.length() - 1 
				&& strPreparedText.at(nQuotePos + 1) == L' '))
			{
				++nQuotePos;
				continue;
			}
		}

		EraseAt(strPreparedText, nQuotePos, 1);
		nQuotePos = 0;
	}

	FindAndReplace(strPreparedText, L" א ", L" אַלֵף ");
	FindAndReplace(strPreparedText, L" ב ", L" בֵית ");
	FindAndReplace(strPreparedText, L" ג ", L" גימל ");
	FindAndReplace(strPreparedText, L" ד ", L" דלֵד ");
	FindAndReplace(strPreparedText, L" ה ", L" הֵי ");
	FindAndReplace(strPreparedText, L" ו ", L" וַו ");
	FindAndReplace(strPreparedText, L" ז ", L" זַין ");
	FindAndReplace(strPreparedText, L" ח ", L" חֵת ");
	FindAndReplace(strPreparedText, L" ט ", L" טֵט ");
	FindAndReplace(strPreparedText, L" י ", L" יוד ");
	FindAndReplace(strPreparedText, L" כ ", L" חף ");
	FindAndReplace(strPreparedText, L" ל ", L" למדאות ");
	FindAndReplace(strPreparedText, L" מ ", L" מֶם ");
	FindAndReplace(strPreparedText, L" נ ", L" נון ");
	FindAndReplace(strPreparedText, L" ס ", L" סַמֵך ");
	FindAndReplace(strPreparedText, L" ע ", L" עיןאות ");
	FindAndReplace(strPreparedText, L" פ ", L" פיהאות ");
	FindAndReplace(strPreparedText, L" צ ", L" צדיאות ");
	FindAndReplace(strPreparedText, L" ק ", L" קוףהאות ");
	FindAndReplace(strPreparedText, L" ר ", L" רֵיש ");
	FindAndReplace(strPreparedText, L" ש ", L" שין ");
	FindAndReplace(strPreparedText, L" ת ", L" תף ");


	/*
	text=[' ' text ' '];
	textN=double(text);
	nums=find(textN>=48 & textN<=57 | text==47 | text==92 | text==58 | text==45);
	lettersH=find((textN>=1456 & textN<=1514) |  text==34 | text==39);
	lettersE=find((textN>=65 & textN<=90) | (textN>=97 & textN<=122));
	vec=zeros(size(text));
	vec(nums)=1;
	vec(lettersH)=2;
	vec(lettersE)=3;
	vec(textN==32)=32;
	counter=1;
	points=[];
	for x=1:length(vec)-1
	if vec(x)~=vec(x+1)
	points(counter)=x;
	counter=counter+1;
	end
	end
	for x=length(points):-1:1
	text=[text(1:points(x)) '  ' text(points(x)+1:end)];
	end
	*/

	InsertAt(strPreparedText, 0, L" ");
	strPreparedText.append(L" ");
	int *pVec = (int *)calloc(strPreparedText.length(), sizeof(int));

	for (int index = 0; index < (int)strPreparedText.length(); index++)
	{
		wchar_t ch = strPreparedText.at(index);
		if ((ch >= 48 && ch <= 57) || ch == 47 || ch == 92 || ch == 58 || ch == 45)
			pVec[index] = 1;
		if ((ch >= 1456 && ch <= 1514) || ch == 34 || ch == 39)
			pVec[index] = 2;
		if ((ch >= 65 && ch <= 90) || (ch >= 97 && ch <= 122))
			pVec[index] = 3;
		if (ch == 32)
			pVec[index] = 32;
	}

	std::list<int> points;
	for (int index = 0; index < (int)strPreparedText.length() - 1; index++)
	{
		if (pVec[index] != pVec[index + 1])
		{
			points.push_front(index);
		}
	}

	free(pVec);

	for (std::list<int>::const_iterator it = points.begin(); it != points.end(); it++)
	{
		InsertAt(strPreparedText, (*it) + 1, L" ");
	}

	//////////////////////////////////////////////////////////////////
	// the purpose of this function is to make sure one of the following
	// characters comes in the right place adjusent to the number
	// fix order of %$שח, and fix commas
	FindAndReplace(strPreparedText, L"$", L" דולרים ");
	FindAndReplace(strPreparedText, L"%", L" אחוזים ");
	FindAndReplace(strPreparedText, L"€", L" אירו ");
	FindAndReplace(strPreparedText, L"₪", L" ש\"ח ");
	FindAndReplace(strPreparedText, L"£", L" פאונד ");
	FindAndReplace(strPreparedText, L"*", L" כוכבית ");
	FindAndReplace(strPreparedText, L"#", L" סולמית ");
	FindAndReplace(strPreparedText, L"+", L" פלוס ");

	/*
	%now - filter out every charachter that isnt a number, punctuation (,.?"') or
	%a letter in Hebrew or English
	ntext=double(text);
	for c=1:length(ntext)
	if ntext(c)<=31 | (ntext(c)>=123 & ntext(c)<=1450) | ntext(c)>1600
	text(c)=' ';
	end
	end
	*/
	for (int index = 0; index < (int)strPreparedText.length(); index++)
	{
		wchar_t ch = strPreparedText.at(index);
		if (ch <= 31 || (ch >= 123 && ch <= 1450) || ch > 1600)
		{
			strPreparedText[index] = L' ';
		}
	}

	/*
	%tokenziation
	for x=1:40;text=strrep(text,'  ',' ');end
	sp=findstr(' ',text);
	token=[];
	for x=1:length(sp)-1%loop on all the words
	token{x}=text(sp(x)+1:sp(x+1)-1);
	end
	*/

	int nDoubleSpacePos;
	while ((nDoubleSpacePos = strPreparedText.find(L"  ")) != std::wstring::npos)
		EraseAt(strPreparedText, nDoubleSpacePos, 1);

	std::vector<std::wstring::size_type> spaceIndexes;
	int nSpaceIndex = -1;
	while ((nSpaceIndex = strPreparedText.find(L" ", nSpaceIndex + 1)) != std::wstring::npos)
		spaceIndexes.push_back((std::wstring::size_type)nSpaceIndex);

	for (int index = 0; index < (int)spaceIndexes.size() - 1; index++)
	{
		CToken token(strPreparedText.substr(spaceIndexes.at(index) + 1, spaceIndexes.at(index + 1) - spaceIndexes.at(index) - 1));
		tokens.push_back(token);
	}

	/*
	after_number=[{'אחוזים'},'#','דולרים','כוכבית','אירו','ש"ח','פאונד','פלוס','סולמית'];
	%the concept below fails for "math" - for example in 1+1 the order should
	%remain. the way to fix it is to add a term that the below works UNLESS the
	%next token is a number
	%better: the way to deal with (example) 3+1 is that like dot, plus between
	%numbers REAMINS, and is handled by numbers module, same goes for - = *
	%(slash is a problem....)
	for x=2:length(token)
	if length(regexp(token{x},'\d')) & x>1
	if sum(strcmp(char(token{x-1}),after_number));
	temp=token{x-1};
	token{x-1}=token{x};
	token{x}=temp;
	end
	end
	end
	*/

	static const wchar_t *szszAfterNumber[] = {
		L"אחוזים",
		L"#",
		L"דולרים",
		L"כוכבית",
		L"אירו",
		L"ש\"ח",
		L"פאונד",
		L"פלוס",
		L"סולמית",
	};
	const int szszAfterNumberSize = sizeof(szszAfterNumber)/sizeof(szszAfterNumber[0]);

	for (int index = 1; index < (int)tokens.size(); index++)
	{
		// regexp(token{x},'\d') really looks for digits within the string
		// and returns an array of indices to each occurrence. The way it
		// is being used (e.g. length(regexp(...))) is to determine whether
		// the string contains any digits at all.
		if (tokens[index].m_strText.find_first_of(L"0123456789") == std::wstring::npos)
			continue;

		bool bAfterNumberFound = false;
		for (int nAfterNumberIndex = 0; nAfterNumberIndex < szszAfterNumberSize; nAfterNumberIndex++)
		{
			if (tokens[index - 1].m_strText == szszAfterNumber[nAfterNumberIndex])
			{
				bAfterNumberFound = true;
				break;
			}
		}

		if (bAfterNumberFound)
		{
			CToken temp = tokens[index];
			tokens[index] = tokens[index - 1];
			tokens[index - 1] = temp;
		}
	}

	/*
	%"makaf" is allowed only between numbers, not words
	for x=2:length(token)-1
		if strcmp(token{x},'מקף')
			if length(regexp(token{x+1},'\d'))>1 & length(regexp(token{x-1},'\d'))>1
			else
				token{x}=[' '];
			end
		end
	end
	*/
	for (int index = 1; index < (int)tokens.size() - 1; index++)
	{
		if (tokens[index].m_strText != L"מקף")
			continue;

		if (tokens[index - 1].m_strText.find_first_of(L"0123456789") != std::wstring::npos
			&& tokens[index + 1].m_strText.find_first_of(L"0123456789") != std::wstring::npos)
		{
			continue;
		}

		tokens[index].m_strText = L" ";
	}

	/*
	FindAndReplace(strPreparedText, L"", L"");
	FindAndReplace(strPreparedText, L"", L"");
	FindAndReplace(strPreparedText, L"", L"");
	FindAndReplace(strPreparedText, L"", L"");

	strRequestBuffer.append(
	"/api/client/get_text_audio?IMEI=2500&sender_language=he&sender=null&language=he&version=1&text=");
	strRequestBuffer.append( urlencode( strUtfText ) );
	*/

	fragment.m_tokens = tokens;
}

bool CTTSEngine::ParseText(const string& strText, CSentenceFragments& lstFragments) const
{
	CStopWatch stopwatch(_T("ParseText"));

	CSentences lstSentences;
	SplitOntoSentences(lstSentences, strText);

	lstFragments.clear();
	for (CSentences::const_iterator it = lstSentences.begin(); it != lstSentences.end(); it++)
	{
		CSentence sentence = *it;
		LOG(_T("Processing sentence '%s' (%d characters)"), ToT(sentence.m_strText).c_str(), sentence.m_strText.length());

		CSentenceFragments lstSentenceFragments;
		new_lid_v4(sentence, lstSentenceFragments);
		LOG(_T("Detected %d fragments in the sentence"), lstSentenceFragments.size());

		lstFragments.insert(lstFragments.end(), lstSentenceFragments.begin(), lstSentenceFragments.end());
	}

	for (CSentenceFragments::iterator it = lstFragments.begin(); it != lstFragments.end(); it++)
	{
		CSentenceFragment& frag = *it;

		if (frag.m_chFragmentLang != 'E' && IsEnglish(frag.GetFullText().c_str()))
		{
			frag.m_chFragmentLang = 'E';
		}
		else if (frag.m_chFragmentLang == 'H')
		{
			parser4(frag);
		}
	}

	return true;
}
