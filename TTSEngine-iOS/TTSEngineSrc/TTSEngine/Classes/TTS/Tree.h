#ifndef __TREE_H
#define __TREE_H

#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>
#include <wchar.h>

typedef unsigned short tree_wchar_t;

#pragma warning(disable: 4200)
#pragma pack(push)
#pragma pack(1)

/* Main Dictionary */

typedef uint32_t MAIN_TREE_OFFSET;
typedef unsigned char MAIN_TREE_COUNTER;
typedef unsigned char MAIN_TREE_INDEX;
typedef const unsigned char *PCMainTree;
typedef uint32_t MAIN_TREE_FLAGS;

typedef struct _tagMainDictEntry
{

	MAIN_TREE_INDEX		m_nStatus;
	MAIN_TREE_INDEX		m_idxPOS;
	MAIN_TREE_OFFSET	m_ofsPhonemes;

}	MainDictEntry, *PMainDictEntry;
typedef const struct _tagMainDictEntry *PCMainDictEntry;

typedef struct _tagMainTreeNodeValue
{

	MAIN_TREE_COUNTER	m_nElementCount;
	MainDictEntry		m_dictEntries[0];

}	MainTreeNodeValue, *PMainTreeNodeValue;
typedef const struct _tagMainTreeNodeValue *PCMainTreeNodeValue;

typedef struct _tagMainTreeLeaf
{

	tree_wchar_t		m_chCharacter;
	MAIN_TREE_OFFSET	m_ofsTreeNode;				/* type: MainTreeNode */

}	MainTreeLeaf, *PMainTreeLeaf;
typedef const struct _tagMainTreeLeaf *PCMainTreeLeaf;

typedef struct _tagMainTreeNode
{

	MAIN_TREE_OFFSET	m_ofsTreeNodeValue;			/* type: MainTreeNodeValue */
	MAIN_TREE_INDEX		m_nLeafCount;
	MainTreeLeaf		m_leaves[0];				/* type: MainTreeLeaf */

}	MainTreeNode, *PMainTreeNode;
typedef const struct _tagMainTreeNode *PCMainTreeNode;

typedef struct _tagMainPosTable
{

	MAIN_TREE_COUNTER	m_nElementCount;
	MAIN_TREE_INDEX		m_nElementSize;
	tree_wchar_t		m_szTransEntries[0];

}	MainPosTable, *PMainPosTable;
typedef const struct _tagMainPosTable *PCMainPosTable;

PCMainTreeNodeValue MainTreeLookup(PCMainTree pTree, const wchar_t *pszWord);

MAIN_TREE_COUNTER MainTreeValueDictEntryCount(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue);
MAIN_TREE_INDEX MainTreeValueDictEntryStatus(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue, MAIN_TREE_INDEX nDictEntryIndex);
const tree_wchar_t *MainTreeValueDictEntryPOS(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue, MAIN_TREE_INDEX nDictEntryIndex);
const char *MainTreeValueDictEntryPhonemes(PCMainTree pTree, PCMainTreeNodeValue pTreeNodeValue, MAIN_TREE_INDEX nDictEntryIndex);

/* Numbers Dictionary */

typedef uint32_t NUMBERS_TREE_OFFSET;
typedef unsigned short NUMBERS_TREE_COUNTER;
typedef unsigned short NUMBERS_TREE_INDEX;
typedef const unsigned char *PCNumbersTree;
typedef uint32_t NUMBERS_TREE_FLAGS;

typedef struct _tagNumbersTreeEntry
{

	NUMBERS_TREE_INDEX	m_nNumber;
	NUMBERS_TREE_OFFSET	m_ofsPhonemes;

}	NumbersTreeEntry, *PNumbersTreeEntry;
typedef const struct _tagNumbersTreeEntry *PCNumbersTreeEntry;

typedef struct _tagNumbersTreeRoot
{

	NUMBERS_TREE_FLAGS		m_nFlags;
	NUMBERS_TREE_COUNTER	m_nEntriesCount;
	NumbersTreeEntry		m_entries[0];

}	NumbersTreeRoot, *PNumbersTreeRoot;
typedef const struct _tagNumbersTreeRoot *PCNumbersTreeRoot;

const char *NumbersTreeLookup(PCNumbersTree pTree, NUMBERS_TREE_INDEX nNumber);

#pragma pack(pop)
#pragma warning(default: 4200)

#ifdef __cplusplus
}
#endif

#endif // !__TREE_H
