﻿#include "common.h"
#include "TreeDict.h"
#include "Sentences.h"
#include "g2p.h"
#include "pos.h"
#include "TTS_Engine.h"

static std::wstring transliterate2(const std::wstring& input)
{
	std::wstring output = input;
	if (input.length() == 0)
		return output;

	__int64_t nCharSum = 0;
	for (int charIndex = 0; charIndex < (int)input.length(); charIndex++)
		nCharSum += input[charIndex];

	if (nCharSum / input.length() > 400)
	{
        FindAndReplace(output, L"א", L"a");
        FindAndReplace(output, L"ב", L"b");
        FindAndReplace(output, L"ג'", L"J");
        FindAndReplace(output, L"ג", L"g");
        FindAndReplace(output, L"ד", L"d");
        FindAndReplace(output, L"ה", L"h");
        FindAndReplace(output, L"ו", L"w");
        FindAndReplace(output, L"ז", L"z");
        FindAndReplace(output, L"ז'", L"Z");
        FindAndReplace(output, L"ח", L"x");
        FindAndReplace(output, L"ט", L"v");
        FindAndReplace(output, L"י", L"i");
        FindAndReplace(output, L"כ", L"k");
        FindAndReplace(output, L"ך", L"k");
        FindAndReplace(output, L"ל", L"l");
        FindAndReplace(output, L"מ", L"m");
        FindAndReplace(output, L"ם", L"m");
        FindAndReplace(output, L"נ", L"n");
        FindAndReplace(output, L"ן", L"n");
        FindAndReplace(output, L"ס", L"s");
        FindAndReplace(output, L"ע", L"y");
        FindAndReplace(output, L"פ", L"p");
        FindAndReplace(output, L"ף", L"p");
        FindAndReplace(output, L"צ'", L"C");
        FindAndReplace(output, L"ץ'", L"C");
        FindAndReplace(output, L"צ", L"c");
        FindAndReplace(output, L"ץ", L"c");
        FindAndReplace(output, L"ק", L"q");
        FindAndReplace(output, L"ר", L"r");
        FindAndReplace(output, L"ש", L"e");
        FindAndReplace(output, L"ת'", L"t");
        FindAndReplace(output, L"ת", L"t");
	}
	else
	{
        FindAndReplace(output, L"a", L"א");
        FindAndReplace(output, L"b", L"ב");
        FindAndReplace(output, L"g", L"ג");
        FindAndReplace(output, L"J", L"ג'");
        FindAndReplace(output, L"d", L"ד");
        FindAndReplace(output, L"h", L"ה");
        FindAndReplace(output, L"w", L"ו");
        FindAndReplace(output, L"z", L"ז");
        FindAndReplace(output, L"x", L"ח");
        FindAndReplace(output, L"v", L"ט");
        FindAndReplace(output, L"i", L"י");
        FindAndReplace(output, L"k", L"כ");
        FindAndReplace(output, L"l", L"ל");
        FindAndReplace(output, L"m", L"מ");
        FindAndReplace(output, L"n", L"נ");
        FindAndReplace(output, L"s", L"ס");
        FindAndReplace(output, L"y", L"ע");
        FindAndReplace(output, L"p", L"פ");
        FindAndReplace(output, L"c", L"צ");
        FindAndReplace(output, L"C", L"צ'");
        FindAndReplace(output, L"q", L"ק");
        FindAndReplace(output, L"r", L"ר");
        FindAndReplace(output, L"e", L"ש");
        FindAndReplace(output, L"t", L"ת");

		if (output[output.length() - 1] == L'כ')
			output[output.length() - 1] = L'ך';

        if (output[output.length() - 1] == L'מ')
            output[output.length() - 1] = L'ם';

        if (output[output.length() - 1] == L'נ')
            output[output.length() - 1] = L'ן';

		if (output[output.length() - 1] == L'צ')
            output[output.length() - 1] = L'ץ';

		if (output[output.length() - 1] == L'פ')
            output[output.length() - 1] = L'ף';
	}

	return output;
}

static void transliterate_nikud(const std::wstring& input, std::vector<std::wstring>& output)
{
/*
switch input
    case 'א'
        output=[{'A'},'']
    case 'ב'
        output=[{'b'},'v'];
    case char(64305)
        output=[{'b'}];
    case 'ג'''
        output={'J'};
    case 'ג'
        output={'g'};
    case 'ד'
        output={'d'};
    case 'ה'
        output=[{'h'},''];
    case 'ו'
        output=[{'v'},''];
    case 'ז'
        output={'z'};
    case 'ז'''
        output={'Z'};
    case 'ח'
        output={'x'};
    case 'ט'
        output={'t'};
    case char(64312)%'?'
        output={'t'};
    case 'י'
        output=[{'y'},'i',''];
    case 'כ'
        output=[{'k'},'x'];
    case char(64315)%'?'
        output=[{'k'}];
    case 'ך'
        output={'x'};
    case 'ל'
        output={'l'};
    case char(64316)%'?'
        output={'l'};
    case 'מ'
        output={'m'};
    case char(64318)%'?'
        output={'m'};
    case 'ם'
        output={'m'};
    case 'נ'
        output={'n'};
    case 'ן'
        output={'n'};
    case 'ס'
        output={'s'};
    case 'ע'
        output=[{'A'},''];
    case 'פ'
        output=[{'p'},'f'];
    case char(64324)%'?'
        output=[{'p'}];
    case 'ף'
        output=[{'p'},'f'];
 case char(64323)%'?'
        output=[{'p'}];
    case 'צ'''
        output={'C'};
    case 'ץ'''
        output={'C'};
    case 'צ'
        output={'M'};
    case 'ץ'
        output={'M'};
    case 'ק',
        output={'k'};
    case 'ר'
        output={'r'};
    case 'ש'
        output=[{'S'},'s'];
    case char(64300)%'?'
        output=[{'S'}];
    case char(64299)%'?'
        output=[{'s'}];
    case 'ת'''
        output={'t'};
    case 'ת'
        output={'t'};
end
*/
	if (input == L"א")
	{
		output.push_back(L"A");
		output.push_back(L"");
	}
	else if (input == L"ב")
	{
		output.push_back(L"b");
		output.push_back(L"v");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64305)
	{
		output.push_back(L"b");
	}
	else if (input == L"ג'")
	{
        output.push_back(L"J");
	}
	else if (input == L"ג")
	{
		output.push_back(L"g");
	}
	else if (input == L"ד")
	{
		output.push_back(L"d");
	}
	else if (input == L"ה")
	{
		output.push_back(L"h");
		output.push_back(L"");
	}
	else if (input == L"ו")
	{
		output.push_back(L"v");
		output.push_back(L"");
	}
	else if (input == L"ז")
	{
		output.push_back(L"z");
	}
	else if (input == L"ז'")
	{
		output.push_back(L"Z");
	}
	else if (input == L"ח")
	{
		output.push_back(L"x");
	}
	else if (input == L"ט")
	{
		output.push_back(L"t");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64312)
	{
		output.push_back(L"t");
	}
	else if (input == L"י")
	{
		output.push_back(L"y");
		output.push_back(L"i");
		output.push_back(L"");
	}
	else if (input == L"כ")
	{
		output.push_back(L"k");
		output.push_back(L"x");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64315)
	{
		output.push_back(L"k");
	}
	else if (input == L"ך")
	{
		output.push_back(L"x");
	}
	else if (input == L"ל")
	{
		output.push_back(L"l");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64316)
	{
		output.push_back(L"l");
	}
	else if (input == L"מ")
	{
		output.push_back(L"m");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64318)
	{
		output.push_back(L"m");
	}
	else if (input == L"ם")
	{
		output.push_back(L"m");
	}
	else if (input == L"נ")
	{
		output.push_back(L"n");
	}
	else if (input == L"ן")
	{
		output.push_back(L"n");
	}
	else if (input == L"ס")
	{
		output.push_back(L"s");
	}
	else if (input == L"ע")
	{
		output.push_back(L"A");
		output.push_back(L"");
	}
	else if (input == L"פ")
	{
		output.push_back(L"p");
		output.push_back(L"f");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64324)
	{
		output.push_back(L"p");
	}
	else if (input == L"ף")
	{
		output.push_back(L"p");
		output.push_back(L"f");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64323)
	{
		output.push_back(L"p");
	}
	else if (input == L"צ'")
	{
		output.push_back(L"C");
	}
	else if (input == L"ץ'")
	{
		output.push_back(L"C");
	}
	else if (input == L"צ")
	{
		output.push_back(L"M");
	}
	else if (input == L"ץ")
	{
		output.push_back(L"M");
	}
	else if (input == L"ק")
	{
		output.push_back(L"k");
	}
	else if (input == L"ר")
	{
		output.push_back(L"r");
	}
	else if (input == L"ש")
	{
		output.push_back(L"S");
		output.push_back(L"s");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64300)
	{
		output.push_back(L"S");
	}
	else if (input.length() == 1 && input[0] == (wchar_t)64299)
	{
		output.push_back(L"s");
	}
	else if (input == L"ת'")
	{
		output.push_back(L"t");
	}
	else if (input == L"ת")
	{
		output.push_back(L"t");
	}
}

static string handle_diacritics(const MainDict& mainDict, const std::wstring& word0)
{
	std::wstring word_sans = L"";
	std::vector<int> dia;
	wchar_t szCh[2] = {0};
	for (int index = 0; index < (int)word0.length(); index++)
	{
		szCh[0] = word0[index];
		if (szCh[0] >= (wchar_t)1488 && szCh[0] <= (wchar_t)1550)
			word_sans.append(szCh);

		if (szCh[0] > (wchar_t)1400 && szCh[0] < (wchar_t)1488)
			dia.push_back(index);
	}

	std::wstring word = word0;
	FindAndReplace(word, L"שּׂ", L"שׂ");
	FindAndReplace(word, L"שּׁ", L"שׁ");
	FindAndReplace(word, (wchar_t)1464, (wchar_t)1463);
	FindAndReplace(word, (wchar_t)1462, (wchar_t)1461);

	std::vector< std::vector<std::wstring> > C;
	std::wstring V;

	for (int d = 0; d < (int)dia.size(); d++)
	{
		if (d == dia.size() - 1 || dia[d + 1] > dia[d] + 1)
		{
			C.push_back(std::vector<std::wstring>());
			transliterate_nikud(word.substr(dia[d] - 1, 1), C[C.size() - 1]);
			wchar_t v = L' ';
			switch(word[dia[d]])
			{
			case (wchar_t)1463:
			case (wchar_t)1458:
			case (wchar_t)1459:
				v = L'a';
				break;
			case (wchar_t)1461:
			case (wchar_t)1457:
				v = L'e';
				break;
			case (wchar_t)1460:
				v = L'i';
				break;
			case (wchar_t)1465:
				v = L'o';
				break;
			case (wchar_t)1467:
				v = L'u';
				break;
			}
			V.push_back(v);
		}
		else
		{
			C.push_back(std::vector<std::wstring>());
			transliterate_nikud(word.substr(dia[d] - 1, 1), C[C.size() - 1]);
			wchar_t v = L' ';
			switch(word[dia[d + 1]])
			{
			case (wchar_t)1463:
			case (wchar_t)1458:
			case (wchar_t)1459:
				v = L'a';
				break;
			case (wchar_t)1461:
			case (wchar_t)1457:
				v = L'e';
				break;
			case (wchar_t)1460:
				v = L'i';
				break;
			case (wchar_t)1465:
				v = L'o';
				break;
			case (wchar_t)1467:
				v = L'u';
				break;
			}
			V.push_back(v);

			// Intentional: skip two instead of one
			++d;
		}
	}

	MainDict::const_iterator it = mainDict.find(word_sans);
	if (it == mainDict.end())
		return L"";
	MainDict::const_value selected_cand = *it;

	int max_cands_counter = -1;
	std::vector<int> cands_counter;
	if (dia.size() > 0)
	{
		for (int z = 0; it != mainDict.end(); z++, it++)
		{
			cands_counter.push_back(0);

			for (int vowels = 0; vowels < (int)V.length(); vowels++)
			{
				for (int cons = 0; cons < (int)C[vowels].size(); cons++)
				{
					std::wstring search = L"";
					search.append(C[vowels][cons]);
					search.push_back(V[vowels]);

					if (wcsstr((*it).GetPhonemes(), search.c_str()) != NULL)
					{
						cands_counter[z] = cands_counter[z] + 1;

						if (cands_counter[z] > max_cands_counter)
						{
							selected_cand = *it;
							max_cands_counter = cands_counter[z];
						}
					}
				}
			}
		}
	}

	return selected_cand.GetPhonemes();
}

static void get_phonetics4(const MainDict& mainDict, const CTrainedLTS& g2p, 
						   const CTokens& sent_tokens, std::vector<string>& sent_phonetics1, 
						   int& sent_easy)
{
	sent_easy = 1;

	for (CTokens::size_type wc = 0; wc < sent_tokens.size(); wc++)
	{
		const std::wstring& sent_token = sent_tokens[wc].m_strText;

		/*
		pun=sum(strcmp(sent_tokens(wc),punct));
		*/
		bool bPunctFound = true;
		for (std::wstring::const_iterator it = sent_token.begin(); it != sent_token.end(); it++)
		{
			if (wcschr(L"?!,./\\:-=;\"@'>< ", *it) == NULL)
			{
				bPunctFound = false;
				break;
			}
		}

		/*
		%decide if WORD (token) is h or E, this is required ONLY for the g2p
		%process. different languages send different requests to mary
		%(different locale)
		temp=double(sent_tokens{wc});
		if mean(temp(temp>64))<1000
			token_lang='E';
		else
			token_lang='H';
		end
		*/

		__int64_t nCharSum = 0;
		int nCharCount = 0;
		for (int charIndex = 0; charIndex < (int)sent_token.length(); charIndex++)
		{
			wchar_t ch = sent_token[charIndex];
            int cha = sent_token[charIndex];
			if (ch > 64)
			{
				nCharSum += ch;
				++nCharCount;
			}
		}

		char token_lang = ((nCharCount == 0 || nCharSum/nCharCount < 1000) ? 'E' : 'H');
		LOG(_T("get_phonetics4.0; token_lang=%c (punctuation=%d)"), token_lang, bPunctFound);

		// if pun==0 & ~isempty(sent_tokens{wc}) & length(regexp(sent_tokens{wc},'\d'))==0%ie word is not a punctuation and doesnt contain a number
		if (!bPunctFound && !sent_token.empty() && sent_token.find_first_of(L"0123456789") == std::wstring::npos)
		{
			/*
			%remove diacritics before the search
			wordinhebrew=sent_tokens{wc};
			if ~isempty(find(wordinhebrew<1488 & wordinhebrew >1455))
				has_diacritics=1;
			else
				has_diacritics=0;
			end
			wordinhebrew((double(wordinhebrew)>=1456 & double(wordinhebrew)<=1487))=[];%making sure diacritics are removed before dictionary search
			*/
			std::wstring wordinhebrew = L"";
			bool has_diacritics = false;
			for (int charIndex = 0; charIndex < (int)sent_token.length(); charIndex++)
			{
				wchar_t ch = sent_token[charIndex];
				if (ch >= 1456 && ch <= 1487)
				{
					has_diacritics = true;
					continue;
				}

				wchar_t szCh[2] = {0};
				szCh[0] = ch;
				wordinhebrew.append(szCh);
			}
			LOG(_T("get_phonetics4.1 [has_diacritics=%d, wordinhebrew=%S, chars=%d]"), has_diacritics, 
				wordinhebrew.c_str(), wordinhebrew.length());

			MainDict::const_iterator it = mainDict.find(wordinhebrew);

			if (it == mainDict.end())
			{
				// Not found
				/*
				elseif isempty(temp)  && isempty(regexp(char(sent_tokens(wc)),'\d'))
					%get phonetics from mary server.
					%differnt handling for Hebrew and English words! (different models within mary)
					%for both H & E - the code below fetches one word at a time. if
					%there is a string of words - better to process them together
				if token_lang=='E'
					%for OOV English words (within hebrew sentences, we send to
					%the g2p server - but need to convert the returned phoneme set, see
					%the deidcated function, and we dont transliterate, also
					%the processing of the reslults is slightly different, as
					%the english model returns results sometimes in a few lines
					tog2p=[tog2p, char([' ####' wordinhebrew '#### '])];
	%                 sent_phonetics1{wc}='gtop';
					word_2_g2p=char(wordinhebrew);
									url_full=['INPUT_TEXT=' word_2_g2p '&INPUT_TYPE=TEXT&OUTPUT_TYPE=PHONEMES&LOCALE=en_US&AUDIO=WAVE_FILE'];%note that this line is different in one parameter for Hebrew or English
									dos(['curl -s -d  "' url_full '" http://localhost:14376/process > g2p_res.txt']) ;
									a=textread('g2p_res.txt','%s','headerlines',4,'delimiter','\n');
									temp=[];
									for l=1:size(a,1)
										if findstr(a{l},'ph="')
											p1=findstr(a{l},'ph="');
											p2=findstr(a{l},'" pos=');
											temp=[temp '-' char(a{l}(p1+4:p2-1))];
										end
									end
									sent_phonetics1{wc}=english2gibrish(temp);
									sent_phonetics1{wc}=strrep(sent_phonetics1{wc},' ','');
									sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'''','.');
									sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'-','#');
									sent_phonetics1{wc}=strrep(sent_phonetics1{wc}, '#.','.');
									sent_phonetics1{wc}=strrep(sent_phonetics1{wc}, '##','#');
				*/
				if (token_lang == 'E')
				{
					// English
					{
						string phonemes = ToT(g2p.Syllabify(g2p.PredictPronunciation(ToUTF8(wordinhebrew))));
						phonemes.insert(0, L"-");

						/*
						sent_phonetics1{wc}=english2gibrish(temp);
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc},' ','');
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'''','.');
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'-','#');
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc}, '#.','.');
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc}, '##','#');
						*/

						/* english2gibrish():
						output=strrep(output,'A','a');
						output=strrep(output,'O','o');
						output=strrep(output,'u','u');
						output=strrep(output,'i','i');
						output=strrep(output,'{','a');
						output=strrep(output,'V','a');
						output=strrep(output,'E','e');
						output=strrep(output,'I','i');
						output=strrep(output,'U','u');
						output=strrep(output,'@','e');
						output=strrep(output,'r=','r');
						output=strrep(output,'aU','aw');
						output=strrep(output,'OI','oy');
						output=strrep(output,'@U','ow');
						output=strrep(output,'EI','ey');
						output=strrep(output,'AI','ay');
						output=strrep(output,'p','p');
						output=strrep(output,'t','t');
						output=strrep(output,'k','k');
						output=strrep(output,'b','b');
						output=strrep(output,'d','d');
						output=strrep(output,'g','g');
						output=strrep(output,'tS','C');
						output=strrep(output,'dZ','J');
						output=strrep(output,'f','f');
						output=strrep(output,'v','v');
						output=strrep(output,'T','t');
						output=strrep(output,'D','d');
						output=strrep(output,'s','s');
						output=strrep(output,'c','k');
						output=strrep(output,'z','z');
						output=strrep(output,'S','S');
						output=strrep(output,'Z','Z');
						output=strrep(output,'h','h');
						output=strrep(output,'l','l');
						output=strrep(output,'m','m');
						output=strrep(output,'n','n');
						output=strrep(output,'N','ng');
						output=strrep(output,'r','r');
						output=strrep(output,'w','w');
						output=strrep(output,'j','y');
						*/
						FindAndReplace(phonemes, L"A",L"a");
						FindAndReplace(phonemes, L"O",L"o");
						FindAndReplace(phonemes, L"u",L"u");
						FindAndReplace(phonemes, L"i",L"i");
						FindAndReplace(phonemes, L"{",L"a");
						FindAndReplace(phonemes, L"V",L"a");
						FindAndReplace(phonemes, L"E",L"e");
						FindAndReplace(phonemes, L"I",L"i");
						FindAndReplace(phonemes, L"U",L"u");
						FindAndReplace(phonemes, L"@",L"e");
						FindAndReplace(phonemes, L"r=",L"r");
						FindAndReplace(phonemes, L"aU",L"aw");
						FindAndReplace(phonemes, L"OI",L"oy");
						FindAndReplace(phonemes, L"@U",L"ow");
						FindAndReplace(phonemes, L"EI",L"ey");
						FindAndReplace(phonemes, L"AI",L"ay");
						FindAndReplace(phonemes, L"p",L"p");
						FindAndReplace(phonemes, L"t",L"t");
						FindAndReplace(phonemes, L"k",L"k");
						FindAndReplace(phonemes, L"b",L"b");
						FindAndReplace(phonemes, L"d",L"d");
						FindAndReplace(phonemes, L"g",L"g");
						FindAndReplace(phonemes, L"tS",L"C");
						FindAndReplace(phonemes, L"dZ",L"J");
						FindAndReplace(phonemes, L"f",L"f");
						FindAndReplace(phonemes, L"v",L"v");
						FindAndReplace(phonemes, L"T",L"t");
						FindAndReplace(phonemes, L"D",L"d");
						FindAndReplace(phonemes, L"s",L"s");
						FindAndReplace(phonemes, L"c",L"k");
						FindAndReplace(phonemes, L"z",L"z");
						FindAndReplace(phonemes, L"S",L"S");
						FindAndReplace(phonemes, L"Z",L"Z");
						FindAndReplace(phonemes, L"h",L"h");
						FindAndReplace(phonemes, L"l",L"l");
						FindAndReplace(phonemes, L"m",L"m");
						FindAndReplace(phonemes, L"n",L"n");
						FindAndReplace(phonemes, L"N",L"ng");
						FindAndReplace(phonemes, L"r",L"r");
						FindAndReplace(phonemes, L"w",L"w");
						FindAndReplace(phonemes, L"j",L"y");

						// the rest
						FindAndErase(phonemes, L' ');
						FindAndReplace(phonemes, L"'", L".");
						FindAndReplace(phonemes, L"-", L"#");
						FindAndReplace(phonemes, L"#.", L".");
						FindAndReplace(phonemes, L"##", L"#");

						sent_phonetics1.push_back(phonemes);
					}
				}
				else // token_lang != 'E'
				{
					// Hebrew

					/*
					%if word is OOV (out of vacabulary) - 3 stages process:
					%(stage 2 not implemnted here yet)
					%1)check if word apears W/O the prefix, and append (if possible)
					%3) if failed (1) & (2) - query G2P server(Transliterate before)
					%stage 1
					sent_phonetics1{wc}=[];
					prefix={'ב','ה','ו','כ','ל','מ','ש','כש'};
					prefix_phonetics={'be','ha','ve','ke','le','mi','Se','ke.Se'};
					place=find(strcmp(wordinhebrew(1),prefix));
					if place & findstr(wordinhebrew(1),prefix{place})==1%check that the first 1/2 charachters are a  prefix
						temp_sent_tokens=wordinhebrew(length(prefix{place})+1:end);%prefix removed
						cands=find(strcmp(temp_sent_tokens,dict_heb));%check if the shortened word has phonetics
						if  length(cands)>0
							if find(strcmp(pos_dummy(cands),'NN'))
								temp=find(strcmp(pos_dummy(cands),'NN'));
								body=dict_phonemes1(cands(temp(1)));%the (1) is becuase there might be SOME NN. safer to take the first only
							elseif find(strcmp(pos_dummy(cands),'NNT'))
								temp=find(strcmp(pos_dummy(cands),'NNT'));
								body=dict_phonemes1(cands(temp(1)));
							else
								body=dict_phonemes1(cands(1));
							end
							sent_phonetics1{wc}=[char(prefix_phonetics(place)) '#' char(body)];
							sent_phonetics1{wc}=strrep(sent_phonetics1{wc}, '#.','.');
						end
					end
					*/
					static const wchar_t *szszPrefix[] = {
						L"ה",
						L"מ",
						L"ב",
						L"ל",
						L"ש",
						L"ו",
						L"כ",
						L"מש",
						L"מכ",
						L"ומ",
						L"שמ",
						L"ול",
						L"כש",
						L"בש",
						L"וה",
						L"וב",
						L"שב",
						L"של",
						L"בכ",
						L"שכ",
						L"וש",
						L"לכ",
						L"וכ",
						L"וכש",
						L"ושל",
					};
					const int szszPrefixSize = sizeof(szszPrefix)/sizeof(szszPrefix[0]);

					static const wchar_t *szszPrefixPhonetics[] = {
						L"ha#",
						L"me#",
						L"be#",
						L"le#",
						L"Se#",
						L"ve#",
						L"ke#",
						L"mi#Se#",
						L"mi#ke#",
						L"ve#mi#",
						L"Se#me#",
						L"ve#le#",
						L"ke#Se#",
						L"be#Se#",
						L"ve#ha#",
						L"ve#be#",
						L"Se#be#",
						L"Se#le#",
						L"be#ke#",
						L"Se#ke#",
						L"ve#Se#",
						L"le#ke#",
						L"ve#ke#",
						L"vek#Se#",
						L"ve#Se#le#",
					};
					const int szszPrefixPhoneticsSize = sizeof(szszPrefixPhonetics)/sizeof(szszPrefixPhonetics[0]);

					std::wstring sent_phonetics1_entry;
					const wchar_t *pszWordInHebrew = wordinhebrew.c_str();

					//int nFoundPrefixIndex = -1;
					for (int prefixIndex = 0; prefixIndex < szszPrefixSize; prefixIndex++)
					{
						LOG(_T("get_phonetics4.13; prefixIndex=%d (prefix=%S)"), prefixIndex, szszPrefix[prefixIndex]);
						if (wcsncmp(pszWordInHebrew, szszPrefix[prefixIndex], wcslen(szszPrefix[prefixIndex])) == 0)
						{
							//nFoundPrefixIndex = prefixIndex;
							std::wstring temp_sent_tokens = wordinhebrew.substr(wcslen(szszPrefix[prefixIndex]));

							// re-use "it" (this if-else branch gets executed when the original "it" value is unusable).
							it = mainDict.find(temp_sent_tokens);
							if (it != mainDict.end())
							{
								MainDict::const_value firstEntry = *it;

								std::wstring strNNentry, strNNTentry;
								int nNNentryStatus = -1, nNNTentryStatus = -1;
								for (; it != mainDict.end(); it++)
								{
									MainDict::const_value entry = *it;
									if (wcscmp(entry.GetPos(), L"NN") == 0)
									{
										strNNentry = entry.GetPhonemes();
										nNNentryStatus = entry.m_nStatus;
									}
									else if (wcscmp(entry.GetPos(), L"NNT") == 0)
									{
										strNNTentry = entry.GetPhonemes();
										nNNTentryStatus = entry.m_nStatus;
									}
								}
								
								if (!strNNentry.empty())
								{
									sent_phonetics1_entry = strNNentry;
								}
								else if(!strNNTentry.empty())
								{
									sent_phonetics1_entry = strNNTentry;
								}
								else
								{
									// None of the above - take the first entry
									sent_phonetics1_entry = firstEntry.GetPhonemes();
								}

								sent_phonetics1_entry = (std::wstring)szszPrefixPhonetics[prefixIndex] + sent_phonetics1_entry;
								FindAndReplace(sent_phonetics1_entry, L"#.", L".");
							}

							// Stop looking for additional prefix matches only if we've got
							// something useful in the output, otherwise the algorithm will
							// resort to G2P calls.
							if (!sent_phonetics1_entry.empty())
								break;
						}
					}

					/*
					%stage2 - if last charchter is a tag (apostroph) - try to
					%search without the tag
					%stage3
					if isempty(sent_phonetics1{wc})%ie stage 1 failed
						tog2p=[tog2p, char([' ####' wordinhebrew '#### '])];
						%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%55
						%Elik's by pass - i dont want to waste time on VSTTS
						%g2p. i want the list of words
						%sent_phonetics1{wc}='gtop';
						word_2_g2p=char(transliterate2(wordinhebrew));
						word_2_g2p=strrep(word_2_g2p,'"','');
						url_full=['INPUT_TEXT=' word_2_g2p '&INPUT_TYPE=TEXT&OUTPUT_TYPE=PHONEMES&LOCALE=iw_IL&AUDIO=WAVE_FILE'];
						dos(['curl -s -d  "' url_full '" http://localhost:14376/process > g2p_res.txt']) ;
						a=textread('g2p_res.txt','%s','headerlines',4,'delimiter','\n');
						try
							p=findstr(a{1},'"');
							sent_phonetics1{wc}=a{1}(p(3)+1:p(4)-1);
							sent_phonetics1{wc}=strrep(sent_phonetics1{wc},' ','');
							sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'''','.');
							sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'-','#');
							sent_phonetics1{wc}=strrep(sent_phonetics1{wc}, '#.','.');
						catch
							sent_phonetics1{wc}='gtop';
						end
						%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
					end
					*/
					if (sent_phonetics1_entry.empty())
					{
						std::wstring word_2_g2p = transliterate2(wordinhebrew);
						LOG(_T("get_phonetics4.21; word_2_g2p=%S (%d chars)"), word_2_g2p.c_str(), word_2_g2p.length());
						FindAndErase(word_2_g2p, L'"');

						string phonemes = ToT(g2p.Syllabify(g2p.PredictPronunciation(ToUTF8(word_2_g2p))));
                                                
						/*
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc},' ','');
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'''','.');
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc},'-','#');
						sent_phonetics1{wc}=strrep(sent_phonetics1{wc}, '#.','.');
						*/

						FindAndErase(phonemes, L' ');
						FindAndReplace(phonemes, L"'", L".");
						FindAndReplace(phonemes, L"-", L"#");
						FindAndReplace(phonemes, L"#.", L".");

						sent_phonetics1_entry = phonemes;
					}

					if (!sent_phonetics1_entry.empty())
					{
						SplitAndAppend(sent_phonetics1, sent_phonetics1_entry);
					}
				}
			}
			else
			{
				// Found a dictionary entry
				if (it.size() == 1)
				{
					/*
					if length(temp)==1
						if has_diacritics
							sent_phonetics1{wc}=nikud4_for_Dmitry(sent_tokens{wc},dict_heb,dict_phonemes1)
						else
							sent_phonetics1(wc)=dict_phonemes1(temp);
						end
					*/
					if (has_diacritics)
					{
						string phonetics = handle_diacritics(mainDict, sent_token);
						if (!phonetics.empty())
							SplitAndAppend(sent_phonetics1, phonetics);
					}
					else
					{
						SplitAndAppend(sent_phonetics1, (*it).GetPhonemes());
					}
				}
				else
				{
					/*
					elseif length(temp>1)% multiple pronucniations found. this sentence will require the POS tagger
						%if a word has multiple pronunciations, 2 stages
						%stage 1 - handle diacritics - if there are (diacritics are
						%optional in hebrew)
						%stage 2 - leave a marking on this particular word so the
						%tagger can handle it. we dont employ the tagger now. we employ
						%it after we process the whole sentence
						if has_diacritics
							sent_phonetics1{wc}=nikud4_for_Dmitry(sent_tokens{wc},dict_heb,dict_phonemes1)
						else
							sent_easy=sent_easy*0;%sent easy as an indication if even one word has multiple pronunciation, meaning we will need either the tagger or the diacritics (or take the first alternative, as a fallback
							sent_phonetics1{wc}='replace_me';%dict_phonemes1(temp(1));
						end
					*/
					if (has_diacritics)
					{
						string phonetics = handle_diacritics(mainDict, sent_token);
						if (!phonetics.empty())
							SplitAndAppend(sent_phonetics1, phonetics);
					}
					else
					{
						sent_easy = 0;
						wchar_t szReplaceMeTag[100];
						swprintf(szReplaceMeTag, 100 , L"replace_me%d", wc);
						sent_phonetics1.push_back(szReplaceMeTag);
					}
				}
			}
		}
		else
		{
			// sent_phonetics1(wc)=sent_tokens(wc);%this should actually handle only punctuation marks and numbers
			sent_phonetics1.push_back(sent_token);
		}
	}
}

static void readnum(std::list<string>& list, const string& strNumber, const NumbersDict& map, const NumbersDict& numbersF)
{
	TCHAR *pszEndPtr = NULL;
	long nNumber = 0;

	if (strNumber.length() == 0)
		return;

	if (strNumber[0] == _T('0'))
	{
		for (int index = 0; index < (int)strNumber.length(); index++)
		{
			NumbersDict::const_iterator it = numbersF.find(strNumber.substr(index, 1));
			if (it != numbersF.end())
				SplitAndAppend(list, ToT(*it));
		}
	}
	else if ((nNumber = wcstol(strNumber.c_str(), &pszEndPtr, 10)) < 10000 && *pszEndPtr == _T('\0'))
	{
		NumbersDict::const_iterator it = numbersF.find(nNumber);
		if (it != numbersF.end())
			SplitAndAppend(list, ToT(*it));
	}
	else if (strNumber.length() == 5 || strNumber.length() == 6)
	{
		if (strNumber.length() == 5 && strNumber.substr(0, 2) == _T("10"))
		{
			list.push_back(_T("Aa.se#ret"));
			list.push_back(_T("Aa#la.fim"));
		}
		else
		{
			unsigned long nNumber = (unsigned long)floor(wcstol(strNumber.c_str(), &pszEndPtr, 10) / 1000.0);
			NumbersDict::const_iterator it = numbersF.find(nNumber);
			if (it != numbersF.end())
				SplitAndAppend(list, ToT(*it) + _T(" .Ae#lef"));
		}

		unsigned long nNumberMod = wcstol(strNumber.c_str(), &pszEndPtr, 10) % 1000;
		if (nNumberMod > 0 && nNumberMod < 100)
			list.push_back(_T("ve"));

		if (nNumberMod > 0)
		{
			NumbersDict::const_iterator it = map.find(nNumberMod);
			if (it != map.end())
				SplitAndAppend(list, ToT(*it));
		}
	}
	else
	{
		for (int index = 0; index < (int)strNumber.length(); index++)
		{
			NumbersDict::const_iterator it = numbersF.find(strNumber.substr(index, 1));
			if (it != numbersF.end())
				SplitAndAppend(list, ToT(*it));
		}
	}
}

static void get_numbers1(const NumbersDict& numbersM, const NumbersDict& numbersF, std::vector<string>& sent_phonetics2)
{
	const TCHAR *pszLegitSeparators = _T("\\/:-");
	const TCHAR *szszMonth[] = {
		_T("le.ya#nuar"),
		_T("le.feb#ru#ar"),
		_T("'le.merM"),
		_T("le#Aap.ril"),
		_T("le.may"),
		_T("le.yu#ni"),
		_T("le.yu#li"),
		_T("le.Ao#gust"),
		_T("le#sep.tem#ber"),
		_T("le#Aok.to#ber"),
		_T("le#no.vem#ber"),
		_T("le#de.Mem#ber")
	};

	for (std::vector<string>::size_type index = 0; index < sent_phonetics2.size(); index++)
	{
		string& phonemes = sent_phonetics2[index];
		if (phonemes.find_first_of(_T("0123456789")) != string::npos)
		{
			int nFirstSlashIndex = phonemes.find(_T("/"));
			int nSecondSlashIndex = 0;
			if (nFirstSlashIndex != string::npos
				&& (nSecondSlashIndex = phonemes.find(_T("/"), nFirstSlashIndex + 1)) != string::npos)
			{
				string strLH = phonemes.substr(0, nFirstSlashIndex);
				string strRH = phonemes.substr(nFirstSlashIndex + 1, nSecondSlashIndex - nFirstSlashIndex - 1);

				wchar_t *pszEndPtr = NULL;
				long nLH = wcstol(strLH.c_str(), &pszEndPtr, 10);
				long nRH = wcstol(strRH.c_str(), &pszEndPtr, 10);

				std::list<string> lstTemp;

				if (nRH <= 12 && nLH < 32 && nRH > 0 && nLH > 0) // DD/MM
				{
					NumbersDict::const_iterator it = numbersM.find(nLH);
					if (it != numbersM.end())
						SplitAndAppend(lstTemp, ToT(*it));

					lstTemp.push_back(szszMonth[nRH - 1]);

					string strYear = phonemes.substr(nSecondSlashIndex + 1);
					if (strYear.length() == 2)
						strYear = (string)_T("20") + strYear;
					it = numbersF.find(strYear);
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));
				}
				else if(nRH < 32 && nLH <= 12 && nRH > 0 && nLH > 0) // MM/DD
				{
					NumbersDict::const_iterator it = numbersM.find(nRH);
					if (it != numbersM.end())
						SplitAndAppend(lstTemp, ToT(*it));

					lstTemp.push_back(szszMonth[nLH - 1]);

					string strYear = phonemes.substr(nSecondSlashIndex + 1);
					if (strYear.length() == 2)
						strYear = (string)_T("20") + strYear;
					it = numbersF.find(strYear);
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));
				}
				else // read digit-by-digit
				{
					readnum(lstTemp, strLH, numbersF, numbersF);
					lstTemp.push_back(_T(" kav na.tuy "));
					readnum(lstTemp, strRH, numbersF, numbersF);
					lstTemp.push_back(_T(" kav na.tuy "));
					readnum(lstTemp, phonemes.substr(nSecondSlashIndex + 1), numbersF, numbersF);
				}

				sent_phonetics2.erase(sent_phonetics2.begin() + index);
				if (lstTemp.empty())
				{
					--index;
				}
				else
				{
					for (std::list<string>::const_iterator it = lstTemp.begin(); it != lstTemp.end(); it++)
					{
						sent_phonetics2.insert(sent_phonetics2.begin() + index, *it);
						++index;
					}
				}
				continue;
			}

			int nFirstDashIndex = phonemes.find(_T("-"));
			int nSecondDashIndex = 0;
			if (nFirstDashIndex != string::npos
				&& (nSecondDashIndex = phonemes.find(_T("-"), nFirstDashIndex + 1)) != string::npos)
			{
				std::list<string> lstTemp;
				for (string::size_type charIndex = 0; charIndex < phonemes.length(); charIndex++)
				{
					if (phonemes[charIndex] != _T('-'))
						readnum(lstTemp, phonemes.substr(charIndex, 1), numbersF, numbersF);
				}

				sent_phonetics2.erase(sent_phonetics2.begin() + index);
				if (lstTemp.empty())
				{
					--index;
				}
				else
				{
					for (std::list<string>::const_iterator it = lstTemp.begin(); it != lstTemp.end(); it++)
					{
						sent_phonetics2.insert(sent_phonetics2.begin() + index, *it);
						++index;
					}
				}
				continue;
			}

			int nFirstColonIndex = phonemes.find(_T(":"));
			int nSecondColonIndex = 0;
			if (nFirstColonIndex != string::npos
				&& (nSecondColonIndex = phonemes.find(_T(":"), nFirstColonIndex + 1)) != string::npos)
			{
				int nDashIndex;
				if ((nDashIndex = phonemes.find(_T("-"))) == string::npos)
				{
					std::list<string> lstTemp;

					NumbersDict::const_iterator it = numbersF.find(phonemes.substr(0, nFirstColonIndex));
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));

					it = numbersF.find(phonemes.substr(nFirstColonIndex + 1, nSecondColonIndex - nFirstColonIndex - 1));
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));

					it = numbersF.find(phonemes.substr(nSecondColonIndex + 1));
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));

					sent_phonetics2.erase(sent_phonetics2.begin() + index);
					if (lstTemp.empty())
					{
						--index;
					}
					else
					{
						for (std::list<string>::const_iterator it = lstTemp.begin(); it != lstTemp.end(); it++)
						{
							sent_phonetics2.insert(sent_phonetics2.begin() + index, *it);
							++index;
						}
					}
					continue;
				}
				else if (nDashIndex > nFirstColonIndex && nDashIndex < nSecondColonIndex)
				{
					std::list<string> lstTemp;

					NumbersDict::const_iterator it = numbersF.find(phonemes.substr(0, nFirstColonIndex));
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));

					unsigned long nNumber1 = atol(ToUTF8(phonemes.substr(nFirstColonIndex + 1, nDashIndex - nFirstColonIndex - 1)).c_str());
					if (nNumber1 > 0)
					{
						string strTemp = _T("ve#");

						it = numbersF.find(nNumber1);
						if (it != numbersF.end())
							strTemp.append(ToT(*it));

						SplitAndAppend(lstTemp, strTemp);
						lstTemp.push_back(_T("da.kot"));
					}
					else
					{
						lstTemp.push_back(_T(".Ae#fes"));
						lstTemp.push_back(_T(".Ae#fes"));
					}

					// Remove zero left-padding (if any).
					lstTemp.push_back(_T("Aad"));
					it = numbersF.find(phonemes.substr(nDashIndex + 1, nSecondColonIndex - nDashIndex - 1));
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));

					unsigned long nNumber2 = atol(ToUTF8(phonemes.substr(nSecondColonIndex + 1)).c_str());
					if (nNumber2 > 0)
					{
						string strTemp = _T("ve#");

						it = numbersF.find(nNumber2);
						if (it != numbersF.end())
							strTemp.append(ToT(*it));

						SplitAndAppend(lstTemp, strTemp);
						lstTemp.push_back(_T("da.kot"));
					}
					else
					{
						lstTemp.push_back(_T(".Ae#fes"));
						lstTemp.push_back(_T(".Ae#fes"));
					}

					sent_phonetics2.erase(sent_phonetics2.begin() + index);
					if (lstTemp.empty())
					{
						--index;
					}
					else
					{
						for (std::list<string>::const_iterator it = lstTemp.begin(); it != lstTemp.end(); it++)
						{
							sent_phonetics2.insert(sent_phonetics2.begin() + index, *it);
							++index;
						}
					}
					continue;
				}
			}

			if (CountNonDigits(phonemes) == 1
				&& (CountChars(phonemes, _T(',')) != 0
					|| CountChars(phonemes, _T(':')) == 1
					|| CountChars(phonemes, _T('.')) == 1
					|| CountChars(phonemes, _T('/')) == 1
					|| CountChars(phonemes, _T('-')) == 1))
			{
				bool done = false;
				std::list<string> lstTemp;

				if (CountChars(phonemes, _T(':')) > 0 && phonemes.length() <= 5)
				{
					int nColonIndex = phonemes.find(_T(':'));

					string strLH = phonemes.substr(0, nColonIndex);
					string strRH = phonemes.substr(nColonIndex + 1);

					std::string strLHc = ToUTF8(strLH);
					std::string strRHc = ToUTF8(strRH);

					long nLH = atol(strLHc.c_str());
					long nRH = atol(strRHc.c_str());
					
					NumbersDict::const_iterator it = numbersF.find(nLH);
					if (it != numbersF.end())
						SplitAndAppend(lstTemp, ToT(*it));

					if (nLH < 25 && nRH < 60)
					{
						if (nRH > 0)
						{
							std::wstring strTemp = _T("ve#");
							
							it = numbersF.find(nRH);
							if (it != numbersF.end())
								strTemp.append(ToT(*it));

							SplitAndAppend(lstTemp, strTemp);
							lstTemp.push_back(_T("da.kot"));
						}
						else
						{
							lstTemp.push_back(_T(".Ae#fes"));
							lstTemp.push_back(_T(".Ae#fes"));
						}
					}
					else
					{
						lstTemp.push_back(_T("ne#ku#do.ta#yim"));

						it = numbersF.find(nRH);
						if (it != numbersF.end())
							SplitAndAppend(lstTemp, ToT(*it));
					}

					done = true;
				}

				if (CountChars(phonemes, _T('/')) == 1)
				{
					int nSlashIndex = phonemes.find(_T('/'));

					string strLH = phonemes.substr(0, nSlashIndex);
					string strRH = phonemes.substr(nSlashIndex + 1);

					std::string strLHc = ToUTF8(strLH);
					std::string strRHc = ToUTF8(strRH);

					long nLH = atol(strLHc.c_str());
					long nRH = atol(strRHc.c_str());

					if (nRH > 12 && nRH < 32 && nLH >= 1 && nLH <= 12)
					{
						NumbersDict::const_iterator it = numbersM.find(nRH);
						if (it != numbersM.end())
							SplitAndAppend(lstTemp, ToT(*it));

						lstTemp.push_back(szszMonth[nLH - 1]);
					}
					else if (nRH >= 1 && nRH <= 12 && nLH >= 1 && nLH < 32)
					{
						NumbersDict::const_iterator it = numbersM.find(nLH);
						if (it != numbersM.end())
							SplitAndAppend(lstTemp, ToT(*it));

						lstTemp.push_back(szszMonth[nRH - 1]);
					}
					else
					{
						readnum(lstTemp, strLH, numbersF, numbersF);
						lstTemp.push_back(_T("kav"));
						lstTemp.push_back(_T("na.tuy"));
						readnum(lstTemp, strRH, numbersF, numbersF);
					}

					done = true;
				}

				if (!done && CountChars(phonemes, _T('-')) == 1 && CountChars(phonemes, _T(':')) == 0)
				{
					if (phonemes[0] == _T('0'))
					{
						for (string::size_type index = 0; index < phonemes.length(); index++)
						{
							if (phonemes[index] == _T('-'))
								continue;

							readnum(lstTemp, phonemes.substr(index, 1), numbersF, numbersF);
						}
					}
					else
					{
						int nSlashIndex = phonemes.find(_T('/'));
						readnum(lstTemp, phonemes.substr(0, nSlashIndex), numbersF, numbersF);
						lstTemp.push_back(_T("ma.kaf"));
						readnum(lstTemp, phonemes.substr(nSlashIndex + 1), numbersF, numbersF);
					}
				}

				if (lstTemp.empty())
				{
					string::size_type nSeparatorIndex = phonemes.find_first_not_of(_T("0123456789"));
					readnum(lstTemp, phonemes.substr(0, nSeparatorIndex), numbersF, numbersF);
					readnum(lstTemp, phonemes.substr(nSeparatorIndex + 1), numbersF, numbersF);
				}

				sent_phonetics2.erase(sent_phonetics2.begin() + index);
				for (std::list<string>::const_iterator it = lstTemp.begin(); it != lstTemp.end(); it++)
				{
					sent_phonetics2.insert(sent_phonetics2.begin() + index, *it);
					++index;
				}
				--index;
				continue;
			}
			else if (CountNonDigits(phonemes) == 0)
			{
				std::list<string> lstTemp;

				readnum(lstTemp, phonemes, numbersF, numbersF);

				sent_phonetics2.erase(sent_phonetics2.begin() + index);

				--index;

				for (std::list<string>::const_iterator it = lstTemp.begin(); it != lstTemp.end(); it++)
				{
					++index;
					sent_phonetics2.insert(sent_phonetics2.begin() + index, *it);
				}
				continue;
			}
			else if (CountNonDigits(phonemes) > 1)
			{
				int nPrevSeparatorIndex = -1, nNextSeparatorIndex = -1;
				std::list<string> lstTemp;

				while ((nNextSeparatorIndex = phonemes.find_first_not_of(_T("0123456789"), nPrevSeparatorIndex + 1)) != string::npos)
				{
					readnum(lstTemp, phonemes.substr(nPrevSeparatorIndex + 1, nNextSeparatorIndex - nPrevSeparatorIndex - 1), numbersF, numbersF);

					const TCHAR chSeparator = phonemes[nNextSeparatorIndex];

					switch(chSeparator)
					{
					case _T('/'):
						lstTemp.push_back(_T("kav"));
						lstTemp.push_back(_T("na.tuy"));
						break;
					case _T('\\'):
						lstTemp.push_back(_T("kav"));
						lstTemp.push_back(_T("na.tuy"));
						break;
					case _T(':'):
						lstTemp.push_back(_T("ne#ku#do.tay#im"));
						break;
					case _T(';'):
						lstTemp.push_back(_T("ne#ku.da"));
						lstTemp.push_back(_T("psik"));
						break;
					case _T('-'):
						lstTemp.push_back(_T("ma.kaf"));
						break;
					}

					nPrevSeparatorIndex = nNextSeparatorIndex;
				}

				readnum(lstTemp, phonemes.substr(nPrevSeparatorIndex + 1), numbersF, numbersF);

				sent_phonetics2.erase(sent_phonetics2.begin() + index);
				if (lstTemp.empty())
				{
					--index;
				}
				else
				{
					for (std::list<string>::const_iterator it = lstTemp.begin(); it != lstTemp.end(); it++)
					{
						sent_phonetics2.insert(sent_phonetics2.begin() + index, *it);
						++index;
					}
					--index;
				}
			}
		}
	}
}

bool CTTSEngine::GetPhonemes(const CSentenceFragment& frag, std::vector<string>& lstPhonetics) const
{
	CStopWatch stopwatch(_T("GetPhonemes"));

	const CTokens& tokens = frag.m_tokens;

	int sent_easy = 1;
	get_phonetics4(*m_pDict, *m_pG2P, tokens, lstPhonetics, sent_easy);

	if (!sent_easy)
	{
		std::wstring request = L"";
		for (CTokens::const_iterator it = tokens.begin(); it != tokens.end(); it++)
		{
			std::wstring token = (*it).m_strText;

			if (token.length() > 0)
			{
				__int64_t charSum = 0;
				for (int charIndex = 0; charIndex < (int)token.length(); charIndex++)
					charSum += token[charIndex];
				__int64_t tokenMean = charSum / token.length();

				if (tokenMean > 400)
				{
					FindAndReplace(token, L"א", L"a");
					FindAndReplace(token, L"ב", L"b");
					FindAndReplace(token, L"ג'", L"J");
					FindAndReplace(token, L"ג", L"g");
					FindAndReplace(token, L"ד", L"d");
					FindAndReplace(token, L"ה", L"h");
					FindAndReplace(token, L"ו", L"w");
					FindAndReplace(token, L"ז", L"z");
					FindAndReplace(token, L"ז'", L"Z");
					FindAndReplace(token, L"ח", L"x");
					FindAndReplace(token, L"ט", L"v");
					FindAndReplace(token, L"י", L"i");
					FindAndReplace(token, L"כ", L"k");
					FindAndReplace(token, L"ך", L"k");
					FindAndReplace(token, L"ל", L"l");
					FindAndReplace(token, L"מ", L"m");
					FindAndReplace(token, L"ם", L"m");
					FindAndReplace(token, L"נ", L"n");
					FindAndReplace(token, L"ן", L"n");
					FindAndReplace(token, L"ס", L"s");
					FindAndReplace(token, L"ע", L"y");
					FindAndReplace(token, L"פ", L"p");
					FindAndReplace(token, L"ף", L"p");
					FindAndReplace(token, L"צ'", L"C");
					FindAndReplace(token, L"ץ'", L"C");
					FindAndReplace(token, L"צ", L"c");
					FindAndReplace(token, L"ץ", L"c");
					FindAndReplace(token, L"ק", L"q");
					FindAndReplace(token, L"ר", L"r");
					FindAndReplace(token, L"ש", L"e");
					FindAndReplace(token, L"ת'", L"t");
					FindAndReplace(token, L"ת", L"t");
				}
			}

			if (!request.empty())
				request.append(L" ");

			request.append(token);
		}

		strings lstStringTokens = Tokenize(ToT(request), _T(" \t"));
		strings lstPartsOfSpeech = m_pTagger->tag(lstStringTokens);

		for (int index = lstPhonetics.size() - 1; index >= 0; index--)
		{
			if (lstPhonetics[index].find(_T("replace_me")) != std::wstring::npos)
			{
				int tokenIndex = atol(utf16_to_utf8(lstPhonetics[index]).c_str() + wcslen(_T("replace_me")));

				// The placeholder has to go.
				lstPhonetics.erase(lstPhonetics.begin() + index);

				MainDict::const_iterator it = m_pDict->find(tokens[tokenIndex].m_strText);
				if (it == m_pDict->end())
				{
					// Shouldn't happen by design
					continue;
				}

				MainDict::const_value firstEntry = *it;
				string foundPosWord;

				const string& pos = lstPartsOfSpeech.at(tokenIndex);

				for (; it != m_pDict->end(); it++)
				{
					if (wcscmp((wchar_t*)(*it).GetPos(), pos.c_str()) == 0)
					{
						foundPosWord = (*it).GetPhonemes();
						break;
					}
				}

				// Ensure that something will be placed into the output
				// even if the server response is badly mangled or the
				// dictionary does not contain a correct POS marker.
				if (foundPosWord.empty())
				{
					foundPosWord = firstEntry.GetPhonemes();
				}

				std::vector<string> temp;
				SplitAndAppend(temp, foundPosWord);

				for (int tempIndex = temp.size() - 1; tempIndex >= 0; tempIndex--)
					lstPhonetics.insert(lstPhonetics.begin() + index, temp[tempIndex]);
			}
		}
	}

	get_numbers1(*m_pNumbersM, *m_pNumbersF, lstPhonetics);

	return true;
}
