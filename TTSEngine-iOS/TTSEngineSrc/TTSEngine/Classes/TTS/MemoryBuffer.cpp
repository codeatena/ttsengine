#include "MemoryBuffer.h"

#include <stdio.h>
//#include <malloc.h>
#include <stdlib.h>
#include <string.h>

#include <new>

#include "HTS_hidden.h"

#ifndef MEMORY_BUFFER_WINDOW
#define MEMORY_BUFFER_WINDOW 65536
#endif

CMemoryBuffer::CMemoryBuffer()
	: m_pBuffer(NULL)
	, m_nSize(0)
	, m_nOffset(0)
{
}

CMemoryBuffer::~CMemoryBuffer()
{
	if (m_pBuffer != NULL)
		free(m_pBuffer);
}

void CMemoryBuffer::Write(const void *pMemPtr, size_t nSize, size_t nCount)
{
	size_t nWriteLength = nSize * nCount;

	if (m_nSize - m_nOffset < nWriteLength)
	{
		size_t nWindowMultiple = (nWriteLength - (m_nSize - m_nOffset) + MEMORY_BUFFER_WINDOW - 1) / MEMORY_BUFFER_WINDOW;
		m_nSize += nWindowMultiple * MEMORY_BUFFER_WINDOW;
		void *pNewBuffer = realloc(m_pBuffer, m_nSize);
		if (pNewBuffer == NULL)
			throw std::bad_alloc();
		m_pBuffer = pNewBuffer;
		memset((char *)m_pBuffer + m_nOffset, 0, m_nSize - m_nOffset);
	}

	memmove((char *)m_pBuffer + m_nOffset, pMemPtr, nWriteLength);

	m_nOffset += nWriteLength;
}

void CMemoryBuffer::WriteLE(const void *pMemPtr, size_t nSize, size_t nCount)
{
	const size_t block = nCount * nSize;

#ifdef WORDS_BIGENDIAN
	HTS_byte_swap(pMemPtr, nSize, block);
#endif                          /* WORDS_BIGENDIAN */
	Write(pMemPtr, nSize, nCount);
}
