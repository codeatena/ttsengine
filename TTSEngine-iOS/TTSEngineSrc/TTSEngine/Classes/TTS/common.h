#pragma once

#include "platform.h"

#include <stdio.h>

#include <string>
#include <vector>
#include <list>
#include <algorithm>
#include <map>

#ifdef _UNICODE
typedef std::wstring string;
#define ToT(s) ToUTF16(s)
#else
typedef std::string string;
#define ToT(s) ToUTF8(s)
#endif

#include "StopWatch.h"

inline std::wstring ToUTF16(const std::string& str) { return utf8_to_utf16(str); }
inline std::wstring ToUTF16(const std::wstring& str) { return str; }
inline std::string ToUTF8(const std::string& str) { return str; }
inline std::string ToUTF8(const std::wstring& str) { return utf16_to_utf8(str); }

inline std::vector<std::wstring> Tokenize(const std::wstring& strText, const wchar_t *pszDelimiters, bool squashEmpty=true)
{
	std::vector<std::wstring> lstResult;

	std::wstring::size_type nNextPos, nPrevPos = 0;
	while ((nNextPos = strText.find_first_of(pszDelimiters, nPrevPos)) != std::wstring::npos)
	{
		if (nNextPos > nPrevPos || !squashEmpty)
		{
			lstResult.push_back(strText.substr(nPrevPos, nNextPos - nPrevPos));
		}

		nPrevPos = nNextPos + 1;
	}

	if (!squashEmpty || wcschr(pszDelimiters, strText[nPrevPos]) == NULL)
		lstResult.push_back(strText.substr(nPrevPos));

	return lstResult;
}

inline std::vector<std::string> Tokenize(const std::string& strText, const char *pszDelimiters, bool squashEmpty=true)
{
	std::vector<std::string> lstResult;

	std::string::size_type nNextPos, nPrevPos = 0;
	while ((nNextPos = strText.find_first_of(pszDelimiters, nPrevPos)) != std::string::npos)
	{
		if (nNextPos > nPrevPos || !squashEmpty)
		{
			lstResult.push_back(strText.substr(nPrevPos, nNextPos - nPrevPos));
		}

		nPrevPos = nNextPos + 1;
	}

	if (!squashEmpty || strchr(pszDelimiters, strText[nPrevPos]) == NULL)
		lstResult.push_back(strText.substr(nPrevPos));

	return lstResult;
}

inline std::wstring& FindAndErase(std::wstring& str, wchar_t chFind)
{
	wchar_t szPattern[2] = {chFind};

	int nPos = 0;
	while ((nPos = str.find(szPattern, nPos)) != std::wstring::npos)
	{
		str.erase(nPos, 1);
	}

	return str;
}

inline std::string& FindAndReplace(std::string& str, LPCSTR pszPattern, LPCSTR pszNewString)
{
    int nNewLen = strlen(pszNewString), nPos = -nNewLen, nLen = strlen(pszPattern);
    while ((nPos = str.find(pszPattern, nPos + nNewLen)) != std::string::npos)
    {
        str.replace(nPos, nLen, pszNewString);
    }
    return str;
}

inline std::wstring& FindAndReplace(std::wstring& str, LPCWSTR pszPattern, LPCWSTR pszNewString)
{
	int nNewLen = wcslen(pszNewString), nPos = -nNewLen, nLen = wcslen(pszPattern);
	while ((nPos = str.find(pszPattern, nPos + nNewLen)) != std::wstring::npos)
	{
		str.replace(nPos, nLen, pszNewString);
	}
	return str;
}

inline std::wstring& FindAndReplace(std::wstring& str, wchar_t chFind, wchar_t chReplace)
{
	wchar_t szPattern[2] = {chFind};
	wchar_t szNewString[2] = {chReplace};
	return FindAndReplace(str, szPattern, szNewString);
}

inline void ReplaceWith(std::wstring& str, std::wstring::size_type nOffset, std::wstring::size_type nLength, const wchar_t *pszNewString)
{
	str.replace(nOffset, nLength, pszNewString);
}

inline void InsertAt(std::wstring& strText, std::wstring::size_type nOffset, const wchar_t *pszInsert)
{
	strText.insert(nOffset, pszInsert);
}

inline void EraseAt(std::wstring& str, std::wstring::size_type nOffset, std::wstring::size_type nLength)
{
	str.erase(nOffset, nLength);
}

inline size_t CountNonDigits(const string& str)
{
	size_t nResult = 0;
	for (string::size_type index = 0, length = str.length(); index < length; index++)
	{
		if (!isdigit(str[index]))
			++nResult;
	}
	return nResult;
}

inline size_t CountChars(const string& str, TCHAR ch)
{
	size_t nResult = 0;
	for (string::size_type index = 0, length = str.length(); index < length; index++)
	{
		if (str[index] == ch)
			nResult++;
	}
	return nResult;
}

inline size_t CountChars(const std::wstring& str, wchar_t chMin, wchar_t chMax)
{
	int nResult = 0;
	for (size_t index = 0, length = str.length(); index < length; index++)
	{
		if (str[index] >= chMin && str[index] <= chMax)
			nResult++;
	}
	return nResult;
}

template<class T>
inline void SplitAndAppend(T& list, const string& str)
{
	int nPrevSpacePos = -1, nNextSpacePos;
	while ((nNextSpacePos = str.find(L' ', nPrevSpacePos + 1)) != string::npos)
	{
		if (nNextSpacePos - nPrevSpacePos - 1 > 0)
		{
			list.push_back(str.substr(nPrevSpacePos + 1, nNextSpacePos - nPrevSpacePos - 1));
		}
		nPrevSpacePos = nNextSpacePos;
	}

	if (nPrevSpacePos > 0)
	{
		if (nPrevSpacePos < (int)str.length() - 1)
			list.push_back(str.substr(nPrevSpacePos + 1));
	}
	else
		list.push_back(str);
}

inline std::string ToString(unsigned long value)
{
	char szBuffer[64];
	sprintf(szBuffer, "%lu", value);
	return szBuffer;
}
