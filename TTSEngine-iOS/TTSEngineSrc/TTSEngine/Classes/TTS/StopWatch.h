#pragma once

#include <time.h>

class CStopWatch
{
public:
	CStopWatch(LPCTSTR pszCaption)
		: m_strCaption(pszCaption)
		, m_bRunning(true)
    {
#ifdef ENABLE_STOPWATCH
		FILETIME ft;
		GetSystemTimeAsFileTime(&ft);

		m_liBegin.HighPart = ft.dwHighDateTime;
		m_liBegin.LowPart = ft.dwLowDateTime;
#endif
        
        m_dStart = clock();
	}

	~CStopWatch()
	{
#ifdef ENABLE_STOPWATCH
		if (m_bRunning)
		{
             
            
			FILETIME ft;
			GetSystemTimeAsFileTime(&ft);

			LARGE_INTEGER liEnd;
			liEnd.HighPart = ft.dwHighDateTime;
			liEnd.LowPart = ft.dwLowDateTime;
            

			printf(_T("%s: %lldms\n"), m_strCaption.c_str(), (liEnd.QuadPart - m_liBegin.QuadPart) / CLOCKS_PER_SEC);
		}
#endif
        
        if (m_bRunning) {
            clock_t tEnd = clock();
            printf("%S: %lu ms\n", m_strCaption.c_str(), (m_dStart - tEnd)/CLOCKS_PER_SEC);
        }
        
	}

	void Reset(LPCTSTR pszCaption)
	{
#ifdef ENABLE_STOPWATCH
		FILETIME ft;
		GetSystemTimeAsFileTime(&ft);

		LARGE_INTEGER liEnd;
		liEnd.HighPart = ft.dwHighDateTime;
		liEnd.LowPart = ft.dwLowDateTime;

		if (m_bRunning)
		{
			printf(_T("%s: %lldms\n"), m_strCaption.c_str(), (liEnd.QuadPart - m_liBegin.QuadPart) / 10000);
		}

		m_liBegin.QuadPart = liEnd.QuadPart;
		m_strCaption = pszCaption;
		m_bRunning = TRUE;
#endif
        
        m_dStart = clock();
        
        if (m_bRunning) {
            clock_t tEnd = clock();
            printf("%S: %lu ms\n", m_strCaption.c_str(), (m_dStart - tEnd)/CLOCKS_PER_SEC);
        }
        
        m_strCaption = pszCaption;
		m_bRunning = true;
        
	}

	void Stop()
	{
#ifdef ENABLE_STOPWATCH
		if (!m_bRunning)
			return;

		FILETIME ft;
		GetSystemTimeAsFileTime(&ft);

		LARGE_INTEGER liEnd;
		liEnd.HighPart = ft.dwHighDateTime;
		liEnd.LowPart = ft.dwLowDateTime;

		printf(_T("%s: %lldms\n"), m_strCaption.c_str(), (liEnd.QuadPart - m_liBegin.QuadPart) / 10000);
		
		m_bRunning = FALSE;
#endif
        if (!m_bRunning)
			return;
        
        if (m_bRunning) {
            clock_t tEnd = clock();
            printf("%S: %lu ms\n", m_strCaption.c_str(), (m_dStart - tEnd)/10000);
        }
        
        m_bRunning = false;
	}

private:
#ifdef ENABLE_STOPWATCH
	string			m_strCaption;
    long            m_liBegin;
	bool			m_bRunning;
#endif
    string  m_strCaption;
    bool	m_bRunning;
    clock_t  m_dStart;
};
