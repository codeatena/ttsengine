#pragma once

class MainDict;
class NumbersDict;
class CTrainedLTS;
class CPOSModel;
class CPOSTaggerME;
struct _HTS_Engine;
typedef struct _HTS_Engine HTS_Engine;
class CMemoryBuffer;

#ifndef DEFAULT_SAMPLING_RATE
#define DEFAULT_SAMPLING_RATE 48000
#endif

#ifndef DEFAULT_SPEECH_RATE
#define DEFAULT_SPEECH_RATE 1.0
#endif

class CTTSEngine
{
public:
	CTTSEngine(size_t nSamplingRate=DEFAULT_SAMPLING_RATE, double fSpeechRate=DEFAULT_SPEECH_RATE);
	~CTTSEngine();

	// Perform initialization of the TTS Engine instance - load the main dictionary, numeric dictionaries, etc.
	bool Configure(const string& strDataPath, const string& strVoicePath);

	// Parse the specified input text (multiple sentences, mixed languages) onto fragments that are comprised of
	// multiple words of the same language.
	bool ParseText(const string& strText, CSentenceFragments& lstFragments) const;

	// Given a sentence fragment (output of CTTSEngine::ParseText), produce corresponding phonemes.
	bool GetPhonemes(const CSentenceFragment& frag, std::vector<string>& lstPhonetics) const;

	// Given a list of phonemes (output of CTTSEngine::GetPhonemes), produce a list of LAB format
	// strings understood by HTS Engine. Note that (by design) GetPhonemes() takes the T-string in
	// its lstPhonetics parameter, while Convert2HTS() requires a C-string. Use ToUTF8() to convert
	// from T-string to C-string.
	std::list<std::string> Convert2HTS(const std::string& strPhonetics) const;
	void SynthesizeRAW(const std::list<std::string>& lstLab, CMemoryBuffer& memoryBuffer);
	void SynthesizeWAV(const std::list<std::string>& lstLab, CMemoryBuffer& memoryBuffer);

private:
	MainDict	*m_pDict;
	NumbersDict	*m_pNumbersM;
	NumbersDict	*m_pNumbersF;

	CTrainedLTS		*m_pG2P;

	CPOSModel		*m_pModel;
	CPOSTaggerME	*m_pTagger;

	size_t		m_nSamplingRate;
	double		m_fSpeechRate;
	HTS_Engine	*m_pHTSEngine;
};
