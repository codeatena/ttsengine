#include "POScommon.h"
#include "POSModel.h"

CPOSModel::CPOSModel(LPCTSTR pszDirectoryName)
{
	string strDirectoryName = pszDirectoryName;
	if (!strDirectoryName.empty() && strDirectoryName[0] == _T('"'))
		strDirectoryName.erase(0, 1);
	if (!strDirectoryName.empty() && strDirectoryName[strDirectoryName.length() - 1] == _T('"'))
		strDirectoryName.erase(strDirectoryName.length() - 1, 1);
	if (!strDirectoryName.empty() && strDirectoryName[strDirectoryName.length() - 1] != _T('\\'))
		strDirectoryName += _T("\\");
	
	LoadManifest(strDirectoryName + _T("manifest.properties"));
	LoadModel(strDirectoryName + _T("pos.model"));
}

CPOSModel::CPOSModel(const string& strManifestFile, const string& strModelFile)
{
	LoadManifest(strManifestFile);
	LoadModel(strModelFile);
}

CPOSModel::~CPOSModel()
{
}

#define sprintf_s(buffer, buffer_size, stringbuffer, ...) (sprintf(buffer, stringbuffer, __VA_ARGS__))

void CPOSModel::LoadManifest(const string& strManifestName)
{
	std::ifstream ifs( T2C(strManifestName).c_str());
	if (!ifs)
	{
		char szBuffer[512] = {0};
		sprintf_s(szBuffer, sizeof(szBuffer), "File '%s' not found", T2C(strManifestName).c_str());
		throw szBuffer;
	}

	while(!ifs.eof())
	{
		char szLine[2048] = {0};
		ifs.getline(szLine, sizeof(szLine));
		while (szLine[0] != 0 && strchr("\r\n", szLine[strlen(szLine) - 1]) != NULL)
			szLine[strlen(szLine) - 1] = 0;

		if (szLine[0] == 0)
			continue;
		if (szLine[0] == '#')
			continue;

		string strLine = C2T(szLine);
		string::size_type pos = strLine.find(_T('='));
		if (pos == string::npos)
			continue;

		string strPropertyName = strLine.substr(0, pos);
		string strPropertyValue = strLine.substr(pos + 1);

		m_manifest[strPropertyName] = strPropertyValue;
	}
}

void CPOSModel::LoadModel(const string& strModelName)
{
       
	std::ifstream ifs(T2C(strModelName).c_str(), std::ifstream::in|std::ifstream::binary);
	if (!ifs)
	{
		char szBuffer[512] = {0};
		sprintf_s(szBuffer, sizeof(szBuffer), "File '%s' not found", T2C(strModelName).c_str());
		throw szBuffer;
	}

    string strModelType = ReadUtf8(ifs);
    
    if (strModelType != _T("GIS"))
		throw new string(_T("Non-GIS model types are not supported"));

	int32_t nCorrectionConstant = ReadInt32(ifs);
	double fCorrectionParam = ReadDouble(ifs);

	int32_t nOutcomesNumber = ReadInt32(ifs);
	strings lstOutcomeLabels(nOutcomesNumber);
	for (int32_t nIndex = 0; nIndex < nOutcomesNumber; nIndex++)
		lstOutcomeLabels[nIndex] = ReadUtf8(ifs);

	int32_t nOutcomePatternsNumber = ReadInt32(ifs);
	std::vector<ints> lstOutcomePatterns(nOutcomePatternsNumber);
	for (int32_t nIndex = 0; nIndex < nOutcomePatternsNumber; nIndex++)
	{
		ints& lstOutcomePatternsRow = lstOutcomePatterns[nIndex];
		string strLine = ReadUtf8(ifs);

		string::size_type nPos, nLastPos = 0;
		while ((nPos = strLine.find(_T(' '), nLastPos)) != string::npos)
		{
			int32_t nOutcomeValue = atoi(T2C(strLine).substr(nLastPos, nPos - nLastPos).c_str());
			lstOutcomePatternsRow.push_back(nOutcomeValue);
			nLastPos = nPos + 1;
		}
		if (nLastPos < strLine.length())
		{
			int32_t nOutcomeValue = atoi(T2C(strLine).substr(nLastPos).c_str());
			lstOutcomePatternsRow.push_back(nOutcomeValue);
		}
	}

	int32_t nPredicatesNumber = ReadInt32(ifs);
	strings lstPredicateLabels(nPredicatesNumber);
	for (int32_t nIndex = 0; nIndex < nPredicatesNumber; nIndex++)
		lstPredicateLabels[nIndex] = ReadUtf8(ifs);

	CContexts lstParameters;
	for (int32_t i = 0; i < nOutcomePatternsNumber; i++)
	{
		ints lstOutcomePattern(lstOutcomePatterns[i].size() - 1);
		for (ints::size_type k = 1; k < lstOutcomePatterns[i].size(); k++)
			lstOutcomePattern[k - 1] = lstOutcomePatterns[i][k];

		for (int32_t j = 0; j < lstOutcomePatterns[i][0]; j++)
		{
			doubles lstContextParameters(lstOutcomePatterns[i].size() - 1);
			for (doubles::size_type k = 1; k < lstOutcomePatterns[i].size(); k++)
				lstContextParameters[k - 1] = ReadDouble(ifs);

			lstParameters.push_back(CContext(lstContextParameters, lstOutcomePattern));
		}
	}

	m_pModel = new CGISModel(lstParameters, lstPredicateLabels, lstOutcomeLabels, nCorrectionConstant, fCorrectionParam);
}
