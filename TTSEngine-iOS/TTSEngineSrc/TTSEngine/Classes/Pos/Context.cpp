#include "POScommon.h"
#include "Context.h"

CContext::CContext()
{
}

CContext::CContext(const doubles& parameters, const ints& outcomes)
	: m_parameters(parameters)
	, m_outcomes(outcomes)
{
}

CContext::CContext(const CContext& other)
	: m_parameters(other.m_parameters)
	, m_outcomes(other.m_outcomes)
{
}

CContext& CContext::operator = (const CContext& other)
{
	m_parameters = other.m_parameters;
	m_outcomes = other.m_outcomes;
	return *this;
}
