#pragma once

#include "BaseModel.h"
#include "Context.h"
#include "GISModel.h"

class CPOSModel : public CBaseModel
{
public:
	CPOSModel(LPCTSTR pszDirectoryName);
	CPOSModel(const string& strManifestFile, const string& strModelFile);
	~CPOSModel();

	CGISModel& getPosModel() { return *m_pModel; }

private:
	void LoadManifest(const string& strManifestName);
	void LoadModel(const string& strModelName);

	properties	m_manifest;
	CGISModel	*m_pModel;
};
