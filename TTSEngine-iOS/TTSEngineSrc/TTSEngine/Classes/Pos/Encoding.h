#ifndef __ENCODING_H
#define __ENCODING_H

#include <iconv.h>
#include "stdint.h"

inline std::wstring Utf8_to_Utf16(LPCSTR pchBytes, int nLength)
{
    std::string s(pchBytes);
    
	std::wstring result(s.length(),L' ');
    std::copy(s.begin(), s.end(), result.begin());
    
	return result;
}

inline std::wstring Utf8_to_Utf16(const std::string& str)
{
	return Utf8_to_Utf16(str.c_str(), str.length());
}

// Convert wide-character string into UTF8-encoded single-byte-character string.
inline std::string Utf16_to_Utf8( const wchar_t* buffer, int len )
{
    
    std::wstring wstr = std::wstring(buffer);
    std::string newbuffer = std::string(wstr.begin(), wstr.end());
    
	return newbuffer;
}

// Convert wide-character string into UTF8-encoded single-byte-character string.
inline std::string Utf16_to_Utf8( const std::wstring& str )
{
	return Utf16_to_Utf8( str.c_str(), (int)str.length() );
}

#endif // !__ENCODING_H
