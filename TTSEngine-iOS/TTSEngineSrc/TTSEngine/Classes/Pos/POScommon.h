#pragma once

#include <stdio.h>
/// #include <tchar.h>
/// #include <windows.h>
#include <float.h>
#include <math.h>

#include <string>
#include <map>
#include <vector>
#include <list>
#include <iostream>
#include <fstream>
#include <algorithm>

// WCHAR_MIN/MAX redefinition
#pragma warning(disable: 4005)
#include "stdint.h"
#pragma warning(default: 4005)
#include "Encoding.h"

#ifdef _UNICODE
typedef std::wstring string;
#define C2T(s) Utf8_to_Utf16(s)
#define T2C(s) Utf16_to_Utf8(s)
#else
typedef std::string string;
#define C2T(s) (s)
#define T2C(s) (s)
#endif

typedef std::vector<string> strings;
typedef std::vector<double> doubles;
typedef std::vector<int> ints;
typedef std::vector<float> floats;
typedef std::map<string, string> properties;

#include "StreamHelpers.h"
