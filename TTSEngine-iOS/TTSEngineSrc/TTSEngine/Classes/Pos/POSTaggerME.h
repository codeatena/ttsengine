#pragma once

#include "POSModel.h"
#include "GISModel.h"

const int DEFAULT_BEAM_SIZE = 3;

static LPCTSTR _SE = _T("*SE*");
static LPCTSTR _SB = _T("*SB*");
static const int _PREFIX_LENGTH = 4;
static const int _SUFFIX_LENGTH = 4;

class CDefaultPOSContextGenerator
{
public:
	CDefaultPOSContextGenerator(int beamSize)
		: dictGram(1)
	{
	}

	static strings getPrefixes(string lex)
	{
		strings prefs(_PREFIX_LENGTH);
		for (int li = 0, ll = _PREFIX_LENGTH; li < ll; li++)
		{
			prefs[li] = lex.substr(0, fmin(li + 1, lex.length()));
		}
		return prefs;
	}

	static strings getSuffixes(string lex)
	{
		strings suffs(_SUFFIX_LENGTH);
		for (int li = 0, ll = _SUFFIX_LENGTH; li < ll; li++)
		{
			suffs[li] = lex.substr(fmax((int)lex.length() - li - 1, 0));
		}
		return suffs;
	}

	strings getContext(int index, const strings& tokens, const strings& tags)
	{
		string next, nextnext, lex, prev, prevprev;
		string tagprev, tagprevprev;

		lex = tokens[index];
		if (tokens.size() > index + 1)
		{
			next = tokens[index + 1];
			if (tokens.size() > index + 2)
				nextnext = tokens[index + 2];
			else
				nextnext = _SE; // Sentence End

		}
		else {
			next = _SE; // Sentence End
		}

		if (index - 1 >= 0)
		{
			prev =  tokens[index - 1];
			tagprev =  tags[index - 1];

			if (index - 2 >= 0)
			{
				prevprev = tokens[index - 2];
				tagprevprev = tags[index - 2];
			}
			else {
				prevprev = _SB; // Sentence Beginning
			}
		}
		else {
			prev = _SB; // Sentence Beginning
		}

		strings e;
		e.push_back(_T("default"));
		// add the word itself
		e.push_back(_T("w=") + lex);
		dictGram[0] = lex;
		{
			// do some basic suffix analysis
			strings suffs = getSuffixes(lex);
			for (strings::size_type i = 0; i < suffs.size(); i++)
			{
				e.push_back(_T("suf=") + suffs[i]);
			}

			strings prefs = getPrefixes(lex);
			for (strings::size_type i = 0; i < prefs.size(); i++)
			{
				e.push_back(_T("pre=") + prefs[i]);
			}
			// see if the word has any special characters
			if (lex.find(_T('-')) != string::npos)
			{
				e.push_back(_T("h"));
			}

			if (lex.find_first_of(_T("ABCDEFGHIJKLMNOPQRSTUVWXYZ")) != string::npos)
			{
				e.push_back(_T("c"));
			}

			if (lex.find_first_of(_T("0123456789")) != string::npos)
			{
				e.push_back(_T("d"));
			}
		}
		// add the words and pos's of the surrounding context
		if (!prev.empty())
		{
			e.push_back(_T("p=") + prev);
			if (!tagprev.empty())
			{
				e.push_back(_T("t=") + tagprev);
			}
			if (!prevprev.empty())
			{
				e.push_back(_T("pp=") + prevprev);
				if (!tagprevprev.empty())
				{
					e.push_back(_T("t2=") + tagprevprev + _T(",") + tagprev);
				}
			}
		}

		if (!next.empty())
		{
			e.push_back(_T("n=") + next);
			if (!nextnext.empty())
			{
				e.push_back(_T("nn=") + nextnext);
			}
		}

		return e;
	}

private:
	strings dictGram;
};

class CSequence
{
public:
	CSequence()
		: m_score(0)
	{}

	CSequence(const CSequence& s)
		: m_outcomes(s.m_outcomes)
		, m_probs(s.m_probs)
		, m_score(s.m_score)
	{
	}

	CSequence(const CSequence& s, const string& outcome, double p)
		: m_outcomes(s.m_outcomes)
		, m_probs(s.m_probs)
	{
		m_outcomes.push_back(outcome);
		m_probs.push_back(p);
		m_score = s.m_score + log(p);
	}

	CSequence& operator = (const CSequence& s)
	{
		m_outcomes = s.m_outcomes;
		m_probs = s.m_probs;
		m_score = s.m_score;
		return *this;
	}

	int compareTo(const CSequence& s) const
	{
		if (m_score < s.m_score)
			return 1;
		if (m_score > s.m_score)
			return -1;
		return 0;
	}

	strings getOutcomes() const
	{
		return m_outcomes;
	}

	double getScore() const
	{
		return m_score;
	}

private:
	double m_score;
	strings m_outcomes;
	doubles	m_probs;
};

typedef std::vector<CSequence> CSequences;

template<class T>
class CListHeap
{
public:
	CListHeap(int sz)
		: m_size(sz)
		, m_haveMax(false)
	{
	}

	CListHeap(const CListHeap& other)
		: m_list(other.m_list)
		, m_size(other.m_size)
		, m_max(other.m_max)
		, m_haveMax(other.m_haveMax)
	{
	}

	CListHeap& operator = (const CListHeap& other)
	{
		m_list = other.m_list;
		m_size = other.m_size;
		m_max = other.m_max;
		m_haveMax = other.m_haveMax;
		return *this;
	}

	void clear()
	{
		m_list.clear();
		m_haveMax = false;
	}

	int left(int i) const
	{
		return (i + 1) * 2 - 1;
	}

	int right(int i) const
	{
		return (i + 1) * 2;
	}

	void swap(int x, int y)
	{
		T ox = m_list[x];
		m_list[x] = m_list[y];
		m_list[y] = ox;
	}

	void heapify(int i)
	{
		while (true)
		{
			int l = left(i);
			int r = right(i);
			int smallest;

			if (l < m_list.size() && lt(m_list[l], m_list[i]))
				smallest = l;
			else
				smallest = i;

			if (r < m_list.size() && lt(m_list[r], m_list[smallest]))
				smallest = r;

			if (smallest != i) {
				swap(smallest, i);
				i = smallest;
			}
			else
				break;
		}
	}

	T extract()
	{
		if (m_list.empty())
			throw "Heap Underflow";

		T top = m_list[0];
		
		int last = m_list.size() - 1;
		if (last != 0)
		{
			m_list[0] = m_list[last];
			m_list.erase(m_list.begin() + last);
			heapify(0);
		}
		else
		{
			m_list.erase(m_list.begin() + last);
		}

		return top;
	}

	void add(const T& o)
	{
		/* keep track of max to prevent unnecessary insertion */
		if (!m_haveMax)
		{
			m_max = o;
			m_haveMax = true;
		}
		else if (gt(o, m_max))
		{
			if (m_list.size() < m_size)
			{
				m_max = o;
			}
			else
			{
				return;
			}
		}
		m_list.push_back(o);

		int i = m_list.size() - 1;

		//percolate new node to correct position in heap.
		while (i > 0 && gt(m_list[parent(i)], o))
		{
			m_list[i] = m_list[parent(i)];
			i = parent(i);
		}

		m_list[i] = o;
	}

	bool gt(const T& o1, const T& o2) const
	{
		return o1.compareTo(o2) > 0;
	}

	bool lt(const T& o1, const T& o2) const
	{
		return o1.compareTo(o2) < 0;
	}

	int parent(int i) const
	{
		return (i - 1) / 2;
	}

	int size() const
	{
		return m_list.size();
	}

private:
	std::vector<T>	m_list;
	int				m_size;
	T				m_max;
	bool			m_haveMax;
};

class CBeamSearch
{
public:
	CBeamSearch(int size, CDefaultPOSContextGenerator* pCg, CGISModel *pModel, int cacheSize)
		: m_size(size)
		, m_pCg(pCg)
		, m_pModel(pModel)
		, m_probs(pModel->getNumOutcomes())
	{
	}

	CSequences bestSequences(int numSequences, const strings& sequence, double minSequenceScore)
	{
		CListHeap<CSequence> prev(m_size);
		CListHeap<CSequence> next(m_size);
		prev.add(CSequence());

		for (strings::size_type i = 0; i < sequence.size(); i++)
		{
			int sz = fmin(m_size, prev.size());
			
			for (int sc = 0; prev.size() > 0 && sc < sz; sc++)
			{
				CSequence top = prev.extract();
				strings outcomes = top.getOutcomes();
				strings contexts = m_pCg->getContext(i, sequence, outcomes);
				doubles scores = m_pModel->eval(contexts, m_probs);

				doubles temp_scores = scores;
				std::sort(temp_scores.begin(), temp_scores.end());
				double min = temp_scores[fmax(0, scores.size() - m_size)];

				for (doubles::size_type p = 0; p < scores.size(); p++)
				{
					if (scores[p] < min)
						continue; //only advance first "size" outcomes
					string out = m_pModel->getOutcome(p);

					CSequence ns(top, out, scores[p]);
					if (ns.getScore() > minSequenceScore)
					{
						next.add(ns);
					}
				}

				if (next.size() == 0)
				{//if no advanced sequences, advance all valid
					for (doubles::size_type p = 0; p < scores.size(); p++)
					{
						string out = m_pModel->getOutcome(p);

						CSequence ns(top, out, scores[p]);
						if (ns.getScore() > minSequenceScore)
						{
							next.add(ns);
						}
					}
				}
			}

			//    make prev = next; and re-init next (we reuse existing prev set once we clear it)
			prev.clear();
			std::swap(next, prev);
		}

		int numSeq = fmin(numSequences, prev.size());
		CSequences topSequences(numSeq);

		for (int seqIndex = 0; seqIndex < numSeq; seqIndex++)
		{
			topSequences[seqIndex] = prev.extract();
		}

		return topSequences;
	}

	CSequence bestSequence(const strings& sequence)
	{
		CSequences sequences = bestSequences(1, sequence, ZeroLog);
		if (!sequences.empty())
			return sequences[0];
		return CSequence();
	}

private:
	static const int ZeroLog = -100000;

	int m_size;
	CDefaultPOSContextGenerator *m_pCg;
	CGISModel *m_pModel;
	doubles m_probs;
};

class CPOSTaggerME
{
public:
	CPOSTaggerME(CPOSModel& model, int beamSize=DEFAULT_BEAM_SIZE, int cacheSize=0)
		: m_posModel(model.getPosModel())
		, m_contextGen(beamSize)
		, m_size(beamSize)
		, m_beam(beamSize, &m_contextGen, &m_posModel, cacheSize)
	{
	}

	~CPOSTaggerME()
	{
	}

	strings tag(const strings& sentence)
	{
		CSequence bestSequence = m_beam.bestSequence(sentence);
		return bestSequence.getOutcomes();
	}

protected:
	CGISModel&					m_posModel;
	CDefaultPOSContextGenerator	m_contextGen;
	int							m_size;
	CBeamSearch					m_beam;
};
