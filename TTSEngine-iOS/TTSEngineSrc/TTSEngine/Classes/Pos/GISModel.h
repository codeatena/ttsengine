#pragma once

#include "Context.h"

class Prior
{
public:
	virtual void logPrior(doubles& dist, const ints& context) = 0;
	virtual void logPrior(doubles& dist, const ints& context, const floats& values) = 0;
	virtual void setLabels(const strings& outcomeLabels, const strings& contextLabels) = 0;
};

class UniformPrior : public Prior
{
public:
	UniformPrior()
		: numOutcomes(0)
		, r(0)
	{}

	void logPrior(doubles& dist, const ints& context, const floats& values)
	{
		for (int oi = 0; oi < numOutcomes; oi++)
		{
			dist[oi] = r;
		}
	}

	void logPrior(doubles& dist, const ints& context)
	{
		logPrior(dist, context, floats());
	}

	void setLabels(const strings& outcomeLabels, const strings& contextLabels)
	{
		numOutcomes = outcomeLabels.size();
		r = log(1.0/numOutcomes);
	}

private:

	int numOutcomes;
	double r;
};

template<class T>
class IndexHashTable
{
public:
	IndexHashTable(const std::vector<T>& mapping)
	{
		for (typename std::vector<T>::size_type i = 0; i < mapping.size(); i++)
		{
			m_values.insert(std::pair<T, int>(mapping[i], i));
		}
	}

	int get(T key, int defValue=0) const
	{
		typename Map::const_iterator it = m_values.find(key);
		if (it == m_values.end())
			return defValue;
		return (*it).second;
	}

	int size() const
	{
		return m_values.size();
	}

private:
	typedef std::map<T, int> Map;

	Map	m_values;
};

class EvalParameters
{
public:
	EvalParameters(const CContexts& params, double correctionParam, double correctionConstant, int numOutcomes)
		: m_params(params)
		, m_numOutcomes(numOutcomes)
		, m_correctionParam(correctionParam)
		, m_correctionConstant(correctionConstant)
		, m_constantInverse(1.0/correctionConstant)
	{
	}

	const CContexts& getParams() const { return m_params; }
	int getNumOutcomes() const { return m_numOutcomes; }
	double getCorrectionConstant() const { return m_correctionConstant; }
	double getConstantInverse() const { return m_constantInverse; }
	double getCorrectionParam() const { return m_correctionParam; }

	void setCorrectionParam(double correctionParam)
	{
		m_correctionParam = correctionParam;
	}

private:
	/** Mapping between outcomes and paramater values for each context. 
	* The integer representation of the context can be found using <code>pmap</code>.*/
	CContexts m_params;
	/** The number of outcomes being predicted. */
	const int m_numOutcomes;
	/** The maximum number of feattures fired in an event. Usually refered to a C.
	* This is used to normalize the number of features which occur in an event. */
	double m_correctionConstant;

	/**  Stores inverse of the correction constant, 1/C. */
	const double m_constantInverse;
	/** The correction parameter of the model. */
	double m_correctionParam;
};

enum ModelType
{
	Maxent,
	Perceptron,
};

class AbstractModel
{
public:
	AbstractModel(const CContexts& params, const strings& predLabels, const strings& outcomeNames, 
		int32_t correctionConstant, double correctionParam)
		: m_pmap(predLabels)
		, m_outcomeNames(outcomeNames)
		, m_evalParameters(params, correctionParam, correctionConstant, outcomeNames.size())
		, m_pPrior(NULL)
		, m_modelType(Maxent)
	{
	}

	virtual ~AbstractModel()
	{
	}

	int getNumOutcomes() const
	{
		return m_evalParameters.getNumOutcomes();
	}

	string getOutcome(int i) const
	{
		return m_outcomeNames[i];
	}

protected:
	IndexHashTable<string>	m_pmap;
	strings					m_outcomeNames;
	EvalParameters			m_evalParameters;
	Prior					*m_pPrior;
	ModelType				m_modelType;
};

class CGISModel : public AbstractModel
{
public:
	CGISModel(const CContexts& lstContexts, const strings& lstPredicateLabels, 
		const strings& lstOutcomeNames, int32_t nCorrectionConstant, double fCorrectionParam)
		: AbstractModel(lstContexts, lstPredicateLabels, lstOutcomeNames, nCorrectionConstant, fCorrectionParam)
	{
		m_pPrior = new UniformPrior();
		m_pPrior->setLabels(lstOutcomeNames, lstPredicateLabels);
		m_modelType = Maxent;
	}

	~CGISModel()
	{
		delete m_pPrior;
	}

	doubles eval(const strings& context, doubles& outsums)
	{
		ints scontexts(context.size());
		for (strings::size_type i = 0; i < context.size(); i++)
		{
			scontexts[i] = m_pmap.get(context[i], -1);
		}
		m_pPrior->logPrior(outsums, scontexts, floats());
		return eval(scontexts, outsums, m_evalParameters);
	}

	doubles eval(const ints& context, doubles& prior, const EvalParameters& model)
	{
		const CContexts& params = model.getParams();
		ints numfeats(model.getNumOutcomes());
		double value = 1;
		for (ints::size_type ci = 0; ci < context.size(); ci++)
		{
			if (context[ci] >= 0)
			{
				const CContext& predParams = params[context[ci]];
				const ints& activeOutcomes = predParams.getOutcomes();
				const doubles& activeParameters = predParams.getParameters();
				for (ints::size_type ai = 0; ai < activeOutcomes.size(); ai++)
				{
					int oid = activeOutcomes[ai];
					numfeats[oid]++;
					prior[oid] += activeParameters[ai] * value;
				}
			}
		}

		double normal = 0.0;
		for (int oid = 0; oid < model.getNumOutcomes(); oid++)
		{
			if (model.getCorrectionParam() != 0)
			{
				prior[oid] = exp(prior[oid] * model.getConstantInverse() 
					+ ((1.0 - ((double) numfeats[oid] / model.getCorrectionConstant())) * model.getCorrectionParam()));
			}
			else
			{
				prior[oid] = exp(prior[oid] * model.getConstantInverse());
			}
			normal += prior[oid];
		}

		for (int oid = 0; oid < model.getNumOutcomes(); oid++)
		{
			prior[oid] /= normal;
		}
		return prior;
	}
};
