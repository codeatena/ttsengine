#pragma once

class CContext
{
public:
	CContext();
	CContext(const doubles& parameters, const ints& outcomes);
	CContext(const CContext& other);

	CContext& operator = (const CContext& other);

	const doubles& getParameters() const { return m_parameters; }
	const ints& getOutcomes() const { return m_outcomes; }

private:
	doubles	m_parameters;
	ints	m_outcomes;
};

typedef std::vector<CContext> CContexts;
