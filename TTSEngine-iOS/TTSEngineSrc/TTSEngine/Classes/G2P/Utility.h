#pragma once

bool ReadAllText(const string& strFileName, sstring& strText);

sstring_vector Tokenize(const sstring& strText, const char *pszDelimiters, bool squashEmpty=true);
