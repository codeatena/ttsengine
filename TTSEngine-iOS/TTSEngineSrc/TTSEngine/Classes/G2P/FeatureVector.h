#pragma once

enum FeatureVectorFeatureType
{
	byteValued, 
	shortValued, 
	floatValued
};

class CFeatureVector
{
public:
	CFeatureVector(const bytes& byteValuedDiscreteFeatures, const shorts& shortValuedDiscreteFeatures, 
		const floats& continuousFeatures, int setUnitIndex);
	CFeatureVector(const CFeatureVector& other);
	~CFeatureVector();

	CFeatureVector& operator = (const CFeatureVector& other);

	inline uint8_t GetByteFeature(int index) const { return m_byteValuedDiscreteFeatures[index]; }
	inline short GetShortFeature(int index) const { return m_shortValuedDiscreteFeatures[index - m_byteValuedDiscreteFeatures.size()]; }
	inline float GetContinuousFeature(int index) const
		{ return m_continuousFeatures[index - m_byteValuedDiscreteFeatures.size() - m_shortValuedDiscreteFeatures.size()]; }

private:
    int		m_unitIndex;
    bytes	m_byteValuedDiscreteFeatures;
    shorts	m_shortValuedDiscreteFeatures;
    floats	m_continuousFeatures;
};
