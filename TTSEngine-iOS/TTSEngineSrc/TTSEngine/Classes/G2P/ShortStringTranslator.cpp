#include "G2Pcommon.h"
#include "ShortStringTranslator.h"

CShortStringTranslator::CShortStringTranslator()
{
}

CShortStringTranslator::CShortStringTranslator(const CShortStringTranslator& other)
	: m_list(other.m_list)
	, m_map(other.m_map)
{
}

CShortStringTranslator::~CShortStringTranslator()
{
}

CShortStringTranslator& CShortStringTranslator::operator = (const CShortStringTranslator& other)
{
	m_list = other.m_list;
	m_map = other.m_map;
	return *this;
}

sstring CShortStringTranslator::operator[] (short b) const
{
	return m_list[b];
}

void CShortStringTranslator::Resize(int nRange)
{
	//m_list.resize(nRange);
}

void CShortStringTranslator::Set(short b, const sstring& s)
{
	m_list.insert(m_list.begin() + b, s);
	m_map[s] = b;
}
