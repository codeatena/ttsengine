#include "G2Pcommon.h"
#include "IntStringTranslator.h"

CIntStringTranslator::CIntStringTranslator()
{
}

CIntStringTranslator::CIntStringTranslator(const CIntStringTranslator& other)
	: m_list(other.m_list)
	, m_map(other.m_map)
{
}

CIntStringTranslator::~CIntStringTranslator()
{
}

CIntStringTranslator& CIntStringTranslator::operator = (const CIntStringTranslator& other)
{
	m_list = other.m_list;
	m_map = other.m_map;

	return *this;
}

int CIntStringTranslator::operator[] (const sstring& s) const
{
	std::map<sstring, int>::const_iterator it = m_map.find(s);
	if (it == m_map.end())
		throw "No int value known for the specified string";
	return (*it).second;
}

void CIntStringTranslator::Resize(int nRange)
{
	//m_list.resize(nRange);
}

void CIntStringTranslator::Set(int i, const sstring& s)
{
	m_list.insert(m_list.begin() + i, s);
	m_map[s] = i;
}
