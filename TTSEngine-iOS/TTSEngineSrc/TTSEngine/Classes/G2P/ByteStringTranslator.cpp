#include "G2Pcommon.h"
#include "ByteStringTranslator.h"

CByteStringTranslator::CByteStringTranslator()
{
}

CByteStringTranslator::CByteStringTranslator(const CByteStringTranslator& other)
	: m_list(other.m_list)
	, m_map(other.m_map)
{
}

CByteStringTranslator::~CByteStringTranslator()
{
}

CByteStringTranslator& CByteStringTranslator::operator = (const CByteStringTranslator& other)
{
	m_list = other.m_list;
	m_map = other.m_map;
	return *this;
}

uint8_t CByteStringTranslator::operator[] (const sstring& s) const
{
	std::map<sstring, uint8_t>::const_iterator it = m_map.find(s);
	if (it == m_map.end())
		throw "No byte value known for the specified string";
	return (*it).second;
}

sstring CByteStringTranslator::operator[] (int8_t b) const
{
	return m_list[b];
}

void CByteStringTranslator::Resize(int nRange)
{
	//m_list.resize(nRange);
}

void CByteStringTranslator::Set(uint8_t b, const sstring& s)
{
	m_list.insert(m_list.begin() + b, s);
	m_map[s] = b;
}
