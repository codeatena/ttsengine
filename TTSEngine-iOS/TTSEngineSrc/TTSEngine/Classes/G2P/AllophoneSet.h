#pragma once

class CAllophoneSet
{
public:
	CAllophoneSet(const string& strFilename);
	CAllophoneSet(const CAllophoneSet& other);
	~CAllophoneSet();

	CAllophoneSet& operator = (const CAllophoneSet& other);

	CPCAllophone GetAllophone(const sstring& ph) const;

private:
	std::map<sstring, CAllophone>		m_mapAllophones;
	std::map<sstring, sstring_vector>	m_mapFeatureValues;
	sstring								m_strName;
	size_t								m_nMaxAllophoneSymbolLength;
	sstring								m_strSilenceAllophoneName;
};

typedef CAllophoneSet *PCAllophoneSet;
