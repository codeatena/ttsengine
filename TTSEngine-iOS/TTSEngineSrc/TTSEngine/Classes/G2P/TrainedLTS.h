#pragma once

class CTrainedLTS
{
public:
	CTrainedLTS(const string& strAllophoneFileName, const string& strTreeFilename);
	~CTrainedLTS();

	sstring PredictPronunciation(const sstring& graphemes) const;
	sstring Syllabify(const sstring& phones) const;

private:

private:
	CAllophoneSet		m_allophoneSet;
	CCART				m_tree;
    CFeatureDefinition	m_featureDefinition;
    int					m_indexPredictedFeature;
    int					m_context;
    bool				m_convertToLowercase;
};
