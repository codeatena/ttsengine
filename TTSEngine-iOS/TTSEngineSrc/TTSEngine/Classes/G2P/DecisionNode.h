#pragma once

class CNode;
typedef CNode *PCNode;

class CNode
{
public:
	virtual ~CNode();

	void SetMother(PCNode node, int nodeIndex);
	virtual int GetNumberOfData() const = 0;

protected:
	CNode(bool isRoot);

protected:
	bool	m_isRoot;
    PCNode	m_mother;
    int		m_nodeIndex;
};

typedef std::vector<CNode> CNodes;
typedef std::vector<PCNode> PCNodes;

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Decision Node stuff
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////

enum DecisionNodeType : int
{ 
	BinaryByteDecisionNode = 0, 
	BinaryShortDecisionNode, 
	BinaryFloatDecisionNode, 
	ByteDecisionNode, 
	ShortDecisionNode
};

class CDecisionNode : public CNode
{
protected:
	CDecisionNode(int featureIndex, int numDaughters, const CFeatureDefinition& featureDefinition);

public:
	~CDecisionNode();

    void AddDaughter(PCNode daughter);
	void CountData();
	int GetNumberOfData() const { return m_nData; }
	virtual PCNode GetNextNode(const CFeatureVector& featureVector) const = 0;

private:
	CDecisionNode(const CDecisionNode& other);
	CDecisionNode& operator = (const CDecisionNode& other);

protected:
    CFeatureDefinition	m_featureDefinition;
	PCNodes				m_daughters;
    int					m_featureIndex;
	sstring				m_feature;
    size_t				m_lastDaughter;
    int					m_nData;
    int					m_uniqueDecisionNodeId;
};

typedef CDecisionNode *PCDecisionNode;
typedef std::vector<CDecisionNode> CDecisionNodes;
typedef std::vector<PCDecisionNode> PCDecisionNodes;

class CBinaryByteDecisionNode : public CDecisionNode
{
public:
	CBinaryByteDecisionNode(int featureIndex, uint8_t value, const CFeatureDefinition& featureDefinition);

	PCNode GetNextNode(const CFeatureVector& featureVector) const;
private:
	uint8_t	m_value;
};

class CBinaryShortDecisionNode : public CDecisionNode
{
public:
	CBinaryShortDecisionNode(int featureIndex, short value, const CFeatureDefinition& featureDefinition);

	PCNode GetNextNode(const CFeatureVector& featureVector) const;
private:
	short m_value;
};

class CBinaryFloatDecisionNode : public CDecisionNode
{
public:
	CBinaryFloatDecisionNode(int featureIndex, float value, const CFeatureDefinition& featureDefinition);

	PCNode GetNextNode(const CFeatureVector& featureVector) const;
private:
	float	m_value;
	bool	m_isByteFeature;
};

class CByteDecisionNode : public CDecisionNode
{
public:
	CByteDecisionNode(int featureIndex, int numDaughters, const CFeatureDefinition& featureDefinition);

	PCNode GetNextNode(const CFeatureVector& featureVector) const;
};

class CShortDecisionNode : public CDecisionNode
{
public:
	CShortDecisionNode(int featureIndex, int numDaughters, const CFeatureDefinition& featureDefinition);

	PCNode GetNextNode(const CFeatureVector& featureVector) const;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Leaf Node stuff
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////

enum LeafNodeLeafType : int
{
    IntArrayLeafNode = 0,
    FloatLeafNode, 
    IntAndFloatArrayLeafNode, 
    StringAndFloatLeafNode,
    FeatureVectorLeafNode, 
    PdfLeafNode
};

class CLeafNode : public CNode
{
protected:
	CLeafNode(int uniqueLeafId=0);

protected:
	int m_uniqueLeafId;
};

typedef CLeafNode *PCLeafNode;
typedef std::vector<CLeafNode> CLeafNodes;
typedef std::vector<PCLeafNode> PCLeafNodes;

class CIntArrayLeafNode : public CLeafNode
{
public:
	CIntArrayLeafNode(const ints& data);

	int GetNumberOfData() const { return m_data.size(); }

protected:
	ints m_data;
};

typedef CIntArrayLeafNode *PCIntArrayLeafNode;

class CFloatLeafNode : public CLeafNode
{
public:
	CFloatLeafNode(float value1, float value2);

	int GetNumberOfData() const { return 1; }

private:
	floats m_data;
};

typedef CFloatLeafNode *PCFloatLeafNode;

class CIntAndFloatArrayLeafNode : public CIntArrayLeafNode
{
public:
	CIntAndFloatArrayLeafNode(const ints& data, const floats& floats0);

	int MostProbableInt() const;
private:
	floats m_floats;
};

typedef CIntAndFloatArrayLeafNode *PCIntAndFloatArrayLeafNode;

class CStringAndFloatLeafNode : public CIntAndFloatArrayLeafNode
{
public:
	CStringAndFloatLeafNode(const ints& data, const floats& floats0);

	sstring MostProbableString(const CFeatureDefinition& featureDefinition, int featureIndex) const;
};

typedef CStringAndFloatLeafNode *PCStringAndFloatLeafNode;
