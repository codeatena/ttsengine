#include "G2Pcommon.h"
#include "FeatureVector.h"

CFeatureVector::CFeatureVector(const bytes& byteValuedDiscreteFeatures, const shorts& shortValuedDiscreteFeatures, 
							   const floats& continuousFeatures, int setUnitIndex)
   : m_unitIndex(setUnitIndex)
   , m_byteValuedDiscreteFeatures(byteValuedDiscreteFeatures)
   , m_shortValuedDiscreteFeatures(shortValuedDiscreteFeatures)
   , m_continuousFeatures(continuousFeatures)
{
	if (setUnitIndex < 0)
		throw "The unit index can't be negative or null when instanciating a new feature vector.";
}

CFeatureVector::CFeatureVector(const CFeatureVector& other)
   : m_unitIndex(other.m_unitIndex)
   , m_byteValuedDiscreteFeatures(other.m_byteValuedDiscreteFeatures)
   , m_shortValuedDiscreteFeatures(other.m_shortValuedDiscreteFeatures)
   , m_continuousFeatures(other.m_continuousFeatures)
{
}

CFeatureVector::~CFeatureVector()
{
}

CFeatureVector& CFeatureVector::operator = (const CFeatureVector& other)
{
   m_unitIndex = other.m_unitIndex;
   m_byteValuedDiscreteFeatures = other.m_byteValuedDiscreteFeatures;
   m_shortValuedDiscreteFeatures = other.m_shortValuedDiscreteFeatures;
   m_continuousFeatures = other.m_continuousFeatures;

   return *this;
}
