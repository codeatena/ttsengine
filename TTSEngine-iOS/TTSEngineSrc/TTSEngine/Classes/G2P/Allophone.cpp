#include "G2Pcommon.h"
#include "Allophone.h"

CAllophone::CAllophone()
{
}

CAllophone::CAllophone(const rapidxml::xml_node<> *pNode, const sstring_vector& lstFeatures)
{
	const rapidxml::xml_attribute<> *pName = pNode->first_attribute("ph");
	if (pName != NULL)
		m_strName = pName->value();
	if (m_strName.empty())
		throw "Element must have a 'ph' attribute";

	sstring strVc, strIsTone;
    if (strcmp(pNode->name(), "consonant") == 0)
	{
        strVc = "-";
        strIsTone = "-";
    }
	else if (strcmp(pNode->name(), "vowel") == 0)
	{
        strVc = "+";
        strIsTone = "-";
    }
	else if (strcmp(pNode->name(), "silence") == 0)
	{
        strVc = "0";
        strIsTone = "-";
    }
	else if (strcmp(pNode->name(), "tone") == 0)
	{
        strVc = "0";
        strIsTone = "+";
    }
	else
	{
		throw "Element must be one of <vowel>, <consonant> and <silence>";
    }

	m_mapFeatures["vc"] = strVc;
	m_mapFeatures["isTone"] = strIsTone;

	for (size_t i = 0; i < lstFeatures.size(); i++)
	{
		const sstring& strFeature = lstFeatures[i];
		const rapidxml::xml_attribute<> *pFeature = pNode->first_attribute(strFeature.c_str());
		if (pFeature != NULL)
			m_mapFeatures[strFeature] = pFeature->value();
		else
			m_mapFeatures[strFeature] = "";
	}
}

CAllophone::CAllophone(const CAllophone& other)
	: m_strName(other.m_strName)
	, m_mapFeatures(other.m_mapFeatures)
{
}

CAllophone::~CAllophone()
{
}

CAllophone& CAllophone::operator = (const CAllophone& other)
{
	m_strName = other.m_strName;
	m_mapFeatures = other.m_mapFeatures;

	return *this;
}

bool CAllophone::IsPause() const
{
	// The following two features are hardcoded and therefore we don't have
	// to check if they are found (always expected to be there).
	return (*m_mapFeatures.find("vc")).second == "0"
		&& (*m_mapFeatures.find("isTone")).second == "-";
}

sstring CAllophone::GetFeature(const sstring& strFeature) const
{
	sstring_map::const_iterator it = m_mapFeatures.find(strFeature);
	if (it == m_mapFeatures.end())
		return "";
	return (*it).second;
}

int CAllophone::Sonority() const
{
    if (IsVowel())
	{
		sstring_map::const_iterator it = m_mapFeatures.find("vlng");
		if (it == m_mapFeatures.end())
			return 5;
        const sstring& vlng = (*it).second;

		if (strstr("ld", vlng.c_str()) != NULL)
			return 6;

        if (vlng == "s") 
			return 5;
        if (vlng == "a") 
			return 4;
        return 5;
    }

	if (IsSonorant()) 
		return 3;

	if (IsFricative())
		return 2;

	return 1;
}

bool CAllophone::IsVowel() const
{
	return ((*m_mapFeatures.find("vc")).second == "+");
}

bool CAllophone::IsSonorant() const
{
	sstring_map::const_iterator it = m_mapFeatures.find("ctype");
	if (it == m_mapFeatures.end())
		return false;

	return (strstr("lnr", (*it).second.c_str()) != NULL);
}

bool CAllophone::IsFricative() const
{
	sstring_map::const_iterator it = m_mapFeatures.find("ctype");
	if (it == m_mapFeatures.end())
		return false;

	return ((*it).second == "f");
}
