#pragma once

class CAllophone
{
public:
	CAllophone();
	CAllophone(const rapidxml::xml_node<> *pNode, const sstring_vector& lstFeatures);
	CAllophone(const CAllophone& other);
	~CAllophone();

	CAllophone& operator = (const CAllophone& other);

	inline const sstring& GetName() const { return m_strName; }
	inline const sstring& name() const { return m_strName; }

	bool IsPause() const;
	sstring GetFeature(const sstring& strFeature) const;
	int Sonority() const;
	bool IsVowel() const;
	bool IsSonorant() const;
	bool IsFricative() const;

private:
	sstring		m_strName;
	sstring_map	m_mapFeatures;
};

typedef CAllophone *PCAllophone;
typedef const CAllophone *CPCAllophone;
