#ifndef __ENCODING_H
#define __ENCODING_H

#include "stdint.h"
#include <iconv.h>

inline std::wstring Utf8_to_Utf16(LPCSTR pchBytes, int nLength)
{
    std::string s(pchBytes);
    
	std::wstring result(s.length(),L' ');
    std::copy(s.begin(), s.end(), result.begin());
	
	return result;
}

inline std::wstring Utf8_to_Utf16(const std::string& str)
{
	return Utf8_to_Utf16(str.c_str(), str.length());
}

// Convert wide-character string into UTF8-encoded single-byte-character string.
inline std::string Utf16_to_Utf8( const wchar_t* buffer, int len )
{
    std::wstring wstr = std::wstring(buffer);
	std::string newbuffer = std::string(wstr.begin(), wstr.end());
    
	return newbuffer;
}

// Convert wide-character string into UTF8-encoded single-byte-character string.
inline std::string Utf16_to_Utf8( const std::wstring& str )
{
	return Utf16_to_Utf8( str.c_str(), (int)str.length() );
}

inline bool EndsWith(const std::string& strText, LPCSTR pszPattern)
{
	size_t nPatternLength = strlen(pszPattern);
	if (strText.length() < nPatternLength)
		return false;
	return (strcmp(strText.c_str() + strText.length() - nPatternLength, pszPattern) == 0);
}

inline bool StringEquals(const std::string& strText, LPCSTR pszPattern, bool bCS=true)
{
	if (bCS)
	{
		return strText == pszPattern;
	}
	else
	{
		std::string strText0 = strText;
		std::transform(strText0.begin(), strText0.end(), strText0.begin(), ::tolower);

		std::string strPattern = pszPattern;
		std::transform(strPattern.begin(), strPattern.end(), strPattern.begin(), ::tolower);

		return strText0 == strPattern;
	}
}

#endif // !__ENCODING_H
