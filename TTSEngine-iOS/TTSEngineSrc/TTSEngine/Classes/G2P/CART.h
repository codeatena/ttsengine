#pragma once

class CDirectedGraph
{
public:
	CDirectedGraph(PCNode rootNode, const CFeatureDefinition& featDef, const CProperties& properties);
	virtual ~CDirectedGraph();

	inline const CFeatureDefinition& GetFeatureDefinition() const { return m_featDef; }
	inline const CProperties& GetProperties() const { return m_properties; }

protected:
    PCNode				m_rootNode;
    CFeatureDefinition	m_featDef;
    CProperties			m_properties;
};

class CCART : public CDirectedGraph
{
public:
	CCART(PCNode rootNode, const CFeatureDefinition& featDef, const CProperties& properties);
	CCART(const CCART& other);
	~CCART();

	PCNode InterpretToNode(const CFeatureVector& featureVector, int minNumberOfData) const;

private:
	CCART& operator = (const CCART& other);
};
