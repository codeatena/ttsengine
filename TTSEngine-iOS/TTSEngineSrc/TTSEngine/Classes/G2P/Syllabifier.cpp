#include "G2Pcommon.h"
#include "Allophone.h"
#include "AllophoneSet.h"
#include "Syllabifier.h"

CSyllabifier::CSyllabifier(const CAllophoneSet& allophoneSet)
	: m_allophoneSet(allophoneSet)
{
}

CSyllabifier::~CSyllabifier()
{
}

sstring CSyllabifier::Syllabify(const sstring& phoneString) const
{
    sstring_vector phoneList = SplitIntoAllophones(phoneString);
    Syllabify(phoneList);
	sstring sb;
    for (size_t index = 0; index < phoneList.size(); index++)
	{
        if (sb.length() > 0) 
			sb += " ";
        sb += phoneList[index];
    }
    return sb;
}

void CSyllabifier::Syllabify(sstring_vector& phoneList) const
{
	if (phoneList.empty())
		return;

	sstring_vector::iterator it = phoneList.begin();

	CPCAllophone previous = GetAllophone(*(it++));
    bool previousIsVowel = (previous != NULL && previous->Sonority() >= 4);
    while (it != phoneList.end())
	{
        CPCAllophone next = GetAllophone(*(it++));
        bool nextIsVowel = (next != NULL && next->Sonority() >= 4);

        if (previousIsVowel 
			&& nextIsVowel 
			&& next->name() != "6")
		{
			size_t tempOffset = (it - phoneList.begin());
			phoneList.insert(it - 1, "-");
			it = phoneList.begin() + tempOffset + 1;
        }
        previousIsVowel = nextIsVowel;
    }

    it = phoneList.begin();
    int minSonority = 7;
    int minIndex = -1;
    int syllableStart = -1;
    while (it != phoneList.end())
	{
        const sstring& s = *(it++);
        if (s == "-")
		{
            minSonority = 7;
            minIndex = -1;
            syllableStart = (it - phoneList.begin()) - 1;
        }
		else
		{
            CPCAllophone ph = GetAllophone(s);
            if (ph != NULL && ph->Sonority() < minSonority)
			{
                minSonority = ph->Sonority();
                minIndex = (it - phoneList.begin()) - 1;
            }
			else if (ph != NULL && ph->Sonority() >= 4)
			{
                if (minIndex > syllableStart + 1)
				{
                    int steps = 0;
                    while ((it - phoneList.begin()) > minIndex)
					{
                        steps++;
                        it--;
                    }
					size_t tempOffset = (it - phoneList.begin());
                    phoneList.insert(it, ".");
					it = phoneList.begin() + tempOffset + 1;
                    while (steps > 0)
					{
                        it++;
                        steps--;
                    }
                }
                minSonority = 7;
                minIndex = -1;
            }
        }
    }

    it = phoneList.begin();
    while (it != phoneList.end())
	{
        const sstring& s = *(it++);
        if (s == ".")
		{
            CPCAllophone ph = GetAllophone(*(it-2));
            if (ph != NULL && ph->Sonority() == 5)
			{
                ph = GetAllophone(*(it++));
                if (ph != NULL && ph->Sonority() <= 3)
				{
                    ph = GetAllophone(*(it++));
                    if (ph != NULL && ph->Sonority() <= 3)
					{
                        it--; it--; it--;
						size_t tempOffset = (it - phoneList.begin());
						phoneList.erase(it);
						it = phoneList.begin() + tempOffset + 1;
                        //it++;
                        phoneList.insert(it, "-");
						it = phoneList.begin() + tempOffset + 2;
                    }
					else
					{
                        it--; it--; it--;

                        *(it - 1) = "-";
                    }
                }
				else
				{
                    it--; it--;
                    *(it - 1) = "-";
                }
            } else {
                *(it - 1) = "-";
            }
        }
    }

    it = phoneList.begin();
    while (it != phoneList.end())
	{
        const sstring& s = *(it++);

		if (s == "-")
		{
            CPCAllophone ph = GetAllophone(*(it++));
            if (ph != NULL && ph->name() == "N")
			{
                ph = GetAllophone(*(it++));
                if (ph != NULL && ph->Sonority() >= 5)
				{
                    it--; it--; it--;
					size_t tempOffset = (it - phoneList.begin());
                    phoneList.erase(it);
					it = phoneList.begin() + tempOffset + 1;
                    //it++;
                    phoneList.insert(it, "-");
					it = phoneList.begin() + tempOffset + 2;
                }
            }
        }
    }

	CorrectStressSymbol(phoneList);
}

CPCAllophone CSyllabifier::GetAllophone(const sstring& phone) const
{
    if (EndsWith(phone, "1"))
        return m_allophoneSet.GetAllophone(phone.substr(0, phone.length() - 1));

	return m_allophoneSet.GetAllophone(phone);
}

void CSyllabifier::CorrectStressSymbol(sstring_vector& phoneList) const
{
    bool stressFound = false;
	sstring_vector::iterator it = phoneList.begin();
    while (it != phoneList.end())
	{
        sstring& s = *(it++);
        if (EndsWith(s, "1"))
		{
			s.erase(s.length() - 1);
            if (!stressFound)
			{
                int steps = 0;
                while (it != phoneList.begin())
				{
                    steps++;
                    const sstring& t = *(--it);
                    if (t == "-" || t == "_")
					{
                        it++;
                        steps--;
                        break;
                    }
                }
				size_t tempOffset = (it - phoneList.begin());
                phoneList.insert(it, "'");
				it = phoneList.begin() + tempOffset + 1;
                while (steps > 0)
				{
                    it++;
                    steps--;
                }
                stressFound = true;
            }
        }
    }

	if (!stressFound)
	{
        it = phoneList.begin();
        while (it != phoneList.end())
		{
            const sstring& s = *(it++);
            CPCAllophone ph = m_allophoneSet.GetAllophone(s);
            if (ph != NULL && ph->Sonority() >= 5)
			{
                int steps = 0;
                while (it != phoneList.begin())
				{
                    steps++;
                    const sstring& t = *(--it);
                    if (t == "-" || t == "_")
					{
                        it++;
                        steps--;
                        break;
                    }
                }
				size_t tempOffset = (it - phoneList.begin());
                phoneList.insert(it, "'");
				it = phoneList.begin() + tempOffset + 1;
                while (steps > 0)
				{
                    it++;
                    steps--;
                }
                break;
            }
        }
    }
}

sstring_vector CSyllabifier::SplitIntoAllophones(const sstring& phoneString) const
{
    sstring_vector phoneList;
    for (size_t i = 0; i < phoneString.length(); i++)
	{
        sstring name;
        for (size_t j = 3; j >= 1; j--)
		{
            if (i + j <= phoneString.length())
			{
                sstring candidate = phoneString.substr(i, j);
                if (GetAllophone(candidate) != NULL)
				{
                    name = candidate;
                    i += j - 1;
                    break;
                }
            }
        }
        if (!name.empty())
            phoneList.push_back(name);
    }
    return phoneList;
}
