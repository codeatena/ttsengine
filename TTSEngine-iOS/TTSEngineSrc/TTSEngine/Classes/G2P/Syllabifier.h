#pragma once

class CSyllabifier
{
public:
	CSyllabifier(const CAllophoneSet& allophoneSet);
	~CSyllabifier();

	sstring Syllabify(const sstring& phones) const;
	void Syllabify(sstring_vector& phoneList) const;

private:
	CPCAllophone GetAllophone(const sstring& phone) const;
	void CorrectStressSymbol(sstring_vector& phoneList) const;
	sstring_vector SplitIntoAllophones(const sstring& phoneString) const;

private:
	CAllophoneSet m_allophoneSet;
};
