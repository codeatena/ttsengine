#pragma once

class CFeatureDefinition
{
public:
	CFeatureDefinition();
	CFeatureDefinition(const CFeatureDefinition& other);
	CFeatureDefinition(std::ifstream& ifs);
	~CFeatureDefinition();

	CFeatureDefinition& operator = (const CFeatureDefinition& other);

	inline sstring GetFeatureName(int index) const { return m_featureNames[index]; }
	inline int GetFeatureIndex(const sstring& featureName) const { return m_featureNames[featureName]; }
	int GetNumberOfValues(int featureIndex) const;

	uint8_t GetFeatureValueAsByte(int featureIndex, const sstring& value) const;
	sstring GetFeatureValueAsString(int featureIndex, int value) const;

private:
	int						m_nNumByteFeatures;
	int						m_nNumShortFeatures;
	int						m_nNumContinuousFeatures;

	CByteStringTranslators	m_byteFeatureValues;
	CIntStringTranslator	m_featureNames;
	floats					m_featureWeights;
	CShortStringTranslators	m_shortFeatureValues;
	sstring_vector			m_floatWeightFuncts;
};
