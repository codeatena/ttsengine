#include "G2Pcommon.h"
#include "MaryHeader.h"
#include "ByteStringTranslator.h"
#include "IntStringTranslator.h"
#include "ShortStringTranslator.h"
#include "FeatureDefinition.h"
#include "FeatureVector.h"
#include "DecisionNode.h"
#include "CART.h"
#include "MaryCARTReader.h"
#include "Utility.h"
#include "StreamHelper.h"

CCART CMaryCARTReader::Load(const string& strFileName)
{
	std::ifstream ifs(Utf16_to_Utf8(strFileName).c_str(), std::ios_base::in|std::ios_base::binary);
	if (!ifs)
		throw "Unable to open the specified file for reading";

	CMaryHeader maryHeader(ifs);
	if (!maryHeader.HasCurrentVersion())
		throw "Wrong version of the database file";
	if (maryHeader.GetType() != MARYTYPE_CARTS)
		throw "No CARTs file";

	CProperties props;

	sstring strPropsStream = ReadUTF8(ifs);
	if (!strPropsStream.empty())
	{
		sstring_vector lstProps = Tokenize(strPropsStream, "\r\n");
		for (size_t i = 0; i < lstProps.size(); i++)
		{
			sstring_vector lstNameValue = Tokenize(lstProps[i], "=");
			if (lstNameValue.size() == 2)
				props[lstNameValue[0]] = lstNameValue[1];
		}
	}

	CFeatureDefinition featureDefinition(ifs);

	int numDecNodes = ReadInt32(ifs);

	PCDecisionNodes dns(numDecNodes);
	std::vector<ints> childIndexes(numDecNodes);

    for (int i = 0; i < numDecNodes; i++)
	{
		int featureNameIndex = ReadInt32(ifs);
		DecisionNodeType nodeType = (DecisionNodeType)ReadInt32(ifs);
        int numChildren = 2;

        switch (nodeType)
		{
		case BinaryByteDecisionNode:
			{
				int criterion = ReadInt32(ifs);
				dns[i] = new CBinaryByteDecisionNode(featureNameIndex, (uint8_t)criterion, featureDefinition);
			}
            break;
        case BinaryShortDecisionNode:
			{
				int criterion = ReadInt32(ifs);
				dns[i] = new CBinaryShortDecisionNode(featureNameIndex, (short)criterion, featureDefinition);
			}
            break;
        case BinaryFloatDecisionNode:
			{
				float floatCriterion = ReadFloat(ifs);
	            dns[i] = new CBinaryFloatDecisionNode(featureNameIndex, floatCriterion, featureDefinition);
			}
            break;
        case ByteDecisionNode:
			{
				numChildren = ReadInt32(ifs);
				if (featureDefinition.GetNumberOfValues(featureNameIndex) != numChildren)
					throw "Inconsistent cart file";
				dns[i] = new CByteDecisionNode(featureNameIndex, numChildren, featureDefinition);
			}
            break;
        case ShortDecisionNode:
			{
				numChildren = ReadInt32(ifs);
				if (featureDefinition.GetNumberOfValues(featureNameIndex) != numChildren)
					throw "Inconsistent cart file";
				dns[i] = new CShortDecisionNode(featureNameIndex, numChildren, featureDefinition);
			}
			break;
        }

		childIndexes[i].resize(numChildren);
        for (int k = 0; k < numChildren; k++)
            childIndexes[i][k] = ReadInt32(ifs);
    }

	int numLeafNodes = ReadInt32(ifs);
    PCLeafNodes lns(numLeafNodes);
    
    for (int j = 0; j < numLeafNodes; j++)
	{
        int leafTypeNr = ReadInt32(ifs);
		LeafNodeLeafType leafNodeType = (LeafNodeLeafType)leafTypeNr;
        switch (leafNodeType)
		{
		case IntArrayLeafNode:
			{
				int numData = ReadInt32(ifs);
				ints data(numData);
				for (int d = 0; d < numData; d++)
					data[d] = ReadInt32(ifs);
				lns[j] = new CIntArrayLeafNode(data);
			}
            break;
        case FloatLeafNode:
			{
				float stddev = ReadFloat(ifs);
				float mean = ReadFloat(ifs);
				lns[j] = new CFloatLeafNode(stddev, mean);
			}
            break;
        case IntAndFloatArrayLeafNode:
        case StringAndFloatLeafNode:
			{
				int numPairs = ReadInt32(ifs);
				ints ints0(numPairs);
				floats floats0(numPairs);
				for (int d = 0; d < numPairs; d++)
				{
					ints0[d] = ReadInt32(ifs);
					floats0[d] = ReadFloat(ifs);
				}
				if (leafNodeType == IntAndFloatArrayLeafNode) 
					lns[j] = new CIntAndFloatArrayLeafNode(ints0, floats0);
				else
					lns[j] = new CStringAndFloatLeafNode(ints0, floats0);
			}
            break;
        case FeatureVectorLeafNode:
			throw "Reading feature vector leaf nodes is not yet implemented";
        case PdfLeafNode:
            throw "Reading pdf leaf nodes is not yet implemented";
        }
    }
    
    for (int i = 0; i < numDecNodes; i++)
	{
        for (size_t k = 0; k < childIndexes[i].size(); k++)
		{
            int childIndex = childIndexes[i][k];
            if (childIndex < 0)
			{
                dns[i]->AddDaughter(dns[-childIndex - 1]);
            }
			else if (childIndex > 0)
			{
                dns[i]->AddDaughter(lns[childIndex - 1]);
            }
			else
			{
                dns[i]->AddDaughter(NULL);
            }
        }
    }
    
    PCNode rootNode = NULL;
    if (dns.size() > 0)
	{
        rootNode = dns[0];

		// DO NOT REMOVE - NEEDED!
		((PCDecisionNode)rootNode)->CountData();
    }
	else if (lns.size() > 0)
	{
        rootNode = lns[0];
    }

    return CCART(rootNode, featureDefinition, props);
}
