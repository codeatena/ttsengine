#include "G2Pcommon.h"
#include "Utility.h"

#include <stdio.h>

bool ReadAllText(const string& strFileName, sstring& strText)
{
	FILE *pFile = fopen(Utf16_to_Utf8(strFileName).c_str(),"rb");
	if (pFile == NULL)
		return false;

	fseek(pFile, 0, SEEK_END);
	long nFileSize = ftell(pFile);
	fseek(pFile, 0, SEEK_SET);

	strText.resize(nFileSize);
	bool bRc = (fread(const_cast<char *>(strText.c_str()), 1, nFileSize, pFile) == nFileSize);

	fclose(pFile);
	return bRc;
}

sstring_vector Tokenize(const sstring& strText, const char *pszDelimiters, bool squashEmpty)
{
	sstring_vector lstResult;

	sstring::size_type nNextPos, nPrevPos = 0;
	while ((nNextPos = strText.find_first_of(pszDelimiters, nPrevPos)) != sstring::npos)
	{
		if (nNextPos > nPrevPos || !squashEmpty)
		{
			lstResult.push_back(strText.substr(nPrevPos, nNextPos - nPrevPos));
		}

		nPrevPos = nNextPos + 1;
	}

	if (!squashEmpty || strchr(pszDelimiters, strText[nPrevPos]) == NULL)
		lstResult.push_back(strText.substr(nPrevPos));

	return lstResult;
}
