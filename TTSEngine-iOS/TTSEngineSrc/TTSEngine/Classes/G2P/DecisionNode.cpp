#include "G2Pcommon.h"
#include "ByteStringTranslator.h"
#include "IntStringTranslator.h"
#include "ShortStringTranslator.h"
#include "FeatureDefinition.h"
#include "FeatureVector.h"
#include "DecisionNode.h"

CNode::CNode(bool isRoot)
	: m_isRoot(isRoot)
	, m_mother(NULL)
	, m_nodeIndex(0)
{
}

CNode::~CNode()
{
}

void CNode::SetMother(PCNode node, int nodeIndex)
{
	m_mother = node;
	m_nodeIndex = nodeIndex;
}

CDecisionNode::CDecisionNode(int featureIndex, int numDaughters, const CFeatureDefinition& featureDefinition)
	: CNode(false)
	, m_featureIndex(featureIndex)
	, m_feature(featureDefinition.GetFeatureName(featureIndex))
	, m_daughters(numDaughters)
	, m_featureDefinition(featureDefinition)
	, m_lastDaughter(0)
	, m_nData(0)
	, m_uniqueDecisionNodeId(0)
{
}

CDecisionNode::~CDecisionNode()
{
}

void CDecisionNode::AddDaughter(PCNode daughter)
{
    if (m_lastDaughter > m_daughters.size() - 1)
		throw "Can not add daughter";

	m_daughters[m_lastDaughter] = daughter;
    if (daughter != NULL)
        daughter->SetMother(this, m_lastDaughter);

	m_lastDaughter++;
}

void CDecisionNode::CountData()
{
    m_nData = 0;
    for (size_t i = 0; i < m_daughters.size(); i++)
	{
		if(m_daughters[i] != NULL)
		{
			PCDecisionNode pCastDaughter = dynamic_cast<PCDecisionNode>(m_daughters[i]);
			if (pCastDaughter != NULL)
				pCastDaughter->CountData();

            m_nData += m_daughters[i]->GetNumberOfData();
        }
    }
}

CBinaryByteDecisionNode::CBinaryByteDecisionNode(int featureIndex, uint8_t value, const CFeatureDefinition& featureDefinition)
	: CDecisionNode(featureIndex, 2, featureDefinition)
	, m_value(value)
{
}

PCNode CBinaryByteDecisionNode::GetNextNode(const CFeatureVector& featureVector) const
{
    uint8_t val = featureVector.GetByteFeature(m_featureIndex);
    
	PCNode returnNode = NULL;
    if (val == m_value)
        returnNode = m_daughters[0];
    else
        returnNode = m_daughters[1];
    return returnNode;
}

CBinaryShortDecisionNode::CBinaryShortDecisionNode(int featureIndex, short value, const CFeatureDefinition& featureDefinition)
	: CDecisionNode(featureIndex, 2, featureDefinition)
	, m_value(value)
{
}

PCNode CBinaryShortDecisionNode::GetNextNode(const CFeatureVector& featureVector) const
{
    short val = featureVector.GetShortFeature(m_featureIndex);
    
	PCNode returnNode = NULL;
    if (val == m_value)
        returnNode = m_daughters[0];  
    else
        returnNode = m_daughters[1];  
    return returnNode;
}

CBinaryFloatDecisionNode::CBinaryFloatDecisionNode(int featureIndex, float value, const CFeatureDefinition& featureDefinition)
	: CDecisionNode(featureIndex, 2, featureDefinition)
	, m_value(value)
	, m_isByteFeature(false)
{
}

PCNode CBinaryFloatDecisionNode::GetNextNode(const CFeatureVector& featureVector) const
{
    float val;

    if (m_isByteFeature) 
        val = (float) featureVector.GetByteFeature(m_featureIndex);
    else
        val = featureVector.GetContinuousFeature(m_featureIndex);
    
	PCNode returnNode = NULL;
    if (val < m_value)
        returnNode = m_daughters[0];
    else
        returnNode = m_daughters[1];
    return returnNode;
}

CByteDecisionNode::CByteDecisionNode(int featureIndex, int numDaughters, const CFeatureDefinition& featureDefinition)
	: CDecisionNode(featureIndex, numDaughters, featureDefinition)
{
}

PCNode CByteDecisionNode::GetNextNode(const CFeatureVector& featureVector) const
{
    uint8_t val = featureVector.GetByteFeature(m_featureIndex);
    return m_daughters[val];
}

CShortDecisionNode::CShortDecisionNode(int featureIndex, int numDaughters, const CFeatureDefinition& featureDefinition)
	: CDecisionNode(featureIndex, numDaughters, featureDefinition)
{
}

PCNode CShortDecisionNode::GetNextNode(const CFeatureVector& featureVector) const
{
    short val = featureVector.GetShortFeature(m_featureIndex);
    return m_daughters[val];
}

CLeafNode::CLeafNode(int uniqueLeafId)
	: CNode(false)
	, m_uniqueLeafId(uniqueLeafId)
{
}

CIntArrayLeafNode::CIntArrayLeafNode(const ints& data)
	: m_data(data)
{
}

CFloatLeafNode::CFloatLeafNode(float value1, float value2)
{
	m_data.push_back(value1);
	m_data.push_back(value2);
}

CIntAndFloatArrayLeafNode::CIntAndFloatArrayLeafNode(const ints& data, const floats& floats0)
	: CIntArrayLeafNode(data)
	, m_floats(floats0)
{
}

int CIntAndFloatArrayLeafNode::MostProbableInt() const
{
    int bestInd = 0;
    float maxProb = 0;
    
    for (size_t i = 0 ; i < m_data.size(); i++)
	{
        if (m_floats[i] > maxProb)
		{
            maxProb = m_floats[i];
            bestInd = m_data[i];
        }
    }

    return bestInd;
}

CStringAndFloatLeafNode::CStringAndFloatLeafNode(const ints& data, const floats& floats0)
	: CIntAndFloatArrayLeafNode(data, floats0)
{
}

sstring CStringAndFloatLeafNode::MostProbableString(const CFeatureDefinition& featureDefinition, int featureIndex) const
{
    int bestInd = MostProbableInt();
    return featureDefinition.GetFeatureValueAsString(featureIndex, bestInd);
}
