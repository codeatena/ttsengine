#pragma once

class CMaryCARTReader
{
public:
	static CCART Load(const string& strFileName);

private:
	CMaryCARTReader();
};
