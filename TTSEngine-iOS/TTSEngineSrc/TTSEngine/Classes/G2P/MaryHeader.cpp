#include "G2Pcommon.h"
#include "MaryHeader.h"
#include "StreamHelper.h"

CMaryHeader::CMaryHeader(std::ifstream& ifs)
{
	Load(ifs);

	if (!HasLegalMagic() || !HasLegalType())
		throw "Ill-formed Mary header!";
}

CMaryHeader::~CMaryHeader()
{
}

void CMaryHeader::Load(std::ifstream& ifs)
{
	m_nMagic = ReadInt32(ifs);
	m_nVersion = ReadInt32(ifs);
	m_nType = ReadInt32(ifs);
}
