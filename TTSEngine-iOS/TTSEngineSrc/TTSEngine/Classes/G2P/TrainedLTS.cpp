#include "G2Pcommon.h"
#include "Allophone.h"
#include "AllophoneSet.h"
#include "ByteStringTranslator.h"
#include "IntStringTranslator.h"
#include "ShortStringTranslator.h"
#include "FeatureDefinition.h"
#include "FeatureVector.h"
#include "DecisionNode.h"
#include "CART.h"
#include "MaryCARTReader.h"
#include "TrainedLTS.h"
#include "Syllabifier.h"

static sstring PREDICTED_STRING_FEATURENAME = "predicted-string";

CTrainedLTS::CTrainedLTS(const string& strAllophoneFileName, const string& strTreeFilename)
	: m_allophoneSet(strAllophoneFileName)
	, m_indexPredictedFeature(0)
	, m_context(0)
	, m_convertToLowercase(false)
	, m_tree(CMaryCARTReader::Load(strTreeFilename))
{
    m_featureDefinition = m_tree.GetFeatureDefinition();
    m_indexPredictedFeature = m_featureDefinition.GetFeatureIndex(PREDICTED_STRING_FEATURENAME);
    const CProperties& props = m_tree.GetProperties();
    if (props.size() == 0)
		throw "Prediction tree does not contain properties";
    
	CProperties::const_iterator it = props.find("lowercase");
	if (it != props.end())
		m_convertToLowercase = (StringEquals((*it).second, "true", false) == true);

	it = props.find("context");
	if (it != props.end())
		m_context = atoi((*it).second.c_str());
}

CTrainedLTS::~CTrainedLTS()
{
}

sstring CTrainedLTS::PredictPronunciation(const sstring& graphemes0) const
{
	sstring graphemes = graphemes0;
    if (m_convertToLowercase)
		std::transform(graphemes.begin(), graphemes.end(), graphemes.begin(), ::tolower);

    sstring returnStr;

    for (size_t i = 0 ; i < graphemes.length() ; i++)
	{
        bytes byteFeatures(2*m_context + 1);

        for (int fnr = 0; fnr < 2*m_context + 1; fnr++)
		{
            int pos = i - m_context + fnr;

            sstring grAtPos = (pos < 0 || pos >= (int)graphemes.length())
				? "null"
				: graphemes.substr(pos, 1);

            try 
			{
                byteFeatures[fnr] = m_tree.GetFeatureDefinition().GetFeatureValueAsByte(fnr, grAtPos);
            }
			catch (std::exception&)
			{
                // Silently ignore unknown characters
                byteFeatures[fnr] = m_tree.GetFeatureDefinition().GetFeatureValueAsByte(fnr, "null");
            }
        }

        CFeatureVector fv(byteFeatures, shorts(), floats(), 0);

        PCStringAndFloatLeafNode leaf = (PCStringAndFloatLeafNode) m_tree.InterpretToNode(fv, 0);
        sstring prediction = leaf->MostProbableString(m_featureDefinition, m_indexPredictedFeature);
        returnStr += prediction.substr(1, prediction.length() - 2);
    }

    return returnStr;        
}

sstring CTrainedLTS::Syllabify(const sstring& phones) const
{
    return CSyllabifier(m_allophoneSet).Syllabify(phones);
}
