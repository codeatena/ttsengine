#pragma once

class CShortStringTranslator
{
public:
	CShortStringTranslator();
	CShortStringTranslator(const CShortStringTranslator& other);
	~CShortStringTranslator();

	CShortStringTranslator& operator = (const CShortStringTranslator& other);
	sstring operator[] (short b) const;

	void Resize(int nRange);
	void Set(short b, const sstring& s);
	inline int GetNumberOfValues() const { return m_list.size(); }

private:
	sstring_vector				m_list;
	std::map<sstring, short>	m_map;
};

typedef CShortStringTranslator PCShortStringTranslator;
typedef std::vector<CShortStringTranslator> CShortStringTranslators;
