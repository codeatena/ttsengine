#pragma once

inline uint8_t ReadByte(std::ifstream& ifs)
{
	uint8_t cByte;
	ifs.read((char *)&cByte, sizeof(cByte));
	if (ifs.fail())
		throw "Read Error";

	return cByte;
}

inline int16_t ReadInt16(std::ifstream& ifs)
{
	uint8_t cBuffer[2];
	ifs.read((char *)cBuffer, sizeof(cBuffer));
	if (ifs.fail())
		throw "Read Error";

	return (((int16_t)cBuffer[0] & 0xFF) << 8) | ((int16_t)cBuffer[1] & 0xFF);
}

inline int32_t ReadInt32(std::ifstream& ifs)
{
	uint8_t cBuffer[4];
	ifs.read((char *)cBuffer, sizeof(cBuffer));
	if (ifs.fail())
		throw "Read Error";

	return (((int32_t)cBuffer[0] & 0xFF) << 24) 
		| (((int32_t)cBuffer[1] & 0xFF) << 16)
		| (((int32_t)cBuffer[2] & 0xFF) << 8)
		| ((int32_t)cBuffer[3] & 0xFF);
}

inline int64_t ReadInt64(std::ifstream& ifs)
{
	uint8_t cBuffer[8];
	ifs.read((char *)cBuffer, sizeof(cBuffer));
	if (ifs.fail())
		throw "Read Error";

	return (((int64_t)cBuffer[0] & 0xFF) << 56) 
		| (((int64_t)cBuffer[1] & 0xFF) << 48)
		| (((int64_t)cBuffer[2] & 0xFF) << 40)
		| (((int64_t)cBuffer[3] & 0xFF) << 32)
		| (((int64_t)cBuffer[4] & 0xFF) << 24) 
		| (((int64_t)cBuffer[5] & 0xFF) << 16)
		| (((int64_t)cBuffer[6] & 0xFF) << 8)
		| ((int64_t)cBuffer[7] & 0xFF);
}

inline double ReadDouble(std::ifstream& ifs)
{
	int64_t nLongValue = ReadInt64(ifs);

	if (nLongValue == 0x7ff0000000000000L)
		throw "The number is positive infinity";
	if (nLongValue == 0xfff0000000000000L)
		throw "The number is negative infinity";

	int32_t s = ((nLongValue >> 63) == 0) ? 1 : -1;
	int32_t e = (int32_t)((nLongValue >> 52) & 0x7ffL);
	int64_t m = (e == 0) ? (nLongValue & 0xfffffffffffffL) << 1 : (nLongValue & 0xfffffffffffffL) | 0x10000000000000L;

	return s * m * pow((double)2, e - 1075);
}

inline float ReadFloat(std::ifstream& ifs)
{
	int32_t nValue = ReadInt32(ifs);

	if (nValue == 0x7f800000u)
		throw "The number is positive infinity";
	if (nValue == 0xff800000u)
		throw "The number is negative infinity";

	int32_t s = ((nValue >> 31) == 0) ? 1 : -1;
	int32_t e = ((nValue >> 23) & 0xff);
	int32_t m = (e == 0) ? (nValue & 0x7fffff) << 1 : (nValue & 0x7fffff) | 0x800000;

	return s * m * pow((float)2, e - 150);
}

inline std::string ReadUTF8(std::ifstream& ifs)
{
	int16_t nLength = ReadInt16(ifs);

	std::string strValue;
	strValue.resize(nLength);
	ifs.read(const_cast<char *>(strValue.c_str()), nLength);
	if (ifs.fail())
		throw "Read Error";

	return strValue;
}
