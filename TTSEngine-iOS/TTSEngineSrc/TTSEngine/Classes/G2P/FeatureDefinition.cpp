#include "G2Pcommon.h"
#include "ByteStringTranslator.h"
#include "IntStringTranslator.h"
#include "ShortStringTranslator.h"
#include "FeatureDefinition.h"
#include "StreamHelper.h"

CFeatureDefinition::CFeatureDefinition()
	: m_nNumByteFeatures(0)
	, m_nNumShortFeatures(0)
	, m_nNumContinuousFeatures(0)
{
}

CFeatureDefinition::CFeatureDefinition(const CFeatureDefinition& other)
	: m_nNumByteFeatures(other.m_nNumByteFeatures)
	, m_nNumShortFeatures(other.m_nNumShortFeatures)
	, m_nNumContinuousFeatures(other.m_nNumContinuousFeatures)
	, m_byteFeatureValues(other.m_byteFeatureValues)
	, m_featureNames(other.m_featureNames)
	, m_featureWeights(other.m_featureWeights)
	, m_shortFeatureValues(other.m_shortFeatureValues)
	, m_floatWeightFuncts(other.m_floatWeightFuncts)
{
}

CFeatureDefinition::CFeatureDefinition(std::ifstream& ifs)
	: m_nNumByteFeatures(0)
	, m_nNumShortFeatures(0)
	, m_nNumContinuousFeatures(0)
{
	m_nNumByteFeatures = ReadInt32(ifs);

	m_byteFeatureValues.resize(m_nNumByteFeatures);
	m_featureNames.Resize(m_nNumByteFeatures);
	m_featureWeights.resize(m_nNumByteFeatures);

	for (int i = 0; i < m_nNumByteFeatures; i++)
	{
		m_featureWeights[i] = ReadFloat(ifs);
		m_featureNames.Set(i, ReadUTF8(ifs));

		size_t numberOfValues = ReadByte(ifs);

		m_byteFeatureValues[i].Resize(numberOfValues);

		for (size_t b = 0; b < numberOfValues; b++)
			m_byteFeatureValues[i].Set(b, ReadUTF8(ifs));
	}

	m_nNumShortFeatures = ReadInt32(ifs);
	if (m_nNumShortFeatures > 0)
	{
		m_shortFeatureValues.resize(m_nNumShortFeatures);
		m_featureWeights.resize(m_nNumByteFeatures + m_nNumShortFeatures);

		for (int i = 0; i < m_nNumShortFeatures; i++)
		{
			m_featureWeights[m_nNumByteFeatures + i] = ReadFloat(ifs);
			m_featureNames.Set(m_nNumByteFeatures + i, ReadUTF8(ifs));

			short numberOfValues = ReadInt16(ifs);

			m_shortFeatureValues[i].Resize(numberOfValues);

			for (short s = 0; s < numberOfValues; s++)
				m_shortFeatureValues[i].Set(s, ReadUTF8(ifs));
		}
	}

	m_nNumContinuousFeatures = ReadInt32(ifs);

	if (m_nNumContinuousFeatures > 0)
	{
		m_floatWeightFuncts.resize(m_nNumContinuousFeatures);
		m_featureWeights.resize(m_nNumByteFeatures + m_nNumShortFeatures + m_nNumContinuousFeatures);

		for (int i = 0; i < m_nNumContinuousFeatures; i++)
		{
			m_featureWeights[m_nNumByteFeatures + m_nNumShortFeatures + i] = ReadFloat(ifs);
			m_floatWeightFuncts[i] = ReadUTF8(ifs);
			m_featureNames.Set(m_nNumByteFeatures + m_nNumShortFeatures + i, ReadUTF8(ifs));
		}
	}
}

CFeatureDefinition::~CFeatureDefinition()
{
}

CFeatureDefinition& CFeatureDefinition::operator = (const CFeatureDefinition& other)
{
	m_nNumByteFeatures = other.m_nNumByteFeatures;
	m_nNumShortFeatures = other.m_nNumShortFeatures;
	m_nNumContinuousFeatures = other.m_nNumContinuousFeatures;
	m_byteFeatureValues = other.m_byteFeatureValues;
	m_featureNames = other.m_featureNames;
	m_featureWeights = other.m_featureWeights;
	m_shortFeatureValues = other.m_shortFeatureValues;
	m_floatWeightFuncts = other.m_floatWeightFuncts;

	return *this;
}

int CFeatureDefinition::GetNumberOfValues(int featureIndex) const
{
    if (featureIndex < m_nNumByteFeatures)
        return m_byteFeatureValues[featureIndex].GetNumberOfValues();
    featureIndex -= m_nNumByteFeatures;
    if (featureIndex < m_nNumShortFeatures)
        return m_shortFeatureValues[featureIndex].GetNumberOfValues();
	throw "The feature is not a byte-valued or short-valued feature";
}

uint8_t CFeatureDefinition::GetFeatureValueAsByte(int featureIndex, const sstring& value) const
{
    if (featureIndex >= m_nNumByteFeatures)
		throw "The requested feature is not a byte-valued feature";

    return m_byteFeatureValues[featureIndex][value];
}

sstring CFeatureDefinition::GetFeatureValueAsString(int featureIndex, int value) const
{
	if (featureIndex < m_nNumByteFeatures)
		return m_byteFeatureValues[featureIndex][(uint8_t)value];
	featureIndex -= m_nNumByteFeatures;
	if (featureIndex < m_nNumShortFeatures)
		return m_shortFeatureValues[featureIndex][(short)value];

	throw "The requested feature is not a byte-valued or short-valued feature";
}
