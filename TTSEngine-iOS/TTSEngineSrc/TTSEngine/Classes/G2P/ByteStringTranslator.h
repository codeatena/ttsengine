#pragma once

class CByteStringTranslator
{
public:
	CByteStringTranslator();
	CByteStringTranslator(const CByteStringTranslator& other);

	~CByteStringTranslator();

	CByteStringTranslator& operator = (const CByteStringTranslator& other);
	uint8_t operator[] (const sstring& s) const;
	sstring operator[] (int8_t b) const;

	void Resize(int nRange);
	void Set(uint8_t b, const sstring& s);
	inline int GetNumberOfValues() const { return m_list.size(); }

private:
	sstring_vector				m_list;
	std::map<sstring, uint8_t>	m_map;
};

typedef CByteStringTranslator *PCByteStringTranslator;
typedef std::vector<CByteStringTranslator> CByteStringTranslators;
