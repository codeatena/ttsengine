#pragma once

class CIntStringTranslator
{
public:
	CIntStringTranslator();
	CIntStringTranslator(const CIntStringTranslator& other);

	~CIntStringTranslator();

	CIntStringTranslator& operator = (const CIntStringTranslator& other);
	inline sstring operator [] (int index) const { return m_list[index]; }
	int operator[] (const sstring& s) const;

	void Resize(int nRange);
	void Set(int i, const sstring& s);

private:
	sstring_vector			m_list;
	std::map<sstring, int>	m_map;
};

typedef CIntStringTranslator *PCIntStringTranslator;
typedef std::vector<CIntStringTranslator> CIntStringTranslators;
