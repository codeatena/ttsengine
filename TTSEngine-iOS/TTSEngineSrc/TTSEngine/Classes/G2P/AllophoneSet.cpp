#include "G2Pcommon.h"
#include "Allophone.h"
#include "AllophoneSet.h"
#include "Utility.h"

CAllophoneSet::CAllophoneSet(const string& strFilename)
	: m_nMaxAllophoneSymbolLength(1)
{
	sstring strFileContent;
	if (!ReadAllText(strFilename, strFileContent))
		throw "Unable to load the specified file into memory";

	rapidxml::xml_document<> doc;
	doc.parse<0>(const_cast<char *>(strFileContent.c_str()));

	rapidxml::xml_node<> *pRoot = doc.first_node();
	rapidxml::xml_attribute<> *pName = pRoot->first_attribute("name");
	if (pName != NULL)
		m_strName = pName->value();

	sstring_vector lstFeatures;
	rapidxml::xml_attribute<> *pFeatures = pRoot->first_attribute("features");
	if (pFeatures != NULL)
		lstFeatures = Tokenize(pFeatures->value(), " ");

	// HACK: the allophones XML file is expected to be flat
	for (rapidxml::xml_node<> *pNode = pRoot->first_node(); pNode != NULL; pNode = pNode->next_sibling())
	{
		if (strcmp(pNode->name(), "vowel") == 0
			|| strcmp(pNode->name(), "consonant") == 0
			|| strcmp(pNode->name(), "silence") == 0
			|| strcmp(pNode->name(), "tone") == 0)
		{
			CAllophone ap(pNode, lstFeatures);
			if (m_mapAllophones.find(ap.GetName()) != m_mapAllophones.end())
				throw "Duplicate allophone definition";
			m_mapAllophones[ap.GetName()] = ap;

            if (ap.IsPause())
			{
                if (!m_strSilenceAllophoneName.empty())
					throw "The file contains more than one silence symbol";
                m_strSilenceAllophoneName = ap.GetName();
            }

			size_t nLen = ap.GetName().length();
			if (nLen > m_nMaxAllophoneSymbolLength)
				m_nMaxAllophoneSymbolLength = nLen;
		}
	}

	if (m_strSilenceAllophoneName.empty())
		throw "The file does not contain a silence symbol";

	for (size_t i = 0; i < lstFeatures.size(); i++)
	{
		const sstring& strFeature = lstFeatures[i];

		sstring_vector lstFeatureValueSet;
		for (std::map<sstring, CAllophone>::const_iterator it = m_mapAllophones.begin(); it != m_mapAllophones.end(); it++)
		{
			const CAllophone& ap = (*it).second;
			lstFeatureValueSet.push_back(ap.GetFeature(strFeature));
		}
		sstring_vector::iterator itZero = std::find(lstFeatureValueSet.begin(), lstFeatureValueSet.end(), "0");
		if (itZero != lstFeatureValueSet.end())
			lstFeatureValueSet.erase(itZero);
		std::sort(lstFeatureValueSet.begin(), lstFeatureValueSet.end());
		lstFeatureValueSet.insert(lstFeatureValueSet.begin(), "0");

		m_mapFeatureValues[strFeature] = lstFeatureValueSet;
	}
}

CAllophoneSet::CAllophoneSet(const CAllophoneSet& other)
	: m_mapAllophones(other.m_mapAllophones)
	, m_mapFeatureValues(other.m_mapFeatureValues)
	, m_strName(other.m_strName)
	, m_nMaxAllophoneSymbolLength(other.m_nMaxAllophoneSymbolLength)
	, m_strSilenceAllophoneName(other.m_strSilenceAllophoneName)
{
}

CAllophoneSet::~CAllophoneSet()
{
}

CAllophoneSet& CAllophoneSet::operator = (const CAllophoneSet& other)
{
	m_mapAllophones = other.m_mapAllophones;
	m_mapFeatureValues = other.m_mapFeatureValues;
	m_strName = other.m_strName;
	m_nMaxAllophoneSymbolLength = other.m_nMaxAllophoneSymbolLength;
	m_strSilenceAllophoneName = other.m_strSilenceAllophoneName;

	return *this;
}

CPCAllophone CAllophoneSet::GetAllophone(const sstring& ph) const
{
	std::map<sstring, CAllophone>::const_iterator it = m_mapAllophones.find(ph);
	if (it == m_mapAllophones.end())
		return NULL;
	return &(*it).second;
}
