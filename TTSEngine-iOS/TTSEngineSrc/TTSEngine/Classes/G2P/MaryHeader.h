#pragma once

#define MARYHEADER_MAGIC 0x4d415259u
#define MARYHEADER_VERSION 40

/* List of authorized file type identifier constants */
#define MARYTYPE_UNKNOWN 0
#define MARYTYPE_CARTS 100
#define MARYTYPE_DIRECTED_GRAPH 110
#define MARYTYPE_UNITS 200
#define MARYTYPE_LISTENERUNITS 225
#define MARYTYPE_UNITFEATS 300
#define MARYTYPE_LISTENERFEATS 325
#define MARYTYPE_HALFPHONE_UNITFEATS 301
#define MARYTYPE_JOINFEATS 400
#define MARYTYPE_SCOST 445
#define MARYTYPE_PRECOMPUTED_JOINCOSTS 450
#define MARYTYPE_TIMELINE 500

class CMaryHeader
{
public:
	CMaryHeader(std::ifstream& ifs);
	~CMaryHeader();

	inline bool HasCurrentVersion() const { return m_nVersion == MARYHEADER_VERSION; }
	inline unsigned int GetType() const { return m_nType; }

private:
	void Load(std::ifstream& ifs);

	inline bool HasLegalMagic() const { return m_nMagic == MARYHEADER_MAGIC; }
	inline bool HasLegalType() const { return m_nType <= MARYTYPE_TIMELINE && m_nType > MARYTYPE_UNKNOWN; }

private:
	unsigned int	m_nMagic;
	unsigned int	m_nVersion;
	unsigned int	m_nType;
};
