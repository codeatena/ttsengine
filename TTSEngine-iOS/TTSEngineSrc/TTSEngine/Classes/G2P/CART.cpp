#include "G2Pcommon.h"
#include "ByteStringTranslator.h"
#include "IntStringTranslator.h"
#include "ShortStringTranslator.h"
#include "FeatureDefinition.h"
#include "FeatureVector.h"
#include "DecisionNode.h"
#include "CART.h"

CDirectedGraph::CDirectedGraph(PCNode rootNode, const CFeatureDefinition& featDef, const CProperties& properties)
	: m_rootNode(rootNode)
	, m_featDef(featDef)
	, m_properties(properties)
{
}

CDirectedGraph::~CDirectedGraph()
{
}

CCART::CCART(PCNode rootNode, const CFeatureDefinition& featDef, const CProperties& properties)
	: CDirectedGraph(rootNode, featDef, properties)
{
}

CCART::CCART(const CCART& other)
	: CDirectedGraph(other.m_rootNode, other.m_featDef, other.m_properties)
{
}

CCART::~CCART()
{
}

PCNode CCART::InterpretToNode(const CFeatureVector& featureVector, int minNumberOfData) const
{
    PCNode currentNode = m_rootNode;
    PCNode prevNode = NULL;

    while (currentNode != NULL 
		&& currentNode->GetNumberOfData() > minNumberOfData
		&& dynamic_cast<PCLeafNode>(currentNode) == NULL)
	{
        prevNode = currentNode;
        currentNode = ((PCDecisionNode) currentNode)->GetNextNode(featureVector);
    }

	if (currentNode == NULL
		|| currentNode->GetNumberOfData() < minNumberOfData
		&& prevNode != NULL)
	{
        currentNode = prevNode;
    }

    return currentNode;
}
