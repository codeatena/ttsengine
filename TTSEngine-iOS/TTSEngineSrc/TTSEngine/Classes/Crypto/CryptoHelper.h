//
//  CryptoHelper.h
//  LaunchBag
//
//  Created by Asatur Galstyan on 09/11/2012.
//  Copyright (c) 2012 ZanazanSystems. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonCryptor.h>
#import <CommonCrypto/CommonKeyDerivation.h>
#import "NSData+Base64.h"

@interface CryptoHelper : NSObject

+(NSString*) generateKey;
+(NSData*) encryptData:(NSData*)data usingKey:(NSString*)key;
+(NSData*) decryptData:(NSData*)data usingKey:(NSString*)key;


@end
