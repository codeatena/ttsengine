//
//  CryptoHelper.m
//  LaunchBag
//
//  Created by Asatur Galstyan on 09/11/2012.
//  Copyright (c) 2012 ZanazanSystems. All rights reserved.
//

#import "CryptoHelper.h"


const CCAlgorithm kAlgorithm = kCCAlgorithmAES128;
const NSUInteger kAlgorithmKeySize = kCCKeySizeAES256;
const NSUInteger kAlgorithmBlockSize = kCCBlockSizeAES128;
const NSUInteger kAlgorithmIVSize = kCCBlockSizeAES128;
const NSUInteger kPBKDFRounds = 10000;

@implementation CryptoHelper

+ (NSData *)randomDataOfLength:(size_t)length {
    NSMutableData *data = [NSMutableData dataWithLength:length];
    
    int result = SecRandomCopyBytes(kSecRandomDefault,
                                    length,
                                    data.mutableBytes);
    NSAssert(result == 0, @"Unable to generate random bytes: %d",  errno);
    
    return data;
}

+(NSString*) generateKey {

    NSData *iv = [self randomDataOfLength:kAlgorithmIVSize];    
    
    NSMutableData * derivedKey = [NSMutableData dataWithLength:kAlgorithmKeySize];
    
    NSString *password = @"";
    
    int result = CCKeyDerivationPBKDF(kCCPBKDF2,               
                                      password.UTF8String,                    
                                      password.length,                       
                                      iv.bytes,                
                                      iv.length,               
                                      kCCPRFHmacAlgSHA1,       
                                      kPBKDFRounds,            
                                      derivedKey.mutableBytes, 
                                      derivedKey.length);      
    
    if (result == kCCSuccess) {

        NSString *sDerivedKey = [derivedKey base64EncodedString];
        return sDerivedKey;
    }

    return nil;
}

+(NSData*) encryptData:(NSData*)data usingKey:(NSString*)key{
    
    char keyPtr[kCCKeySizeAES256+1];
	bzero(keyPtr, sizeof(keyPtr)); 
	
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
  
	NSUInteger dataLength = [data length];
	
	size_t bufferSize = dataLength + kCCBlockSizeAES128;
	void *buffer = malloc(bufferSize);
	
	size_t numBytesEncrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCEncrypt,
                                          kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding,
                                          keyPtr,
                                          kCCKeySizeAES256,
                                          NULL,
                                          [data bytes],
                                          dataLength, /* input */
                                          buffer,
                                          bufferSize, /* output */
                                          &numBytesEncrypted);
    
	if (cryptStatus == kCCSuccess) {
		return [NSData dataWithBytesNoCopy:buffer length:numBytesEncrypted];
	}
    
	free(buffer);
    return nil;
}

+(NSData*) decryptData:(NSData*)data usingKey:(NSString*)key{
    
    char keyPtr[kCCKeySizeAES256+1];
	bzero(keyPtr, sizeof(keyPtr));
	
	[key getCString:keyPtr maxLength:sizeof(keyPtr) encoding:NSUTF8StringEncoding];
	
	NSUInteger dataLength = [data length];
	
	size_t bufferSize = dataLength + kCCBlockSizeAES128;
	void *buffer = malloc(bufferSize);
	
	size_t numBytesDecrypted = 0;
	CCCryptorStatus cryptStatus = CCCrypt(kCCDecrypt,
                                          kCCAlgorithmAES128,
                                          kCCOptionPKCS7Padding,
                                          keyPtr,
                                          kCCKeySizeAES256,
                                          NULL,
                                          [data bytes],
                                          dataLength,
                                          buffer,
                                          bufferSize,
                                          &numBytesDecrypted);
	
	if (cryptStatus == kCCSuccess) {
		return [NSData dataWithBytesNoCopy:buffer length:numBytesDecrypted];
	}
	
	free(buffer);
    return nil;
}

@end
