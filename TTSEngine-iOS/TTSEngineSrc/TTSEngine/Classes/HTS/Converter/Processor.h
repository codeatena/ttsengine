#pragma once

#define MISSING_PHONEME "x"
#define MISSING_INDEX 0

inline bool IsPhoneme(char ch)
{
	return (ch != ' ' && ch != '.' && ch != '#' && ch != ',');
}

inline bool IsPause(char ch)
{
	return (ch == ',' || ch == '_');
}

inline bool IsWordDelim(char ch)
{
	return (ch == ' ' || ch == ',');
}

inline bool IsPhraseDelim(char ch)
{
	return (ch == ',');
}

inline bool IsVowel(char ch)
{
	return (ch == 'a' || ch == 'e' || ch == 'i' || ch == 'o' || ch == 'u');
}

class CPhones
{
private:
	std::string			m_strPhones;

	std::vector<size_t>	m_lstSyllableOffsets;
	std::vector<size_t>	m_lstSyllableEnds;
	std::vector<size_t>	m_lstSyllableSizes;

	std::vector<size_t>	m_lstWordOffsets;
	std::vector<size_t>	m_lstWordEnds;
	std::vector<size_t>	m_lstWordSizes;

	std::vector<size_t>	m_lstPhraseOffsets;
	std::vector<size_t>	m_lstPhraseEnds;
	std::vector<size_t>	m_lstPhraseSizes;

	// Mapping phoneme offsets in the input utterance to their timings
	std::map<size_t, CDecimal>
						m_mapTimings;

public:
	CPhones(const std::string& phones)
		: m_strPhones(phones)
	{
		for (size_t pos = 0; pos < m_strPhones.length(); pos++)
		{
			if (::IsPhoneme(m_strPhones[pos])
				&& (pos == 0 || !::IsPhoneme(m_strPhones[pos - 1])))
			{
				m_lstSyllableOffsets.push_back(pos);
			}

			if (!::IsPhoneme(m_strPhones[pos])
				&& pos > 0
				&& ::IsPhoneme(m_strPhones[pos - 1]))
			{
				m_lstSyllableEnds.push_back(pos - 1);
				m_lstSyllableSizes.push_back(pos - m_lstSyllableOffsets[m_lstSyllableOffsets.size() - 1]);
			}

			if (!::IsWordDelim(m_strPhones[pos])
				&& (pos == 0 || ::IsWordDelim(m_strPhones[pos - 1])))
			{
				m_lstWordOffsets.push_back(pos);
			}

			if (::IsWordDelim(m_strPhones[pos])
				&& pos > 0
				&& !::IsWordDelim(m_strPhones[pos - 1]))
			{
				m_lstWordEnds.push_back(pos - 1);
				m_lstWordSizes.push_back(pos - m_lstWordOffsets[m_lstWordOffsets.size() - 1]);
			}

			if (!::IsPhraseDelim(m_strPhones[pos])
				&& (pos == 0 || ::IsPhraseDelim(m_strPhones[pos - 1])))
			{
				m_lstPhraseOffsets.push_back(pos);
			}

			if (::IsPhraseDelim(m_strPhones[pos])
				&& pos > 0
				&& !::IsPhraseDelim(m_strPhones[pos - 1]))
			{
				m_lstPhraseEnds.push_back(pos - 1);
				m_lstPhraseSizes.push_back(pos - m_lstPhraseOffsets[m_lstPhraseOffsets.size() - 1]);
			}
		}

		if (m_lstSyllableEnds.size() < m_lstSyllableOffsets.size())
		{
			m_lstSyllableEnds.push_back(m_strPhones.length() - 1);
			m_lstSyllableSizes.push_back(m_strPhones.length() - m_lstSyllableOffsets[m_lstSyllableOffsets.size() - 1]);
		}
		m_lstSyllableOffsets.erase(m_lstSyllableOffsets.begin());
		m_lstSyllableOffsets.erase(m_lstSyllableOffsets.end() - 1);
		m_lstSyllableEnds.erase(m_lstSyllableEnds.begin());
		m_lstSyllableEnds.erase(m_lstSyllableEnds.end() - 1);
		m_lstSyllableSizes.erase(m_lstSyllableSizes.begin());
		m_lstSyllableSizes.erase(m_lstSyllableSizes.end() - 1);

		if (m_lstWordEnds.size() < m_lstWordOffsets.size())
		{
			m_lstWordEnds.push_back(m_strPhones.length() - 1);
			m_lstWordSizes.push_back(m_strPhones.length() - m_lstWordOffsets[m_lstWordOffsets.size() - 1]);
		}
		m_lstWordOffsets.erase(m_lstWordOffsets.begin());
		m_lstWordOffsets.erase(m_lstWordOffsets.end() - 1);
		m_lstWordEnds.erase(m_lstWordEnds.begin());
		m_lstWordEnds.erase(m_lstWordEnds.end() - 1);
		m_lstWordSizes.erase(m_lstWordSizes.begin());
		m_lstWordSizes.erase(m_lstWordSizes.end() - 1);

		if (m_lstPhraseEnds.size() < m_lstPhraseOffsets.size())
		{
			m_lstPhraseEnds.push_back(m_strPhones.length() - 1);
			m_lstPhraseSizes.push_back(m_strPhones.length() - m_lstPhraseOffsets[m_lstPhraseOffsets.size() - 1]);
		}
		for (size_t nIndex = 0; nIndex < m_lstPhraseOffsets.size(); nIndex++)
		{
			size_t nPhraseOffset = m_lstPhraseOffsets[nIndex];
			size_t nPhraseEnd = m_lstPhraseEnds[nIndex];

			for (size_t nWordIndex = 0; nWordIndex < m_lstWordOffsets.size(); nWordIndex++)
			{
				if (nPhraseOffset > m_lstWordOffsets[nWordIndex])
					continue;
				
				nPhraseOffset = m_lstWordOffsets[nWordIndex];
				break;
			}
			for (size_t nWordIndex = m_lstWordOffsets.size() - 1; nWordIndex > 0; nWordIndex--)
			{
				if (nPhraseEnd < m_lstWordOffsets[nWordIndex])
					continue;

				nPhraseEnd = m_lstWordEnds[nWordIndex];
				break;
			}

			m_lstPhraseOffsets[nIndex] = nPhraseOffset;
			m_lstPhraseEnds[nIndex] = nPhraseEnd;
			m_lstPhraseSizes[nIndex] = nPhraseEnd - nPhraseOffset;
		}
	}

	bool InitTiming(std::ifstream& timings, std::string& strTimingError)
	{
		strTimingError.clear();

		int line = 1;
		std::string strTimingInfo;
		// The very first line of the timings file contains hash sign
		std::getline(timings, strTimingInfo, '\n');

		for (size_t pos = 0; pos < m_strPhones.length(); pos++)
		{
			if (::IsPhoneme(m_strPhones[pos]))
			{
				// Each phoneme should have timing information
				unsigned int sec, fracSec, temp;
				char phone;

				++line;
				std::getline(timings, strTimingInfo, '\n');

				if (sscanf(strTimingInfo.c_str(), "%u.%u %u %c", &sec, &fracSec, &temp, &phone) != 4)
				{
					strTimingError = "Malformed line #" + ToString(line);
					return false;
				}

				if (m_strPhones[pos] != phone)
				{
					strTimingError = "Phoneme mismatch at position #" + ToString(pos);
					return false;
				}

				m_mapTimings[pos] = CDecimal(sec, fracSec);
			}
		}

		return true;
	}

	operator const std::string& () const { return m_strPhones; }
	size_t length() const { return m_strPhones.length(); }

	inline bool IsPhoneme(size_t pos) const
	{
		return (pos >= 0 && pos <= length() - 1 && ::IsPhoneme(m_strPhones[pos]));
	}

	inline CDecimal GetCurrPhonemeTiming(size_t pos) const
	{
		std::map<size_t, CDecimal>::const_iterator it = m_mapTimings.find(pos);
		if (it == m_mapTimings.end())
			return 0;
		return (*it).second;
	}

	inline CDecimal GetPrevPhonemeTiming(size_t pos) const
	{
		int counter = -1;
		int direction = (counter > 0 ? 1 : -1);
		while (counter != 0 && pos >= 0 && pos <= length() - 1)
		{
			if (pos == 0 && direction < 0)
				break;
			if (pos == length() - 1 && direction > 0)
				break;

			pos += direction;
			if (::IsPhoneme(m_strPhones[pos]))
				counter -= direction;
		}
		if (counter == 0)
			return GetCurrPhonemeTiming(pos);
		return 0;
	}

	inline char GetCharAt(size_t pos) const
	{
		return m_strPhones[pos];
	}

	inline std::string GetPhonemeAt(size_t pos) const
	{
		return ::IsPause(m_strPhones[pos]) ? "pau" : std::string(1, m_strPhones[pos]);
	}

	inline std::string GetPhoneme(size_t pos, int counter) const
	{
		if (counter == 0)
			throw std::exception();

		int direction = (counter > 0 ? 1 : -1);
		while (counter != 0 && pos >= 0 && pos <= length() - 1)
		{
			if (pos == 0 && direction < 0)
				break;
			if (pos == length() - 1 && direction > 0)
				break;

			pos += direction;
			if (::IsPhoneme(m_strPhones[pos]))
				counter -= direction;
		}
		if (counter == 0)
			return GetPhonemeAt(pos);
		return MISSING_PHONEME;
	}

	inline std::string PrevPrevPhoneme(size_t pos) const
	{
		return GetPhoneme(pos, -2);
	}

	inline std::string PrevPhoneme(size_t pos) const
	{
		return GetPhoneme(pos, -1);
	}

	inline std::string NextPhoneme(size_t pos) const
	{
		return GetPhoneme(pos, +1);
	}

	inline std::string NextNextPhoneme(size_t pos) const
	{
		return GetPhoneme(pos, +2);
	}

	inline size_t GetPhonemeInSyllablePos(size_t pos, int direction) const
	{
		if (direction == 0)
			throw std::exception();

		size_t offset = 0;
		while (pos >= 0 && pos <= length() - 1)
		{
			if (pos == 0 && direction < 0)
				break;
			if (pos == length() - 1 && direction > 0)
				break;

			pos += direction;

			if (!::IsPhoneme(m_strPhones[pos]))
				break;

			++offset;
		}
		return offset;
	}

	inline size_t GetForwardPhonemeInSyllablePos(size_t pos) const
	{
		return GetPhonemeInSyllablePos(pos, -1);
	}

	inline size_t GetBackwardPhonemeInSyllablePos(size_t pos) const
	{
		return GetPhonemeInSyllablePos(pos, +1);
	}

	inline int GetSyllableIndex(size_t pos) const
	{
		for (size_t i = 0; i < m_lstSyllableOffsets.size(); i++)
		{
			if (pos < m_lstSyllableOffsets[i])
				break;

			if (pos <= m_lstSyllableEnds[i])
				return i;
		}

		return -1;
	}

	inline bool IsSyllableStressed(int idx) const
	{
		size_t pos = m_lstSyllableOffsets[idx];
		return (pos > 0 && m_strPhones[pos - 1] == '.');
	}

	inline std::string IsPrevSyllableStressed(size_t pos) const
	{
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		if (idx < 1)
			return "0";
		return (IsSyllableStressed(idx - 1) ? "1" : "0");
	}

	inline std::string IsPrevSyllableAccented(size_t pos) const
	{
		// Not applicable
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		return "0";
	}

	inline std::string GetPrevSyllablePhonemeCount(size_t pos) const
	{
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		if (idx < 1)
			return "0";
		return ToString(m_lstSyllableSizes[idx - 1]);
	}

	inline std::string IsCurrSyllableStressed(size_t pos) const
	{
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		return (IsSyllableStressed(idx) ? "1" : "0");
	}

	inline std::string IsCurrSyllableAccented(size_t pos) const
	{
		// Not applicable
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		return "0";
	}

	inline std::string GetCurrSyllablePhonemeCount(size_t pos) const
	{
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		return ToString(m_lstSyllableSizes[idx]);
	}

	inline int GetWordIndex(size_t pos) const
	{
		for (size_t i = 0; i < m_lstWordOffsets.size(); i++)
		{
			if (pos < m_lstWordOffsets[i])
				break;

			if (pos <= m_lstWordEnds[i])
				return i;
		}

		return -1;
	}

	inline std::string GetForwardSyllableInWordPos(size_t pos) const
	{
		int wordIdx = GetWordIndex(pos);
		if (wordIdx < 0)
			return "x";
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableOffset = 0;

		size_t wordOffset = m_lstWordOffsets[wordIdx];
		while (syllableIdx > 0)
		{
			--syllableIdx;
			if (m_lstSyllableOffsets[syllableIdx] < wordOffset)
				break;
			//syllableOffset += m_lstSyllableSizes[syllableIdx];
			++syllableOffset;
		}

		return ToString(syllableOffset);
	}

	inline std::string GetBackwardSyllableInWordPos(size_t pos) const
	{
		int wordIdx = GetWordIndex(pos);
		if (wordIdx < 0)
			return "x";
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableOffset = 0;

		size_t wordEnd = m_lstWordEnds[wordIdx];
		while (syllableIdx < (int)m_lstSyllableOffsets.size() - 1)
		{
			++syllableIdx;
			if (m_lstSyllableOffsets[syllableIdx] > wordEnd)
				break;
			//syllableOffset += m_lstSyllableSizes[syllableIdx];
			++syllableOffset;
		}

		return ToString(syllableOffset);
	}

	inline int GetPhraseIndex(size_t pos) const
	{
		for (size_t i = 0; i < m_lstPhraseOffsets.size(); i++)
		{
			if (pos < m_lstPhraseOffsets[i])
				break;

			if (pos <= m_lstPhraseEnds[i])
				return i;
		}

		return -1;
	}

	inline std::string GetForwardSyllableInPhrasePos(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableOffset = 0;

		size_t phraseOffset = m_lstPhraseOffsets[phraseIdx];
		while (syllableIdx > 0)
		{
			--syllableIdx;
			if (m_lstSyllableOffsets[syllableIdx] < phraseOffset)
				break;
			//syllableOffset += m_lstSyllableSizes[syllableIdx];
			++syllableOffset;
		}

		return ToString(syllableOffset);
	}

	inline std::string GetBackwardSyllableInPhrasePos(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableOffset = 0;

		size_t phraseEnd = m_lstPhraseEnds[phraseIdx];
		while (syllableIdx < (int)m_lstSyllableOffsets.size() - 1)
		{
			++syllableIdx;
			if (m_lstSyllableOffsets[syllableIdx] > phraseEnd)
				break;
			//syllableOffset += m_lstSyllableSizes[syllableIdx];
			++syllableOffset;
		}

		return ToString(syllableOffset);
	}

	inline std::string GetForwardStressedSyllablesInPhrase(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableCount = 0;

		size_t phraseOffset = m_lstPhraseOffsets[phraseIdx];
		while (syllableIdx > 0)
		{
			--syllableIdx;
			if (m_lstSyllableOffsets[syllableIdx] < phraseOffset)
				break;
			if (IsSyllableStressed(syllableIdx))
				++syllableCount;
		}

		return ToString(syllableCount);
	}

	inline std::string GetBackwardStressedSyllablesInPhrase(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableCount = 0;

		size_t phraseEnd = m_lstPhraseEnds[phraseIdx];
		while (syllableIdx < (int)m_lstSyllableOffsets.size() - 1)
		{
			++syllableIdx;
			if (m_lstSyllableOffsets[syllableIdx] > phraseEnd)
				break;
			if (IsSyllableStressed(syllableIdx))
				++syllableCount;
		}

		return ToString(syllableCount);
	}

	inline std::string GetForwardAccentedSyllablesInPhrase(size_t pos) const
	{
		// Not applicable
		if (GetPhraseIndex(pos) < 0 || GetSyllableIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetBackwardAccentedSyllablesInPhrase(size_t pos) const
	{
		// Not applicable
		if (GetPhraseIndex(pos) < 0 || GetSyllableIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetForwardCountFromStressedSyllable(size_t pos) const
	{
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableCount = 0;

		while (syllableIdx > 0)
		{
			--syllableIdx;
			++syllableCount;
			if (IsSyllableStressed(syllableIdx))
				return ToString(syllableCount);
		}

		return "0";
	}

	inline std::string GetBackwardCountToStressedSyllable(size_t pos) const
	{
		int syllableIdx = GetSyllableIndex(pos);
		if (syllableIdx < 0)
			return "x";

		size_t syllableCount = 0;

		while (syllableIdx < (int)m_lstSyllableOffsets.size() - 1)
		{
			++syllableIdx;
			++syllableCount;
			if (IsSyllableStressed(syllableIdx))
				return ToString(syllableCount);
		}

		return "0";
	}

	inline std::string GetForwardCountFromAccentedSyllable(size_t pos) const
	{
		// Not applicable
		if (GetSyllableIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetBackwardCountToAccentedSyllable(size_t pos) const
	{
		// Not applicable
		if (GetSyllableIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetCurrSyllableVowels(size_t pos) const
	{
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		
		std::string strVowels;

		for (size_t charPos = m_lstSyllableOffsets[idx]; charPos <= m_lstSyllableEnds[idx]; charPos++)
		{
			if (::IsVowel(m_strPhones[charPos]))
				strVowels += m_strPhones[charPos];
		}

		// if a syllable has no vowel, you need to print "novowel" (line 997). dont ask me what is the difference
		// between x and no vowel, but in files alice38 & 19 novowel is used
		if (strVowels.empty())
			strVowels = "novowel";

		return strVowels;
	}

	inline std::string IsNextSyllableStressed(size_t pos) const
	{
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		if (idx >= (int)m_lstSyllableOffsets.size() - 1)
			return "0";
		return (IsSyllableStressed(idx + 1) ? "1" : "0");
	}

	inline std::string IsNextSyllableAccented(size_t pos) const
	{
		// Not applicable
		if (GetSyllableIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetNextSyllablePhonemeCount(size_t pos) const
	{
		int idx = GetSyllableIndex(pos);
		if (idx < 0)
			return "x";
		if (idx >= (int)m_lstSyllableOffsets.size() - 1)
			return "0";
		return ToString(m_lstSyllableSizes[idx + 1]);
	}

	inline std::string GetPrevWordGPOS(size_t pos) const
	{
		return "0";
	}

	inline size_t GetSyllableCountInWord(int idx) const
    {
        size_t wordOffset = m_lstWordOffsets[idx], wordEnd = m_lstWordEnds[idx];
        
        // ATTENTION
        // ---------
        // Marker characters (dot and hash sign) are considered a part of a word, but do not
        // constitute a syllable. Therefore, it is possible to have a word begin or end with
        // a marker, in which case GetSyllableIndex() called for the corresponding offset/end
        // index will yield negative one. We shall want to find the beginning of the syllable
        // by seeking the corresponding offset/end.
        
        int syllableBeginIdx;
        while ((syllableBeginIdx = GetSyllableIndex(wordOffset)) < 0 && wordOffset < wordEnd)
            ++wordOffset;
        
        int syllableEndIdx;
        while ((syllableEndIdx = GetSyllableIndex(wordEnd)) < 0 && wordEnd > wordOffset)
            --wordEnd;
        
        return (syllableEndIdx - syllableBeginIdx + 1);
    }

	inline std::string GetSyllableCountInPrevWord(size_t pos) const
	{
		int wordIdx = GetWordIndex(pos);
		if (wordIdx < 0)
			return "x";
		if (wordIdx < 1)
			return "0";
		return ToString(GetSyllableCountInWord(wordIdx - 1));
	}

	inline std::string GetCurrWordGPOS(size_t pos) const
	{
		return "0";
	}

	inline std::string GetSyllableCountInCurrWord(size_t pos) const
	{
		int wordIdx = GetWordIndex(pos);
		if (wordIdx < 0)
			return "x";
		return ToString(GetSyllableCountInWord(wordIdx));
	}

	inline std::string GetForwardWordInPhrasePos(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		int wordIdx = GetWordIndex(pos);
		if (wordIdx < 0)
			return "x";

		size_t wordCount = 0;

		size_t phraseOffset = m_lstPhraseOffsets[phraseIdx];
		while (wordIdx > 0)
		{
			--wordIdx;
			if (m_lstWordOffsets[wordIdx] < phraseOffset)
				break;
			++wordCount;
		}

		return ToString(wordCount);
	}

	inline std::string GetBackwardWordInPhrasePos(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		int wordIdx = GetWordIndex(pos);
		if (wordIdx < 0)
			return "x";

		size_t wordCount = 0;

		size_t phraseEnd = m_lstPhraseEnds[phraseIdx];
		while (wordIdx < (int)m_lstWordOffsets.size() - 1)
		{
			++wordIdx;
			if (m_lstWordOffsets[wordIdx] > phraseEnd)
				break;
			++wordCount;
		}

		return ToString(wordCount);
	}

	inline std::string GetContentWordCountBeforeCurrInPhrase(size_t pos) const
	{
		// Not supported
		if (GetWordIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetContentWordCountAfterCurrInPhrase(size_t pos) const
	{
		// Not supported
		if (GetWordIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetWordCountFromPrevContentWord(size_t pos) const
	{
		// Not supported
		if (GetWordIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetWordCountToNextContentWord(size_t pos) const
	{
		// Not supported
		if (GetWordIndex(pos) < 0)
			return "x";
		return "0";
	}

	inline std::string GetNextWordGPOS(size_t pos) const
	{
		return "0";
	}

    inline std::string GetSyllableCountInNextWord(size_t pos) const
    {
        int wordIdx = GetWordIndex(pos);
        if (wordIdx < 0)
            return "x";
        if (wordIdx >= (int)m_lstWordOffsets.size() - 1)
            return "0";
        return ToString(GetSyllableCountInWord(wordIdx + 1));
    }
    
    inline size_t GetSyllableCountInPhrase(int idx) const
    {
        size_t phraseOffset = m_lstPhraseOffsets[idx], phraseEnd = m_lstPhraseEnds[idx];
        
        int syllableBeginIdx;
        while ((syllableBeginIdx = GetSyllableIndex(phraseOffset)) < 0 && phraseOffset < phraseEnd)
            ++phraseOffset;
        
        int syllableEndIdx;
        while ((syllableEndIdx = GetSyllableIndex(phraseEnd)) < 0 && phraseEnd > phraseOffset)
            --phraseEnd;
        
        return (syllableEndIdx - syllableBeginIdx + 1);
    }
    
    inline std::string GetSyllableCountInPrevPhrase(size_t pos) const
    {
        int phraseIdx = GetPhraseIndex(pos);
        if (phraseIdx < 0)
            return "x";
        if (phraseIdx < 1)
            return "0";
        return ToString(GetSyllableCountInPhrase(phraseIdx - 1));
    }
    
    inline size_t GetWordCountInPhrase(int idx) const
    {
        size_t phraseOffset = m_lstPhraseOffsets[idx], phraseEnd = m_lstPhraseEnds[idx];
        
        int wordBeginIdx = GetWordIndex(phraseOffset);
        if (wordBeginIdx < 0)
            wordBeginIdx = 0;
        int wordEndIdx = GetWordIndex(phraseEnd);
        
        return (wordEndIdx - wordBeginIdx + 1);
    }

	inline std::string GetWordCountInPrevPhrase(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		if (phraseIdx < 1)
			return "0";
		return ToString(GetWordCountInPhrase(phraseIdx - 1));
	}

	inline std::string GetSyllableCountInCurrPhrase(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		return ToString(GetSyllableCountInPhrase(phraseIdx));
	}

	inline std::string GetWordCountInCurrPhrase(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		return ToString(GetWordCountInPhrase(phraseIdx));
	}

	inline std::string GetForwardPhraseInUtterancePos(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		return ToString(phraseIdx);
	}

	inline std::string GetBackwardPhraseInUtterancePos(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		return ToString(m_lstPhraseOffsets.size() - phraseIdx - 1);
	}

	inline size_t GetCurrPhraseTOBIEndtone(size_t pos) const
	{
		return 0;
	}

	inline std::string GetSyllableCountInNextPhrase(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		if (phraseIdx >= (int)m_lstPhraseOffsets.size() - 1)
			return "0";
		return ToString(GetSyllableCountInPhrase(phraseIdx + 1));
	}

	inline std::string GetWordCountInNextPhrase(size_t pos) const
	{
		int phraseIdx = GetPhraseIndex(pos);
		if (phraseIdx < 0)
			return "x";
		if (phraseIdx >= (int)m_lstPhraseOffsets.size() - 1)
			return "0";
		return ToString(GetWordCountInPhrase(phraseIdx + 1));
	}

	inline size_t GetSyllableCountInUtterance() const
	{
		return m_lstSyllableOffsets.size();
	}

	inline size_t GetWordCountInUtterance() const
	{
		return m_lstWordOffsets.size();
	}

	inline size_t GetPhraseCountInUtterance() const
	{
		return m_lstPhraseOffsets.size();
	}
};
