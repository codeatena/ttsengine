#pragma once

class CDecimal
{
public:

	static void Initialize(int precision);

	CDecimal();
	CDecimal(const CDecimal& d);
	CDecimal(const __int64_t n);
	CDecimal(int intPart, int fractPart);

	virtual ~CDecimal();

	CDecimal operator+(const CDecimal&);
	CDecimal operator-(const CDecimal&);
	CDecimal operator*(const CDecimal&);
	CDecimal operator/(const CDecimal&);

	CDecimal operator +=(const CDecimal&);
	CDecimal operator -=(const CDecimal&);
	CDecimal operator *=(const CDecimal&);
	CDecimal operator /=(const CDecimal&);

	bool operator==(const CDecimal&) const;
	bool operator!=(const CDecimal&) const;
	bool operator<(const CDecimal&) const;
	bool operator<=(const CDecimal&) const;
	bool operator>(const CDecimal&) const;
	bool operator>=(const CDecimal&) const;

	std::string ToString() const;
	double ToDouble() const;
	__int64_t ToInteger() const { return n; }

protected:

	__int64_t n;

	static int precision;
	static __int64_t q;
	static char* pad;
};
