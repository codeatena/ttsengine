#include "HTSconv.h"
#include "Decimal.h"
#include <math.h>

int CDecimal::precision=0;
__int64_t CDecimal::q=0;
char* CDecimal::pad=0L;

// static initialization.  Sets up all CDecimal arithmetic functions
void CDecimal::Initialize(int prec)
{
	precision=prec;
	// create an array of 0's for padding
	pad=new char[precision+1];
	memset(pad, '0', precision);
	pad[precision]='\0';
	// get fractional precision
	q=(__int64_t)pow(10.0, (double)prec);		
}

CDecimal::CDecimal(void)
{
	n=0;
}

CDecimal::CDecimal(const CDecimal& d)
{
	n=d.n;
}

CDecimal::CDecimal(const __int64_t n)
{
	this->n=n;
}

CDecimal::CDecimal(int intPart, int fractPart)
{
	n=intPart;
	n*=q;
	n+=(__int64_t)fractPart;
}

CDecimal::~CDecimal()
{
}

CDecimal CDecimal::operator+(const CDecimal& d)
{
	return CDecimal(n + d.n);
}

CDecimal CDecimal::operator-(const CDecimal& d)
{
	return CDecimal(n - d.n);
}

CDecimal CDecimal::operator*(const CDecimal& d)
{
	return CDecimal(n * d.n / q);
}

CDecimal CDecimal::operator/(const CDecimal& d)
{
	return CDecimal(n * q / d.n);
}


CDecimal CDecimal::operator +=(const CDecimal& d)
{
	n=n + d.n;
	return *this;
}

CDecimal CDecimal::operator -=(const CDecimal& d)
{
	n=n - d.n;
	return *this;
}

CDecimal CDecimal::operator *=(const CDecimal& d)
{
	n=n * d.n / q;
	return *this;
}

CDecimal CDecimal::operator /=(const CDecimal& d)
{
	n=n * q / d.n;
	return *this;
}

bool CDecimal::operator==(const CDecimal& d) const
{
	return n == d.n;
}

bool CDecimal::operator!=(const CDecimal& d) const
{
	return n != d.n;
}

bool CDecimal::operator<(const CDecimal& d) const
{
	return n < d.n;
}

bool CDecimal::operator<=(const CDecimal& d) const
{
	return n <= d.n;
}

bool CDecimal::operator>(const CDecimal& d) const
{
	return n > d.n;
}

bool CDecimal::operator>=(const CDecimal& d) const
{
	return n >= d.n;
}

std::string CDecimal::ToString() const
{
	char s[64];
	__int64_t n2=n/q;
	int fract=(int)(n-n2*q);
	sprintf(s, "%d.%0*d", (int)n2, precision, fract);
	return s;
}

double CDecimal::ToDouble(void) const
{
	return atof(ToString().c_str());
}