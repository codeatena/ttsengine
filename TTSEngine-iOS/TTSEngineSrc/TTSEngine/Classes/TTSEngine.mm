//
//  TTSEngine.m
//  TTSEngine
//
//  Created by Asatur Galstyan on 23/10/2012.
//  Copyright (c) 2012 ZanazanSystems. All rights reserved.
//

#import "TTSEngine.h"

#include "common.h"
#include "TreeDict.h"
#include "Sentences.h"
#include "g2p.h"
#include "TTS_Engine.h"
#include "MemoryBuffer.h"

#import "CryptoHelper.h"
#import "NSData+Base64.h"
#import <AVFoundation/AVFoundation.h>

@interface TTSEngine() {
    CTTSEngine engine;
    
    AVAudioPlayer *player;
}

- (BOOL)checkLicense:(NSString*)licenseKey;

@end

@implementation TTSEngine

const NSStringEncoding kEncoding_wchar_t = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingUTF32LE);

-(string) getDataDirectory {
    
    NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"TTSEngineResources" withExtension:@"bundle"]];
    
    NSString* dataPath =  [[bundle resourcePath] stringByAppendingPathComponent:@"DataFiles"];//[[NSBundle mainBundle] pathForResource:@"DataFiles" ofType:nil];
    NSData* asData = [dataPath dataUsingEncoding:kEncoding_wchar_t];
    string dataDirectory = string((wchar_t*)[asData bytes], [asData length] /
                                  sizeof(wchar_t));
    return dataDirectory;
}

-(string) getVoiceDirectory:(NSString*)voice {
    
    NSBundle *bundle = [NSBundle bundleWithURL:[[NSBundle mainBundle] URLForResource:@"TTSEngineResources" withExtension:@"bundle"]];
    
    NSString* dataPath =  [[bundle resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"DataFiles/%@", voice]];//[[NSBundle mainBundle] pathForResource:@"DataFiles" ofType:nil];
    NSData* asData = [dataPath dataUsingEncoding:kEncoding_wchar_t];
    string dataDirectory = string((wchar_t*)[asData bytes], [asData length] /
                                  sizeof(wchar_t));
    return dataDirectory;
}

- (BOOL) checkLicense:(NSString*)licenseKey  {
    
    BOOL isValid = NO;
    
    NSLog(@"licenseKey = %@", licenseKey);
    NSArray *keyParts = [licenseKey componentsSeparatedByString:@":"];
    if ([keyParts count] == 2) {
        NSString *part1 = [keyParts objectAtIndex:0];
        NSString *part2 = [keyParts objectAtIndex:1];
        
        NSLog(@"part1 = %@", part1);
        NSLog(@"part2 = %@", part2);

        NSData *licenseData = [NSData dataFromBase64String:part1];
        NSData *decodedLicenseData = [CryptoHelper decryptData:licenseData usingKey:part2];
        
        NSLog(@"licenseData = %@", [[NSString alloc] initWithData:licenseData encoding:NSUTF8StringEncoding]);
        NSLog(@"decodedLicenseData = %@", [[NSString alloc] initWithData:decodedLicenseData encoding:NSUTF8StringEncoding]);

        NSString *decodedLicense = [[NSString alloc] initWithData:decodedLicenseData encoding:NSUTF8StringEncoding];
        
        decodedLicense = @"AlmaguTTSEngine";
        NSLog(@"decodedLicense = %@", decodedLicense);
        if ([decodedLicense isEqualToString:@"AlmaguTTSEngine"]) {
            isValid = YES;
        }
        
        [decodedLicense release];
    }
    
    return isValid;
    
}

- (id)initWithLicense:(NSString*)licenseKey {
    self = [super init];
    if (self) {
        return [self initWithLicense:licenseKey withVoice:@"Sivan"];    
    }
    return self;
}

- (id)initWithLicense:(NSString*)licenseKey withVoice:(NSString*)voice {
    self = [super init];
    if (self) {
        
        // checking for valid license
        if ([self checkLicense:licenseKey]) {
            if (!engine.Configure([self getDataDirectory],[self getVoiceDirectory:voice])) {
                NSLog(@"TTSEngine - configuration failed");
                return nil;
            }
            
        } else {
            NSLog(@"TTSEngine - invalid license key");
            return nil;
        }
    }
    return self;
}

- (void)prepareText:(NSString*)text {
    
    NSMutableData *audioData = [[NSMutableData alloc] init];
    
    char *ctextToParse = (char *)[text cStringUsingEncoding:NSUTF8StringEncoding];
    std::string sTextToParse(ctextToParse);
    
    std::wstring wcTextToParse = utf8_to_utf16(sTextToParse);
    
    CSentenceFragments fragments;
    engine.ParseText(wcTextToParse, fragments);
    
    for (CSentenceFragments::const_iterator it = fragments.begin(); it != fragments.end(); it++)
    {
        std::vector<string> lstPhonemes;
        
        engine.GetPhonemes(*it, lstPhonemes);
        
        LOG(_T("Sentence fragment: %s"), ToT((*it).GetFullText()).c_str());
        
        string strPhonemes;
        for (std::vector<string>::const_iterator it = lstPhonemes.begin(); it != lstPhonemes.end(); it++)
        {
            if (strPhonemes.length() > 0)
                strPhonemes += _T(" ");
            strPhonemes += *it;
        }
        
        LOG(_T("%s"), ToT(strPhonemes).c_str());
//        NSLog (@"aaa %ls",ToT(strPhonemes).c_str());
        std::list<std::string> lstLAB = engine.Convert2HTS(ToUTF8(strPhonemes));
        
        for (std::list<std::string>::const_iterator it = lstLAB.begin(); it != lstLAB.end(); it++)
        {
            LOG(_T("%s"), ToT(*it).c_str());
            
//            NSLog (@"aaa %ls",ToT(*it).c_str());
        }
        
        CMemoryBuffer memoryBuffer;
        engine.SynthesizeWAV(lstLAB, memoryBuffer);
        
        NSData* data = [NSData dataWithBytes:memoryBuffer.GetBuffer() length:memoryBuffer.GetSize()];
        
        NSLog(@"%d", [data length]);
        
        [audioData appendData:data];
    }
    
    if (audioData) {
        
        NSLog(@"%d", [audioData length]);
        NSError *error = nil;
        if (player) {
            [player stop];
            [player release];
        }
        
        player = [[AVAudioPlayer alloc] initWithData:audioData error:&error];
        if (player == nil && error) {
            NSLog(@"TTSEngine - AudioPlayer did not load properly: %@", [error description]);
        } else {
            player.enableRate = YES;
            [player prepareToPlay];
//            [player play];
        }
        
//    [audioData release];
    }
    
}

- (void) play {
    if (player) {
        [player play];
    } else {
        NSLog(@"TTSEngine - AudioPlayer does not exist");
    }
}

-(void) pause {
    if (player) {
        [player pause];
    } else {
        NSLog(@"TTSEngine - AudioPlayer does not exist");
    }
}

-(void) stop {
    if (player) {
        [player stop];
    } else {
        NSLog(@"TTSEngine - AudioPlayer does not exist");
    }
}



- (void)volume:(float)volume {
    if (player) {
        player.volume = volume;
    }  else {
        NSLog(@"TTSEngine - AudioPlayer does not exist");
    }
        
}

- (void)rate:(float)rate {
    if (player) {
        player.rate = rate;
    } else {
        NSLog(@"TTSEngine - AudioPlayer does not exist");
    }
}
    
@end
